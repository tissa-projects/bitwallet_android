package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleObserver;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;

import java.text.DecimalFormat;

public class PaymentReceivedMerchandActivity extends AppCompatActivity implements LifecycleObserver {

    private Context context;
    private TextView txtBtcAmt, txtdollarAmt, txtAddress;
    private static final String TAG = PaymentReceivedMerchandActivity.class.getName();
    private static int SPLASH_TIME_OUT = 5000;
    private Handler handler;
    private boolean minimizeBtnPressed = false;

    String senderAddress, strHash, strTimeStamp, strReceiverAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_payment_received_merchand_tab);
        } else {
            setContentView(R.layout.activity_payment_received_merchand);
        }
        context = PaymentReceivedMerchandActivity.this;
        initialize();
        getIntentData();
        splashTime();
    }


    @Override
    protected void onResume() {
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onResume();
    }

    /* *//* //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            handler.removeCallbacksAndMessages(null);
            finish();
        }
        super.onUserLeaveHint();
    }
*//*
    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            handler.removeCallbacksAndMessages(null);
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            handler.removeCallbacksAndMessages(null);
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
            handler.removeCallbacksAndMessages(null);
        }
        super.onStop();

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            handler.removeCallbacksAndMessages(null);
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

*/
    private void initialize() {
        txtBtcAmt = findViewById(R.id.txt_btc);
        txtdollarAmt = findViewById(R.id.txt_dollar);
        txtAddress = findViewById(R.id.txt_address);

    }

    private void getIntentData() {
       boolean isTipSwitchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_TIP_SWITCH_ON, false);
        Log.e(TAG, "getIntentData: "+isTipSwitchOn );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            senderAddress = extras.getString("senderAddress");
            //  btcValue = extras.getString("valueBtc");
            strHash = extras.getString("hash");
            strTimeStamp = extras.getString("timeStamp");
            strReceiverAddress = extras.getString("receiverAddress");

           /* Log.e(TAG, "doInBackground: strHash " + strHash);
            Log.e(TAG, "doInBackground:senderAddress " + senderAddress);
            Log.e(TAG, "doInBackground:strReceiverAddress " + strReceiverAddress);
            Log.e(TAG, "doInBackground:strTimeStamp " + strTimeStamp);*/

            String strtipAmt = ACU.MySP.getFromSP(context, ACU.MySP.TIP_AMT, "");
            double receivedDollerAmt = (double) (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.TIP_RECEIPT_DOLLAR_AMT, "")));

            DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
            final String strReceivedDollerAmt = numberFormat.format(receivedDollerAmt);
            Log.e(TAG, "getIntentData: TIPAMT: " + strtipAmt);

            txtBtcAmt.setText((ACU.MySP.getFromSP(context, ACU.MySP.TIP_RECEIPT_BTC_AMT, "")) + " BTC");
            if (isTipSwitchOn) {
                txtdollarAmt.setText("$" + strReceivedDollerAmt + " + $" + strtipAmt + " TIP");
            } else {
                txtdollarAmt.setText("$" + strReceivedDollerAmt);
            }
            txtAddress.setText(senderAddress);
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void splashTime() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(context, ReciptMerchandActivity.class);
                intent.putExtra("senderAddress", senderAddress);
                intent.putExtra("hash", strHash);
                intent.putExtra("timeStamp", strTimeStamp);
                intent.putExtra("receiverAddress", strReceiverAddress);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
