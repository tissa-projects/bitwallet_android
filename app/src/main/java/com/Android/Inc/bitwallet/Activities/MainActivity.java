package com.Android.Inc.bitwallet.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.Android.Inc.bitwallet.Models.CertificateModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    TextView txtLogin, txtSignUp;
    Context context;
    View layout;
    String screenType;
    boolean isSecurityCode = true;
    private static final String TAG = MainActivity.class.getName();
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 12;
    private int currentApiVersion;
    private boolean isApprovedFileManagePermission = false;


    private PendingIntent pendingIntent;
    private AlarmManager manager;
    private int interval = 500;

    private ArrayList<CertificateModel> certificateModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_main_tablet);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_main);
        }

        context = MainActivity.this;

        certificateModels = new ArrayList<>();

        //for logout no btn : do not have to go directly on setting frgmnt
        ACU.MySP.saveSP(context, ACU.MySP.LOGOUT_NO_BTN, " ");

        /*if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
                finish();
        }else */
        isSecurityCode = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_SECURITY_CODE, false);
        Log.e(TAG, "onCreate: isSecurityCode: " + isSecurityCode);
        if (ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_STATUS, "").equalsIgnoreCase("true") && isSecurityCode) {
            if (ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").equalsIgnoreCase("BUSINESS")) {
                startActivity(new Intent(MainActivity.this, ChargeBitwalletActivity.class));
                finish();
            } else {
                // individual user
                startActivity(new Intent(MainActivity.this, PinActivity.class));
                finish();
            }
        }

        initialize();
        forceUpdate();
        currentApiVersion = Build.VERSION.SDK_INT;

        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                //   Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();

            } else {
                requestPermission();
            }


            certificateModels.clear();
            AssetManager assetManager = context.getAssets();
            String[] files = new String[0];
            try {
                files = assetManager.list("Files");
                for (int i = 0; i < files.length; i++) {
                    CertificateModel model = new CertificateModel();
                    model.assetcertificate = files[i].toString();
                    certificateModels.add(model);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

           /* for(int i=0; i<files.length; i++) {
                Toast.makeText(context, "\n"+files[i], Toast.LENGTH_SHORT).show();

            }*/


//            if(currentApiVersion >= Build.VERSION_CODES.R )
//            {
//                if (!Environment.isExternalStorageManager())
//                {
//                   // Toast.makeText(getApplicationContext(), "Permission not granted!", Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent();
//                intent.setAction(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
//                Uri uri = Uri.fromParts("package", this.getPackageName(), null);
//                intent.setData(uri);
//                startActivity(intent);
//
//                }
//                else
//                {
//                    // Toast.makeText(getApplicationContext(), "Permission granted!", Toast.LENGTH_LONG).show();
//
//                }
//
//
            //    }
        }
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        // UsdRateServiceCall();

    }

    private void initialize() {
        txtLogin = findViewById(R.id.txt_login);
        txtSignUp = findViewById(R.id.txt_signUp);

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, CheckIndividualBussinsActivity.class));
                finish();

            }
        });
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, STORAGE_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            //   System.exit(0);
        }
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case STORAGE_PERMISSION_REQUEST_CODE:
//                Log.e("grantResult", grantResults[0] + "" + "" + PackageManager.PERMISSION_DENIED);
                if (grantResults.length > 0) {

                    boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean storageDenied = grantResults[0] == PackageManager.PERMISSION_DENIED;
                    boolean cameraDenied = grantResults[1] == PackageManager.PERMISSION_DENIED;

                    if (storageDenied || cameraDenied) {
                        requestPermission();
                    }/* else if (storageAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now You Can Access Storage", Toast.LENGTH_LONG).show();
                    }else if (storageAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now You Can Access Storage", Toast.LENGTH_LONG).show();
                    }*/
                }
                break;

        }
    }

    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new ForceUpdateAsync(currentVersion, MainActivity.this).execute();
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, String> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.e("ABCDEF", " : " + "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en");
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                        .timeout(10000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                latestVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return latestVersion;
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!(context instanceof SplashActivity)) {
                        if (!((Activity) context).isFinishing()) {
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }


        public void showForceUpdateDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

            if (screenType.equalsIgnoreCase("tablet")) {
                dialog.setContentView(R.layout.dialog_app_update_tab);
            } else {
                dialog.setContentView(R.layout.dialog_app_update);
            }
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialog.findViewById(R.id.btn_cancel).setVisibility(View.GONE);
            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                        dialog.dismiss();
                    }
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }


}
