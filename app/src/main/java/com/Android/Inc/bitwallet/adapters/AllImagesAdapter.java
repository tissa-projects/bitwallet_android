package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;

import org.json.JSONArray;

import java.util.ArrayList;

public class AllImagesAdapter extends PagerAdapter {

    private Context context;
    private JSONArray imageArray;
    private LayoutInflater inflater;

    public AllImagesAdapter(Context context, ArrayList<String> strImageArray) {

        try {
          //  String strImageArray = ACU.MySP.getFromSP(context, ACU.MySP.IMAGE_ARRAY, "");

            imageArray = new JSONArray(strImageArray);
            inflater = LayoutInflater.from(context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        this.context = context;
    }

    @Override
    public int getCount() {
        return imageArray.length();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, container, false);
        try {
            Log.e("TAG", "instantiateItem: "+imageArray.toString() );
            String strImg = null;
            assert imageLayout != null;
            final ImageView imageView = (ImageView) imageLayout
                    .findViewById(R.id.image);

            strImg = imageArray.getString(position);
            Log.e("strImg", "instantiateItem: "+strImg );
            imageView.setImageBitmap(getBitmap(strImg));
            container.addView(imageLayout, 0);

        }catch (Exception e){
            e.printStackTrace();
        }
        return imageLayout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View) object);
    }

    public static Bitmap getBitmap(String image) {
        byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
        return bitmap;
    }
}
