package com.Android.Inc.bitwallet.Activities.CustomActivities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.adapters.ListCountryAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CountryCodeActivity extends AppCompatActivity {

    private Context context;
    Activity activity;
    LinearLayout ll_backArrow;
    private static final String TAG = CountryCodeActivity.class.getSimpleName();

    List<String> countryNameList, countryCodeList;
    ListView listView;
    SearchView searchView;
    ListCountryAdapter adapter;
    private List<ListItemModel> listItemListModel;
    String screenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_country_code_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_country_code);
        }
        context = CountryCodeActivity.this;
        activity = CountryCodeActivity.this;
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        countryNameList = Arrays.asList(getResources().getStringArray(R.array.countryName));
        countryCodeList = Arrays.asList(getResources().getStringArray(R.array.countryCode));
        initialize();

        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initialize() {
        listView = findViewById(R.id.list_view);
        searchView = findViewById(R.id.search);
        listItemListModel = new ArrayList<>();

        for (int i = 0; i < countryCodeList.size(); i++) {
            ListItemModel listItemModel = new ListItemModel();
            listItemModel.setName(countryNameList.get(i));
            listItemModel.setCode(countryCodeList.get(i));

            listItemListModel.add(listItemModel);

        }

        adapter = new ListCountryAdapter(listItemListModel, context);
        listView.setAdapter(adapter);

        searchView.setActivated(true);
        searchView.setQueryHint("Select Your Country");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    adapter.filter("");
                    listView.clearTextFilter();

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

    }


}
