package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.google.zxing.Result;

import java.text.DecimalFormat;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.Manifest.permission.CAMERA;

public class ScanCodeActivity extends AppCompatActivity implements LifecycleObserver, ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private Context context;
    private LinearLayout backArrow;
    private boolean minimizeBtnPressed = false;
    private String btc_amount = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scan_code);
        context = ScanCodeActivity.this;
        scannerView = findViewById(R.id.scanner);
        backArrow = findViewById(R.id.title_bar_left_menu);


        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        int currentApiVersion = Build.VERSION.SDK_INT;

        /*if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                //   Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
            } else {
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                requestPermission();
            }
        }*/
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }

    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            } else {
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {
                    ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                       /* Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }*/
                    }
                }
                break;

        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new androidx.appcompat.app.AlertDialog.Builder(ScanCodeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    boolean hasAddress = false;

    @Override
    public void handleResult(Result result) {
        try {
            String str_barCode = null;
            String myResult = result.getText();
            str_barCode = myResult;
            btc_amount = "0";

           /* String[] myStringArr = str_barCode.split("amount=");
            if (myStringArr.length > 1) {
                btc_amount = myStringArr[1];
                String[] myStringArr2 = myStringArr[0].split("bitcoin");
                if (myStringArr2.length > 1) {
                    myStringArr[0] = myStringArr2[1];
                }
            } else {
                String[] myStringArr1 = myStringArr[0].split("bitcoin");
                if (myStringArr1.length > 1) {
                    myStringArr[0] = myStringArr1[1];
                }
            }
            if (myStringArr[0].length() > 45 || myStringArr[0].length() < 26) {
                Log.d("TAG", "handleResult: 14");
                Log.d("TAG", "handleResult: length is less tha 26 - " + myResult.length());
            }

            DecimalFormat BtcAmtFormat = new DecimalFormat("#0.00000000");
            final String String_btc = BtcAmtFormat.format(Float.valueOf(btc_amount));

            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, myStringArr[0]);
            ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, String_btc);*/

            if (str_barCode.length() == 34) {
                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, str_barCode);
                ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, "0.00");
            } else if (myResult.length() > 26) {
                Log.d("TAG", "handleResult: 1" + " " + myResult);
                String[] str_arr1 = myResult.split(":");
                Log.d("TAG", "handleResult: 2 " + str_arr1.length);
                if (str_arr1.length > 1) {
                    Log.d("TAG", "handleResult: 3");
                    String[] str_arr2 = str_arr1[1].split("\\?");
                    Log.d("TAG", "handleResult: 4" + str_arr2.length);
                    str_barCode = str_arr2[0];
                    Log.d("TAG", "handleResult: 5" + str_barCode);
                    if (str_arr2.length > 1) {
                        Log.d("TAG", "handleResult: 6" + str_arr2[1]);
                        String[] str_arr3 = str_arr2[1].split("=");
                        Log.d("TAG", "handleResult: 7" + str_arr3.length);
                        if (str_arr3.length == 2) {
                            Log.d("TAG", "handleResult: 8" + str_arr3[1]);
                            btc_amount = str_arr3[1];
                            Log.d("TAG", "handleResult: 9");
                        } else {
                            Log.d("TAG", "handleResult: 10");
                            btc_amount = "0";
                        }
                    } else {
                        Log.d("TAG", "handleResult: 11");
                        btc_amount = "0";
                    }
                } else {
                    Log.d("TAG", "handleResult: 12" + " " + str_arr1[0]);
                    //   str_barCode = str_arr1[1];
                    str_barCode = str_arr1[0];
                    btc_amount = "0";
                }
                Log.d("TAG", "handleResult: 13");
                DecimalFormat BtcAmtFormat = new DecimalFormat("#0.00000000");
                final String String_btc = BtcAmtFormat.format(Float.valueOf(btc_amount));
                if (str_barCode.length() > 45) {
                    ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, " ");
                    ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, " ");
                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "invalid address");
                } else {
                    ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, str_barCode);
                    ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, String_btc);
                }
                //  ACU.MySP.saveSP(contextDashBoard, ACU.MySP.DOLLER_AMOUNT, "");
            } else {
                ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, " ");
                ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, " ");
                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "invalid address");
                Log.d("TAG", "handleResult: 14");
                Log.d("TAG", "handleResult: length is less tha 26 - " + myResult.length());
            }







           /* if (myResult.length() > 26 && myResult.length() != 34) {
                Log.d("TAG", "handleResult: 1" + " " + myResult);
                String[] str_arr1 = myResult.split(":");
                Log.d("TAG", "handleResult: 2 " + str_arr1.length);
                if (str_arr1.length > 1 && str_arr1[1].length() > 36) {
                    Log.d("TAG", "handleResult: 3");
                    String[] str_arr2 = str_arr1[1].split("\\?");
                    Log.d("TAG", "handleResult: 4");
                    str_barCode = str_arr2[0];
                    Log.d("TAG", "handleResult: 5");
                    if (str_arr2.length > 1) {
                        Log.d("TAG", "handleResult: 6");
                        String[] str_arr3 = str_arr2[1].split("=");
                        Log.d("TAG", "handleResult: 7");
                        if (str_arr3.length == 2) {
                            Log.d("TAG", "handleResult: 8");
                            btc_amount = str_arr3[1];
                            Log.d("TAG", "handleResult: 9");
                        } else {
                            Log.d("TAG", "handleResult: 10");
                            btc_amount = "0";
                        }
                    } else {
                        Log.d("TAG", "handleResult: 11");
                        btc_amount = "0";
                    }
                } else {
                    Log.d("TAG", "handleResult: 12" + " " + str_arr1[0]);
                    //   str_barCode = str_arr1[1];
                    str_barCode = str_arr1[0];
                    btc_amount = "0";
                }
                Log.d("TAG", "handleResult: 13");
                DecimalFormat BtcAmtFormat = new DecimalFormat("#0.00000000");
                final String String_btc = BtcAmtFormat.format(Float.valueOf(btc_amount));

                ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, str_barCode);
                ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, String_btc);
                //  ACU.MySP.saveSP(contextDashBoard, ACU.MySP.DOLLER_AMOUNT, "");
            } else {
                Log.d("TAG", "handleResult: 14");
                Log.d("TAG", "handleResult: length is less tha 26 - " + myResult.length());
            }*/

        } catch (Exception e) {
            Log.d("TAG", "handleResult: 15");
            Log.e("errorScan", " onResumebardcode " + e.getMessage());
            ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, " ");
            ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, " ");
            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, "invalid address");
        }
        Intent intent = new Intent();
        intent.putExtra("txnType", "SEND");
        setResult(2, intent);
        finish();//finishing activity


    }

    @Override
    public void onBackPressed() {
        ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, "");
        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, "");
        Intent intent = new Intent();
        intent.putExtra("txnType", "SEND");
        setResult(2, intent);
        finish();//finishing activity
    }


}
