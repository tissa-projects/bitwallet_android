package com.Android.Inc.bitwallet.utils;

import android.content.Intent;
import android.util.Log;

import com.Android.Inc.bitwallet.Activities.PaymentReceivedMerchandActivity;
import com.Android.Inc.bitwallet.Activities.TipActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class SocketListener extends WebSocketListener {

    private TipActivity tipActivity;
    private String myAddress;
    private static final String TAG = "SocketListener";

    public SocketListener(TipActivity tipActivity, String strAddress) {
        this.tipActivity = tipActivity;
        myAddress = strAddress;
        Log.e(TAG, "SocketListener: "+strAddress );
      //  tipActivity.startActivity(new Intent(tipActivity,PaymentReceivedMerchandActivity.class));
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        super.onOpen(webSocket, response);
    }

    @Override
    public void onMessage(WebSocket webSocket, final String text) {
        super.onMessage(webSocket, text);
        try {
            JSONObject jsonObject = new JSONObject(text);
            JSONObject xObject = jsonObject.getJSONObject("x");
            String strhash = xObject.getString("hash");
            Log.e(TAG, "onMessage:strhash "+strhash);
            String strTimeStamp = xObject.getString("time");
            Log.e(TAG, "onMessage:strTimeStamp "+strTimeStamp );
            JSONArray inputsArray = xObject.getJSONArray("inputs");
            JSONObject jsonObject1 = inputsArray.getJSONObject(0);
            Log.e(TAG, "onMessage: jsonObject1 "+jsonObject1 );
            JSONObject prev_outObject = jsonObject1.getJSONObject("prev_out");
            String strSenderAddress = prev_outObject.getString("addr");
            Log.e(TAG, "onMessage:strSenderAddress "+strSenderAddress );
            JSONArray outArray = xObject.getJSONArray("out");
            for (int i = 0; i < outArray.length(); i++) {
                JSONObject jsonObject2 = outArray.getJSONObject(i);
                String receiverAddress = jsonObject2.getString("addr");

                if (receiverAddress.equals(myAddress)) {
                  /*  long valueSatoshi = jsonObject2.getLong("value");
                    double valueBtc = (double) valueSatoshi / 100000000;
                    DecimalFormat numberFormatBTC = new DecimalFormat("#0.00000000");*/
                  //  String strBtcAmt = numberFormatBTC.format(valueBtc);


                    Intent intent = new Intent(tipActivity, PaymentReceivedMerchandActivity.class);
                    intent.putExtra("senderAddress", strSenderAddress);
                  //  intent.putExtra("valueBtc", strBtcAmt);
                    intent.putExtra("hash", strhash);
                    intent.putExtra("timeStamp", strTimeStamp);
                    intent.putExtra("receiverAddress", receiverAddress);
                    tipActivity.startActivity(intent);
                    tipActivity.finish();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        super.onClosing(webSocket, code, reason);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        super.onClosed(webSocket, code, reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        super.onFailure(webSocket, t, response);
        //    Log.e("error", t.getMessage());
    }

}