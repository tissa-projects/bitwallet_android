package com.Android.Inc.bitwallet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

public class PaymentSuccessActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {


    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private TextView txtSentAmt, txtDollerAmt, txtAddressKey, txtNetwkFee, txtTotalSent, txtSellMsg, txtLine,txtNetwkFeeApplicable;
    private TextView btnBackToWallet;
    private Context context;
    LinearLayout ll_backArrow;
    String screenType, strType, strWyreFinalRecAmt;
    private boolean minimizeBtnPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_payment_success_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_payment_success);
        }

        context = PaymentSuccessActivity.this;
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        txtSentAmt = findViewById(R.id.txt_sentAmt);
        txtDollerAmt = findViewById(R.id.txt_DollerAmt);
        txtAddressKey = findViewById(R.id.txt_addressKey);
        txtNetwkFee = findViewById(R.id.txt_netwkFee);
        txtTotalSent = findViewById(R.id.txt_totalSent);
        btnBackToWallet = findViewById(R.id.btn_backToWallet);
        txtSellMsg = findViewById(R.id.txt_sell_msg);
        txtLine = findViewById(R.id.txt_line);
        txtNetwkFeeApplicable = findViewById(R.id.txt_netwk_fee_applicable);


        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        txtSentAmt.setText(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMOUNT, "") + " " + ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, ""));
        txtDollerAmt.setText("$ " + ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, ""));
        txtAddressKey.setText(ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ADDRESS, ""));
        txtNetwkFee.setText(ACU.MySP.getFromSP(context, ACU.MySP.NETWORK_FEE, ""));
        txtTotalSent.setText(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMOUNT, ""));

        btnBackToWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, WalletListActivity.class));
                finish();
            }
        });
        getIntentData();
    }

    public void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strType = bundle.getString("type");
            strWyreFinalRecAmt = bundle.getString("wyreFinalRecAmt");

            if (strType.equals("sell_activity")) {
                txtLine.setText("HAS BEEN SOLD");
                //txtSellMsg.setText("Your " + Miscellaneous.CustomsMethods.convertToBTCFormat(strWyreFinalRecAmt) +" "+ getResources().getString(R.string.sell_msg));
                txtSellMsg.setVisibility(View.VISIBLE);
                txtNetwkFeeApplicable.setVisibility(View.VISIBLE);
            } else {
                txtSellMsg.setVisibility(View.GONE);
                txtNetwkFeeApplicable.setVisibility(View.GONE);
                txtLine.setText("HAS BEEN SENT");
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, TransactionActivity.class));
        finish();
    }
}
