package com.Android.Inc.bitwallet.Activities.CustomActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.adapters.ListCountryAdapter;
import com.Android.Inc.bitwallet.adapters.SearchListAdapter;

import java.util.ArrayList;
import java.util.List;

public class StateCityActivity extends AppCompatActivity {

    private Context context;
    Activity activity;
    LinearLayout ll_backArrow;
    private static final String TAG = StateCityActivity.class.getSimpleName();

    List<String> NameList;
    ListView listView;
    SearchView searchView;
    SearchListAdapter adapter;
    private List<ListItemModel> listItemListModel;
    String screenType,searchType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_state_city_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_state_city);
        }
        context = StateCityActivity.this;
        activity = StateCityActivity.this;
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        getIntentData();
        initialize();

        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void getIntentData(){
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            NameList = bundle.getStringArrayList("name_list");
            searchType = bundle.getString("search_type");
        }

    }

    private void initialize() {
        listView = findViewById(R.id.list_view);
        searchView = findViewById(R.id.search);
        listItemListModel = new ArrayList<>();

        for (int i = 0; i < NameList.size(); i++) {
            ListItemModel listItemModel = new ListItemModel();
            listItemModel.setName(NameList.get(i));

            listItemListModel.add(listItemModel);

        }

        adapter = new SearchListAdapter(listItemListModel, context);
        listView.setAdapter(adapter);

        searchView.setActivated(true);
        searchView.setQueryHint("Select Your "+searchType);
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    adapter.filter("");
                    listView.clearTextFilter();

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

    }

}