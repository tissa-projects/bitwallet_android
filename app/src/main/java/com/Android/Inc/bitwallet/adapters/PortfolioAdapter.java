package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.Activities.PortfolioActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.MyViewHolder> {

    Context context;
    JSONArray jsonArray;
    ArrayList<Integer> colors = null;
    PortfolioActivity myActivity;


    public PortfolioAdapter(Context context, JSONArray jsonArray, ArrayList<Integer> colors) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.colors = colors;
        myActivity = (PortfolioActivity) context;

        Log.e("Adapter", "portfolio");

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        try {
            JSONObject txnListObj = jsonArray.getJSONObject(position);
            holder.txtDot.setVisibility(View.GONE);
            final String strCurrencyType = txnListObj.getString("currency");
            String strWalletName = txnListObj.getString("name");
            if (strWalletName.length() > 8) {
                holder.txtDot.setVisibility(View.VISIBLE);
                strWalletName = strWalletName.substring(0, 8);
                Log.e("strWalletName", strWalletName);
                if (strWalletName.substring(7, 8).equalsIgnoreCase(" ")) {
                    Log.e("strWalletName", strWalletName);
                    strWalletName = strWalletName.substring(0, 7);
                    Log.e("strWalletName", strWalletName);
                }
            }
            final String strWalletId = txnListObj.getString("wallet_id");
            final int isDefaultWallet = txnListObj.getInt("is_default");

            if (isDefaultWallet == 1) {
                Log.e("TAG", "onBindViewHolder: strWalletId" + strWalletId);
                ACU.MySP.saveSP(context, ACU.MySP.IS_DEFAULT_WALLET_ID, strWalletId);
            }
            final String strWalletUniqueId = txnListObj.getString("unique_id");

            double WalletAmt = txnListObj.getDouble("wallet_bal");


            double walletDollerAmt = (WalletAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));


            DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
            final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
            Log.e("StringRecycler", strWalletDollerAmt);
            DecimalFormat numberFormatBtc = new DecimalFormat("#0.00000000");

            //Added by Niraj
            String tempAmount = numberFormatBtc.format(WalletAmt);
            try {
                tempAmount = tempAmount.replaceAll(",", ".");

            } catch (Exception ignore) {
            }
            //-----------------------------
            final String strWalletAmt = tempAmount;


            final String strWallettype = txnListObj.getString("walletType");

            if (holder instanceof MyViewHolder) {
                final MyViewHolder view = (MyViewHolder) holder;
                view.txtCurrencyType.setText(strCurrencyType);
                view.txtWalletName.setText(strWalletName);
                view.txtDollerAmt.setText("$" + strWalletDollerAmt);
                if (!(colors == null)) {
                    view.lineView.setBackgroundColor(colors.get(position));
                }

                view.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMT, strWalletDollerAmt);
                        ACU.MySP.saveSP(context, ACU.MySP.BTC_AMT, strWalletAmt);
                        ACU.MySP.saveSP(context, ACU.MySP.CURRENCY_TYPE, strCurrencyType);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_ID, strWalletId);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_UNIQUE_ID, strWalletUniqueId);
                        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "Portfolio_sendReceive");
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_Type, strWallettype);
                        context.startActivity(new Intent(context, TransactionActivity.class));
                        myActivity.finish();

                    }
                });
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtCurrencyType, txtWalletName, txtDollerAmt, txtDot;
        View lineView;
        LinearLayout ll;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtCurrencyType = (TextView) itemView.findViewById(R.id.txt_currencyType);
            this.txtWalletName = (TextView) itemView.findViewById(R.id.txt_walletName);
            this.txtDollerAmt = (TextView) itemView.findViewById(R.id.txt_dollerAmt);
            this.txtDot = (TextView) itemView.findViewById(R.id.txt_dot);
            this.lineView = itemView.findViewById(R.id.view);
            this.ll = itemView.findViewById(R.id.recycler_h_ll);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_horizontal_portfolio_tablet, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_horizontal_portfolio, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }
}
