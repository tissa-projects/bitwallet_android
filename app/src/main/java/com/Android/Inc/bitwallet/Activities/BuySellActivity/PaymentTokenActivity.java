package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.WebAppInterface;
import com.Android.Inc.bitwallet.utils.ACU;

public class PaymentTokenActivity extends AppCompatActivity implements LifecycleObserver {

    private WebView webView;
    private Context context;
    private String strPublicToken;
    private String screenType,strWyreAccid;
    private boolean minimizeBtnPressed =false;
    private ProgressBar progressBar;
    private static final String TAG = PaymentTokenActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_payment_token_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_payment_token);
        }
        context = PaymentTokenActivity.this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        getIntentData();
        mywebView();

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getIntentData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            strWyreAccid = bundle.getString("wyreAccId");
        }
    }

    private void mywebView() {

        webView = findViewById(R.id.webView);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);
        WebAppInterface webAppInterface = new WebAppInterface(context,strWyreAccid);
        webView.addJavascriptInterface(webAppInterface, "Android");

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e(TAG, "onPageFinished: url: "+url );
                    progressBar.setVisibility(View.GONE);
            }
        });
                webView.loadUrl("file:///android_asset/wyre.html");



    }


    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }
    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");

        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context,SellActivity.class));
        finish();
    }
}
