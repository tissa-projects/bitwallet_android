package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.RetrofitClient.RetrofitClient;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.EmployeeAdapter;
import com.Android.Inc.bitwallet.adapters.FilterNEmployeesAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerEmployee;
    private EmployeeAdapter employeeAdapter;
    private TextView txtTotalBtc, txtTotalTip,txtBtcAmt,txtDollarAmt;
    private JSONArray dataArray;
    private Context context;
    private ProgressBar progressBar;
    private LinearLayout title_bar_left_menu;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn,imgFilter;
    private String status_code = "";

    private String screenType, emp_email = "",emp_code = "",strBalanceDollerAmt,strBalanceBtcAmt;
    private boolean minimizeBtnPressed = false;

    private FilterNEmployeesAdapter filterNEmployeesAdapter = null;

    private API exampleApi;
    private AsyncTask mtaskEmployee = null;
    public static String transactionListUrl = ACU.MySP.MAIN_URL + "business/paymnt_wallet_history";
    public static String exampleUrl = ACU.MySP.MAIN_URL + "employeelist";
    private static final String TAG = EmployeeActivity.class.getSimpleName();


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_employee_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_employee);
        }
        context = EmployeeActivity.this;
        initialize();
        getIntentData();
        initRecycler();
        if (VU.isConnectingToInternet(context,screenType)) {
        //    DevEmployeeData();
            TransactionListData();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGrou3nd");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private void initialize() {
        txtTotalBtc = findViewById(R.id.txt_total_btc);
        txtTotalTip = findViewById(R.id.txt_total_tip);
        txtBtcAmt = findViewById(R.id.txt_btc_amt);
        txtDollarAmt = findViewById(R.id.txt_dollar_amt);
        imgFilter = findViewById(R.id.img_filter);
        recyclerEmployee = findViewById(R.id.recycler_transactions);
        swipeRefreshLayout = findViewById(R.id.transaction_refresh);
        progressBar = findViewById(R.id.emp_pogressbar);
        title_bar_left_menu = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);


        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        title_bar_left_menu.setOnClickListener(this);
        txtTotalTip.setOnClickListener(this);
        txtTotalBtc.setOnClickListener(this);
        imgFilter.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener( this);
    }


    private void initRecycler() {
        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerEmployee.setLayoutManager(linearLayoutManager);
        recyclerEmployee.setHasFixedSize(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
         //   DevEmployeeData();
            TransactionListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_filter:
                showMenu();
                break;
            case R.id.txt_total_btc:
                txtTotalBtc.setTextColor(getResources().getColor(R.color.white));
                txtTotalBtc.setBackground(ContextCompat.getDrawable(context, R.drawable.left_curved_background));
                txtTotalTip.setTextColor(getResources().getColor(R.color.btn_color));
                txtTotalTip.setBackground(ContextCompat.getDrawable(context, R.drawable.right_white_curved_background));
                txtBtcAmt.setText(strBalanceBtcAmt);
                txtDollarAmt.setText(strBalanceDollerAmt);
                break;
            case R.id.txt_total_tip:
                txtTotalTip.setTextColor(getResources().getColor(R.color.white));
                txtTotalTip.setBackground(ContextCompat.getDrawable(context, R.drawable.right_curved_background));
                txtTotalBtc.setTextColor(getResources().getColor(R.color.btn_color));
                txtTotalBtc.setBackground(ContextCompat.getDrawable(context, R.drawable.left_white_curved_background));
                txtBtcAmt.setText("0.000065");
                txtDollarAmt.setText("0.60");
                break;

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SearchEmployeeActivity.class));
        finish();
    }


    private void showMenu() {
        PopupMenu popup = new PopupMenu(this, imgFilter);
        popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menuDaily:
                        Toast.makeText(EmployeeActivity.this, "You clicked Daily", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.menuAllTime:
                        Toast.makeText(EmployeeActivity.this, "You clicked All Time", Toast.LENGTH_SHORT).show();
                        break;

                }
                return true;
            }
        });// to implement on click event on items of menu
        /*MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());*/
        popup.show();

    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        Log.d(TAG, "getIntentData: IN");
        if (extras != null) {
            emp_email = extras.getString("EmpEmail");
            emp_code = extras.getString("EmpCode");

            Log.d(TAG, "getIntentData: "+emp_email+" "+emp_code);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevEmployeeData() {
        progressBar.setVisibility(View.VISIBLE);

        final String str_username = "Abhilesh.kathote@tissatech.com";
        Log.e(TAG, "DevEmployeeData: "+str_username);
        final String str_encode_username = VU.encode(str_username.getBytes());
        Log.e(TAG, "DevEmployeeData: "+str_encode_username);
        final String str_empemail = VU.encode(emp_email.getBytes());
        Log.e(TAG, "DevEmployeeData: "+str_empemail);
        final String str_empCode = VU.encode(emp_code.getBytes());
        final String str_user_type = ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "");
        Log.e(TAG, "DevEmployeeData: "+str_empCode+"  "+str_user_type);

        final String str_encode_user_type = VU.encode(str_user_type.getBytes());

        Log.e(TAG, "DevEmployeeData: IN");
        Log.e(TAG, "DevEmployeeData: "+str_username+" "+emp_code+" "+emp_email);

        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzE2NDM5NDMsInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczMTE1MTcyfQ._xfXGW3Q-vYV-mmMHHDzmfHWtXqYF6d-hTxJWSNvGTGHVd_kpzlrTLf26hoCOZdfD4m5Q6h_h34slIT0S4kLPA";
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().EmployeeData(auth_token, str_encode_username,str_empemail,str_empCode,str_encode_user_type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                  //  String strTip = Obj.getString("tip_amt");
                    txtTotalTip.setText("0.0011");
                    if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                        dataArray = Obj.getJSONArray("data");
                      //  adapterCall();
                    } else {
                        dialogEmployeeListActivity(strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void adapterCall() {

        employeeAdapter = new EmployeeAdapter(context, dataArray);
        recyclerEmployee.setAdapter(employeeAdapter);
    }

    public void dialogEmployeeListActivity(final String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    private void TransactionListData() {
        mtaskEmployee = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null;
                try {                             //username
                    exampleApi = new API(context, VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()),
                            VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes()));
                    s = exampleApi.doGet(transactionListUrl, "PaymentReceivedActivity");
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        Log.e("log", "if");
                        dialogPaymentReceivedSingleLine("Request Timeout.Please Try Again");
                        //  CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        double balance = jsonObject.getDouble("balance");

                        final float walletDollerAmt = (float) (balance * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))); //=======================usd fixed changed==============

                        DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                        strBalanceDollerAmt = numberFormat.format(walletDollerAmt);

                        DecimalFormat walletAmtFormat = new DecimalFormat("#0.00000000");
                        strBalanceBtcAmt = walletAmtFormat.format(balance);

                        txtBtcAmt.setText(strBalanceBtcAmt);
                        txtDollarAmt.setText(strBalanceDollerAmt);

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            dialogSessionExpire("Session Has Been Expired.", "Please Login Again");
                        } else {
                            JSONArray dataListArray = jsonObject.getJSONArray("data");
                            filterNEmployeesAdapter = new FilterNEmployeesAdapter(context, dataListArray,screenType);
                            recyclerEmployee.setAdapter(filterNEmployeesAdapter);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("log", "catch");
                    dialogPaymentReceivedSingleLine("Request Timeout.Please Try Again");
                    // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                }
            }
        }.execute();
    }

    public void dialogPaymentReceivedSingleLine(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if(screenType.equalsIgnoreCase("tablet")){
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else{
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public void dialogSessionExpire(String text1, String text2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if(screenType.equalsIgnoreCase("tablet")){
            dialog.setContentView(R.layout.dialog_two_line_tab);
        } else{
            dialog.setContentView(R.layout.dialog_two_line);
        }
        TextView firstmsgTxt = dialog.findViewById(R.id.txt_firstline);
        firstmsgTxt.setText(text1);
        TextView secondmsgTxt = dialog.findViewById(R.id.txt_secondline);
        secondmsgTxt.setText(text2);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("statusCode", status_code);
                if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                    dialog.dismiss();
                    ACU.MySP.saveSP(context, ACU.MySP.LOGOUT_YES_BTN, "logoutClick");
                    ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS, "");
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                    //   getActivity().finish();
                    // getActivity().onBackPressed();

                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }




}
