package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.BuyUsingDebitCardActivity;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.MoonPayActivity;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.SellActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.TransactionAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

public class TransactionActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener,
        View.OnTouchListener, SwipeRefreshLayout.OnRefreshListener {

    private View layout;
    Context context;
    private TextView txtTotalBitAmt, txtTotalDollerAmt;
    private ProgressBar progressBar;
    ImageView imgDial, imgBarCode;
    LinearLayout ll_backArrow;
    String screenType;

    RecyclerView recyclerTransaction;
    TransactionAdapter transactionAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String status_code = "", strWyreAccId, strWalletId = "";
    private boolean minimizeBtnPressed = false;
    private static final String TAG = TransactionActivity.class.getSimpleName();
    private AsyncTask mtaskWalletTransaction = null, mtaskCheckPaymentAvailable = null, mtaskAddressReferenceId = null;
    public static String transactionListUrl = "business/v2/get_txns_history"; // Prod
    // public static String transactionListUrl = "business/v2/nownode/get_txns_history"; //Testing
    public static String walletDetailsUrl = "business/v2/get-address";
    public static String transactionListUrlcache = "business/v2/get_cache_txn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_transaction_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_transaction);
        }

        context = TransactionActivity.this;
        initialize();
        if (VU.isConnectingToInternet(context, screenType)) {
            TransactionListData();

        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed538
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private void initialize() {
        ACU.MySP.setSPBoolean(context, "isSell", false); // for changing is sell transaction status


        txtTotalBitAmt = findViewById(R.id.txt_totalBitAmt);
        txtTotalDollerAmt = findViewById(R.id.txt_totalDollarAmt);
        progressBar = findViewById(R.id.transaction_progressBar);
        recyclerTransaction = findViewById(R.id.recycler_transactions);
        swipeRefreshLayout = findViewById(R.id.transaction_refresh);
        imgDial = findViewById(R.id.img_dial);
        imgBarCode = findViewById(R.id.img_barcode);

        imgDial.setOnTouchListener(this);
        imgBarCode.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        ll_backArrow.setOnClickListener(this);

        txtTotalBitAmt.setText(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "") + " " + ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, ""));
        txtTotalDollerAmt.setText("$" + " " + ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMT, ""));

        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerTransaction.setLayoutManager(linearLayoutManager);
        recyclerTransaction.setHasFixedSize(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.img_barcode:
                startActivity(new Intent(context, ReceiveCurrencyActivity.class));
                finish();
                break;

        }

    }


    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    private void TransactionListData() {
        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&walletType=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "") +
                            "&unique_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_UNIQUE_ID, "");

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CacheTransactionListData();
//                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status_code.equalsIgnoreCase("405") && status == false) {
                            CacheTransactionListData();
                        }
                        if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strCurrency = dataObj.getString("currency");
                            String strBtcAMt = dataObj.getString("wallet_bal");
                            String btcAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(strBtcAMt);
                            txtTotalBitAmt.setText(strBtcAMt.equals("0") ? "0.00000000" + " " + strCurrency : btcAmt + " " + strCurrency);
                            String strDollarAmt = Miscellaneous.CustomsMethods.convertBtcToDoller(strBtcAMt, context);
                            txtTotalDollerAmt.setText("$" + " " + strDollarAmt);
                            ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, strDollarAmt);
                            JSONArray dataListArray = dataObj.getJSONArray("txns");
                            if (dataListArray != null) {
                                transactionAdapter = new TransactionAdapter(context, dataListArray, screenType);
                                recyclerTransaction.setAdapter(transactionAdapter);
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        }
//                        else {
//                            CustomDialogs.dialogShowMsg(context, screenType, msg);
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onPostExecute: " + e.getMessage());
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    private void CacheTransactionListData() {
        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "");
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrlcache);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strCurrency = dataObj.getString("currency");
                            String strBtcAMt = dataObj.getString("wallet_bal");
                            String btcAmt = Miscellaneous.CustomsMethods.convertToBTCFormat(strBtcAMt);
                            txtTotalBitAmt.setText(strBtcAMt.equals("0") ? "0.00000000" + " " + strCurrency : btcAmt + " " + strCurrency);
                            String strDollarAmt = Miscellaneous.CustomsMethods.convertBtcToDoller(strBtcAMt, context);
                            txtTotalDollerAmt.setText("$" + " " + strDollarAmt);
                            ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, strDollarAmt);
                            JSONArray dataListArray = dataObj.getJSONArray("txns");
                            if (dataListArray != null) {
                                transactionAdapter = new TransactionAdapter(context, dataListArray, screenType);
                                recyclerTransaction.setAdapter(transactionAdapter);
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onPostExecute: " + e.getMessage());
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    //Swipe referesh
    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
            Log.e("inRefresh", "refresh");
            TransactionListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onDestroy() {
        if (mtaskWalletTransaction != null && mtaskWalletTransaction.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskWalletTransaction.cancel(true);
        } else if (mtaskCheckPaymentAvailable != null && mtaskCheckPaymentAvailable.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskCheckPaymentAvailable.cancel(true);
        } else if (mtaskAddressReferenceId != null && mtaskAddressReferenceId.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskAddressReferenceId.cancel(true);
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        if (mtaskWalletTransaction != null && mtaskWalletTransaction.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskWalletTransaction.cancel(true);
        } else if (mtaskCheckPaymentAvailable != null && mtaskCheckPaymentAvailable.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskCheckPaymentAvailable.cancel(true);
        } else if (mtaskAddressReferenceId != null && mtaskAddressReferenceId.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskAddressReferenceId.cancel(true);
        }
        super.onPause();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        float centerX, centerY, touchX, touchY, radius;
        centerX = v.getWidth() / 2;
        centerY = v.getHeight() / 2;
        touchX = event.getX();
        touchY = event.getY();
        radius = centerX;

        if (Math.pow(touchX - centerX, 2)
                + Math.pow(touchY - centerY, 2) < Math.pow(radius, 2)) {
            System.out.println("Inside Circle");
            //code here
            double y1 = ((2 * radius) - touchX);


            if (touchX < centerX && touchY < centerY) {
                if (touchX < touchY) {
                    Log.e("touch", "buy");
                    //  dialogTransactionTwoLine("This Feature Is Not Yet Available", "In Your Country Of Residence");
                   /* ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                    ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                    if (VU.isConnectingToInternet(context, screenType))
                        getWalletDetails();
*/
                    ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "TransactionActivity");
                    // startActivity(new Intent(context, MoonPayActivity.class));

                    Intent intent = new Intent(context, MoonPayActivity.class);
                    intent.putExtra("txt_type", "BUY");
                    startActivity(intent);
                    finish();
                } else {
                    Log.e("touch", "send");
                    try {
                        if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {
                            startActivity(new Intent(context, SendCurrencyActivity.class));
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));
                        }
                    } catch (Exception e) {
                        startActivity(new Intent(context, SendCurrencyActivity.class));

                    }
                }

            } else if (touchX < centerX && touchY > centerY) {
                if (touchY < y1) {
                    Log.e("touch", "buy");
                    // dialogTransactionTwoLine("This Feature Is Not Yet Available", "In Your Country Of Residence");
                    // startActivity(new Intent(context, BuyUsingDebitCardActivity.class));
                    /*ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                    ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                    if (VU.isConnectingToInternet(context, screenType))
                        getWalletDetails();*/
                    ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "TransactionActivity");
                    //startActivity(new Intent(context, MoonPayActivity.class));
                    Intent intent = new Intent(context, MoonPayActivity.class);
                    intent.putExtra("txt_type", "BUY");
                    startActivity(intent);
                    finish();
                } else {
                    Log.e("touch", "request");
                    startActivity(new Intent(context, RequestCurrencyActivity.class));
                    //   finish();
                }

            } else if (touchX > centerX && touchY > centerY) {
                if (touchX > touchY) {
                    Log.e("touch", "sell sell1");
                    if (VU.isConnectingToInternet(context, screenType)) {
                        if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {
                            Log.e("Sell touch", "Btc= " + ACU.MySP.BTC_AMT);
                            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "TransactionActivity");
                            Intent intent = new Intent(context, MoonPayActivity.class);
                            intent.putExtra("txt_type", "SELL");
                            startActivity(intent);
                            finish();
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));

                        }
                    }
                } else {
                    Log.e("touch", "request");
                    startActivity(new Intent(context, RequestCurrencyActivity.class));
                    //  finish();
                }

            } else if (touchX > centerX && touchY < centerY) {
                Log.e("touch", "send");
                if (touchY < y1) {
                    Log.e("touch", "send if ");
                    try {
                        if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {
                            startActivity(new Intent(context, SendCurrencyActivity.class));
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));
                        }
                    } catch (Exception e) {
                        startActivity(new Intent(context, SendCurrencyActivity.class));
                    }
                } else {
                    Log.e("touch", "send else");
                    if (VU.isConnectingToInternet(context, screenType)) {
                        if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {
                            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "TransactionActivity");
                            Intent intent = new Intent(context, MoonPayActivity.class);
                            intent.putExtra("txt_type", "SELL");
                            startActivity(intent);
                            finish();
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));

                        }
                    }
                }

            }
            return false;
        } else {
            System.out.println("Outside Circle");
            return true;
        }
    }


    @Override
    public void onBackPressed() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("Portfolio_sendReceive")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, PortfolioActivity.class));
            finish();
        } else {
            startActivity(new Intent(context, WalletListActivity.class));
            finish();
        }
    }

    private void getWalletDetails() {

        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                strWalletId = ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "");
                Log.e(TAG, "doInBackground: walletId : " + strWalletId);
                try {
                    urlParameters = "txn_type=BUY" + "&empcode=" +
                            "&wallet_id=" + strWalletId + "&wallettype=" + "MAIN";
                    //    urlParameters = "txn_type=BUY" + "&empcode="+"wallettype=MAIN";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(walletDetailsUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground response : " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            Log.e(TAG, "onPostExecute: dataObj :" + dataObj);
                            String strWalletAccId = dataObj.getString("wyre_acc_id");
                            String strTxnType = dataObj.getString("txn_type");
                            String strReferenceId = dataObj.getString("referenceId");
                            String strAddress = dataObj.getString("address");
                            String strBuyWidgetUrl = dataObj.getString("wyre_buy_widget_url");
                            Log.e(TAG, "onPostExecute: strAddress: " + strAddress);
                            Intent intent = new Intent(context, BuyUsingDebitCardActivity.class);
                            intent.putExtra("address", strAddress);
                            intent.putExtra("referenceId", strReferenceId);
                            intent.putExtra("accountId", strWalletAccId);
                            intent.putExtra("buyWidgetUrl", strBuyWidgetUrl);
                            startActivity(intent);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();
    }


}
