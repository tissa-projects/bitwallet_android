package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class SessionListAdapter extends RecyclerView.Adapter<SessionListAdapter.MyViewHolder> {

    Context context;
    JSONArray jsonArray;


    public SessionListAdapter(Context context, JSONArray jsonArray) {
        this.context = context;
        this.jsonArray = jsonArray;


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) holder;

            try {
                final JSONObject sessionListObj = jsonArray.getJSONObject(position);
                final String strIpAddress = sessionListObj.getString("ip_address");
                final String strStrtDate = sessionListObj.getString("started_date");
                final String strLastAccess = sessionListObj.getString("last_access");
                final String strExpiryDate = sessionListObj.getString("expiry_date");
                final String strLogoutDate = sessionListObj.getString("log_out_date");

                holder.txtIpAddressName.setText(strIpAddress);
                holder.txtStartedDate.setText(strStrtDate);
                holder.TxtAccessDate.setText(strLastAccess);
                holder.txtExpiryDate.setText(strExpiryDate);

                if (strLogoutDate.equalsIgnoreCase("")) {
                    holder.txtLogoutDate.setText("N/A");
                } else {
                    holder.txtLogoutDate.setText(strLogoutDate);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtIpAddressName, txtStartedDate, TxtAccessDate, txtExpiryDate, txtLogoutDate;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtIpAddressName = itemView.findViewById(R.id.txt_ipName);
            this.txtStartedDate = itemView.findViewById(R.id.txt_dateName);
            this.TxtAccessDate = itemView.findViewById(R.id.txt_lastAccessdate);
            this.txtExpiryDate = itemView.findViewById(R.id.txt_Expirydate);
            this.txtLogoutDate = itemView.findViewById(R.id.txt_logoutdate);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_sessions_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_sessions, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


}
