package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.RetrofitClient.RetrofitClient;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUpdateEmployeeActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    private static final String TAG = AddUpdateEmployeeActivity.class.getSimpleName();
    public static String exampleUrl = ACU.MySP.MAIN_URL + "employeeadd";
    View layout;
    private Button btnSave;
    private Context context;
    private ProgressBar progressBar;
    private AppCompatEditText edtFname, edtLname, edtEmail, edtMbNo, edtCountryCode, edtEmpPin;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private LinearLayout titleBarBackBtn, llEmpCode;
    private String screenType;
    private boolean minimizeBtnPressed = false;
    private int pass_view = 0;
    private TextView txtHeading, txtEmpCode;
    private String buseremail, empcode, empId, email, contactNo, firstName, lastName, employeePin, countryCode;
    private API exampleApi;
    private AsyncTask mtaskAddEmployee = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_add_update_employee_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_add_update_employee);
        }
        context = AddUpdateEmployeeActivity.this;
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        getIntentData();

        //for eye icon :- Pin  visible/invisible
        EdtPinOnTouch();

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        String code = ACU.MySP.getFromSP(context, ACU.MySP.COUNTRY_CODE, "");
        Log.e(TAG, "onResume: " + code);
        if (!code.equalsIgnoreCase("")) {
            edtCountryCode.setText(code);

        }

        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGrou3nd");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


  /*  @Override
    protected void onResume() {

        if ((ACU.MySP.getFromSP(context, ACU.MySP.COUNTRY_CODE, "")) != "") {
            edtCountryCode.setText(ACU.MySP.getFromSP(context, ACU.MySP.COUNTRY_CODE, ""));
        }
        super.onResume();
    }*/

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            buseremail = extras.getString("buseremail");
            empcode = extras.getString("empcode");
            empId = extras.getString("empId");
            email = extras.getString("email");
            contactNo = extras.getString("contactNo");
            firstName = extras.getString("firstName");
            lastName = extras.getString("lastName");
            employeePin = extras.getString("employeePin");
            countryCode = extras.getString("countryCode");
            Log.e(TAG, "getIntentData: " + countryCode + buseremail + empcode + empId + email + contactNo + firstName + lastName + employeePin);
            edtCountryCode.setText(countryCode);
            edtFname.setText(firstName);
            edtLname.setText(lastName);
            edtEmail.setText(email);
            edtMbNo.setText(contactNo);
            edtEmpPin.setText(employeePin);
            disableEditText(edtFname);
            disableEditText(edtLname);
            disableEditText(edtEmail);
            disableEditText(edtMbNo);
            disableEditText(edtEmpPin);
            edtEmpPin.setClickable(false);
            pass_view = 1;
            edtEmpPin.setTransformationMethod(new PasswordTransformationMethod());       // to make password filed *
            edtEmpPin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);

            btnSave.setText("EDIT");
            txtHeading.setText("EMPLOYEE DETAILS");
            txtEmpCode.setText(empcode);
            llEmpCode.setVisibility(View.VISIBLE);

        } else {
            txtHeading.setText("ADD EMPLOYEE");
            llEmpCode.setVisibility(View.GONE);
        }

    }

    private void initialize() {
        ACU.MySP.saveSP(context, ACU.MySP.COUNTRY_CODE, "");
        txtEmpCode = findViewById(R.id.txt_emp_code);
        llEmpCode = findViewById(R.id.emp_code);
        txtHeading = findViewById(R.id.txt_heading);
        edtFname = findViewById(R.id.edt_fname);
        edtLname = findViewById(R.id.edt_Lname);
        edtEmail = findViewById(R.id.edt_mail_id);
        edtMbNo = findViewById(R.id.edt_mbNo);
        edtEmpPin = findViewById(R.id.edt_emp_pin);
        edtCountryCode = findViewById(R.id.edt_code);
        btnSave = findViewById(R.id.btn_save);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);
        progressBar = findViewById(R.id.add_emp_progressBar);
        edtCountryCode.setFocusable(false);
        edtCountryCode.setClickable(true);


        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);
        edtCountryCode.setOnClickListener(this);


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
               // socialMedia("linkedIn", "https://www.linkedin.com/company/bitwallet%E2%84%A2?trk=similar-companies_org_image");
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;

            case R.id.btn_save:
                if (btnSave.getText().equals("EDIT")) {
                    btnSave.setText("UPDATE");
                    enableEditText(edtFname);
                    enableEditText(edtLname);
                    enableEditText(edtMbNo);
                    enableEditText(edtEmpPin);
                    edtEmpPin.setClickable(true);
                    /*edtEmpPin.setFocusable(false);
                    edtEmpPin.*/
                } else if (btnSave.getText().equals("SAVE")) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        if (Validate()) {
                            //   AddEmpployee();
                            DevAddEmployeeData();
                        }
                    }
                } else if (btnSave.getText().equals("UPDATE")) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        if (Validate()) {
                            DevUpdateEmployeeData();
                        }
                    }
                }
                break;
            case R.id.edt_code:
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                startActivity(new Intent(context, CountryCodeForAddUpdateActivity.class));
                //  finish();
                break;
            case R.id.title_bar_left_menu:
                startActivity(new Intent(context, EmployeeListActivity.class));
                finish();
                break;
        }
    }

    private void disableEditText(EditText mEditText) {
        mEditText.setEnabled(false);
        mEditText.setFocusable(false);
      //  mEditText.setFocusableInTouchMode(false);
    }

    private void enableEditText(EditText mEditText) {
        mEditText.setFocusableInTouchMode(true);
        mEditText.setFocusable(true);
        mEditText.setEnabled(true);
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, EmployeeListActivity.class));
        finish();

        super.onBackPressed();
    }


    public boolean Validate() {
        if (VU.isEmpty(edtFname)) {
            dialogAddUpdateEmployeeActivity("Enter First Name");
            return false;
        } else if (VU.isEmpty(edtLname)) {
            dialogAddUpdateEmployeeActivity("Enter Last Name");
            return false;
        } else if (VU.isEmailId(edtEmail)) {
            dialogAddUpdateEmployeeActivity("Enter Valid Email Address");
            return false;
        } else if (edtMbNo.getText().length() > 12 || edtMbNo.getText().length() < 10) {
            dialogAddUpdateEmployeeActivity("Enter Valid Mobile No");
            return false;
        } else if (VU.isEmpty(edtCountryCode)) {
            dialogAddUpdateEmployeeActivity("Select Country Code");
            return false;
        } else if (VU.isEmpty(edtEmpPin) || (edtEmpPin.length() != 6)) {
            dialogAddUpdateEmployeeActivity("Pin Must Be Equal To 6 Digit");
            return false;
        }
        return true;
    }

    public void dialogAddUpdateEmployeeActivity(final String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    // register Employee
    private void AddEmpployee() {
        progressBar.setVisibility(View.VISIBLE);
        final String str_fName = edtFname.getText().toString();
        final String str_LName = edtLname.getText().toString();
        final String str_encode_email = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_emp_pin = VU.encode(edtEmpPin.getText().toString().getBytes());
        final String str_mbNo_with_country_code = edtCountryCode.getText().toString() + edtMbNo.getText().toString();
        final String str_encode_mbNo_with_country_code = VU.encode(str_mbNo_with_country_code.getBytes());
        final String str_bUser_email = ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "");
        final String str_encode_bUser_email = VU.encode(str_bUser_email.getBytes());

        Log.e(TAG, "AddEmpployee: " + str_encode_email + " " + str_encode_emp_pin + " " + str_mbNo_with_country_code + " " + str_encode_bUser_email + " ");

        mtaskAddEmployee = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null;
                try {
                    exampleApi = new API(context, str_fName, str_LName, str_encode_email, str_encode_mbNo_with_country_code, str_encode_emp_pin, str_encode_bUser_email);
                    s = exampleApi.doGet(exampleUrl, "AddEmployee");
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        dialogAddUpdateEmployeeActivity("Request Timeout.Please Try Again");
                    } else {
                        try {
                            String api_response = s;
                            Log.e(TAG, "onResponse: " + api_response);
                            JSONObject Obj = new JSONObject(api_response);
                            String statusCode = Obj.getString("status_code");
                            String strMessage = Obj.getString("message");
                            if (Obj != null && statusCode.equalsIgnoreCase("0")) {
                                edtFname.setText("");
                                edtLname.setText("");
                                edtEmail.setText("");
                                edtMbNo.setText("");
                                edtCountryCode.setText("");
                                edtEmpPin.setText("");
                                dialogAddUpdateEmployeeActivity(strMessage);
                            } else {
                                dialogAddUpdateEmployeeActivity(strMessage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //    CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    dialogAddUpdateEmployeeActivity("Request Timeout.Please Try Again");
                }
            }
        }.execute();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevAddEmployeeData() {
        progressBar.setVisibility(View.VISIBLE);
        final String str_encode_fName = VU.encode(edtFname.getText().toString().getBytes());
        final String str_encode_LName = VU.encode(edtLname.getText().toString().getBytes());
        final String str_encode_email = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_emp_pin = VU.encode(edtEmpPin.getText().toString().getBytes());
        final String str_encode_mbNo = VU.encode(edtMbNo.getText().toString().getBytes());
        final String str_encode_country_code = VU.encode(edtCountryCode.getText().toString().getBytes());
        final String str_bUser_email = "Abhilesh.kathote@tissatech.com";
        final String str_encode_bUser_email = VU.encode(str_bUser_email.getBytes());
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzIwOTAzMzksInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczNTYxNTY4fQ.l4p8RqyPCBmv6CDz6erXeCWQU0TEZzAR4Pe02k9rqWwOSFh3aNP1xlEL8iTu6FOFba19i9XFugYCW718iSEBSA";
        Log.e(TAG, "AddEmpployee: " + str_encode_email + " " + str_encode_emp_pin + " " + str_encode_mbNo + " " + str_encode_bUser_email + " ");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().AddEmployee(auth_token, str_encode_fName, str_encode_LName, str_encode_email,
                str_encode_mbNo, str_encode_emp_pin, str_encode_bUser_email, str_encode_country_code);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    if (Obj != null && statusCode.equalsIgnoreCase("0")) {
                        edtFname.setText("");
                        edtLname.setText("");
                        edtEmail.setText("");
                        edtMbNo.setText("");
                        edtCountryCode.setText("");
                        edtEmpPin.setText("");
                        dialogAddUpdateEmployeeActivity(strMessage);
                    } else {
                        dialogAddUpdateEmployeeActivity(strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevUpdateEmployeeData() {
        progressBar.setVisibility(View.VISIBLE);
        final String str_encode_fName = VU.encode(edtFname.getText().toString().getBytes());
        final String str_encode_LName = VU.encode(edtLname.getText().toString().getBytes());
        final String str_encode_email = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_emp_pin = VU.encode(edtEmpPin.getText().toString().getBytes());
        final String str_encode_mbNo = VU.encode(edtMbNo.getText().toString().getBytes());
        final String str_encode_country_code = VU.encode(edtCountryCode.getText().toString().getBytes());
        final String str_bUser_email = "Abhilesh.kathote@tissatech.com";
        final String str_encode_bUser_email = VU.encode(str_bUser_email.getBytes());
        final String str_encode_emp_id = VU.encode(empId.getBytes());
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzE2NDM5NDMsInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczMTE1MTcyfQ._xfXGW3Q-vYV-mmMHHDzmfHWtXqYF6d-hTxJWSNvGTGHVd_kpzlrTLf26hoCOZdfD4m5Q6h_h34slIT0S4kLPA";
        Log.e(TAG, "DevUpdateEmployeeData: " + str_encode_email + " " + str_encode_emp_pin + " " + str_encode_mbNo + " " + str_encode_bUser_email + " ");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().EmployeeUpdate(auth_token, str_encode_fName, str_encode_LName, str_encode_mbNo, str_encode_country_code
                , str_encode_bUser_email, str_encode_email, str_encode_emp_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    if (Obj != null && statusCode.equalsIgnoreCase("0")) {
                        btnSave.setText("EDIT");
                        disableEditText(edtFname);
                        disableEditText(edtLname);
                        disableEditText(edtEmail);
                        disableEditText(edtMbNo);
                        disableEditText(edtEmpPin);
                        dialogAddUpdateEmployeeActivity(strMessage);
                    } else {
                        dialogAddUpdateEmployeeActivity(strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    // update Employee
    private void UpdateEmployeeData() {
        progressBar.setVisibility(View.VISIBLE);
        final String str_encode_fName = VU.encode(edtFname.getText().toString().getBytes());
        final String str_encode_LName = VU.encode(edtLname.getText().toString().getBytes());
        final String str_encode_email = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_emp_pin = VU.encode(edtEmpPin.getText().toString().getBytes());
        final String str_encode_mbNo = VU.encode(edtMbNo.getText().toString().getBytes());
        final String str_encode_country_code = VU.encode(edtCountryCode.getText().toString().getBytes());
        final String str_bUser_email = ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "");
        final String str_encode_bUser_email = VU.encode(str_bUser_email.getBytes());
        final String str_encode_emp_id = VU.encode(empId.getBytes());
        Log.e(TAG, "UpdateEmployeeData: " + str_encode_email + " " + str_encode_emp_pin + " " + str_encode_mbNo + " " + str_encode_bUser_email + " ");


        mtaskAddEmployee = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null;
                try {
                    exampleApi = new API(context, str_encode_fName, str_encode_LName, str_encode_mbNo, str_encode_country_code,
                             str_encode_bUser_email, str_encode_email, str_encode_emp_id);
                    s = exampleApi.doGet(exampleUrl, "UpdateEmployee");
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        dialogAddUpdateEmployeeActivity("Request Timeout.Please Try Again");
                    } else {
                        try {
                            String api_response = s;
                            Log.e(TAG, "onResponse: " + api_response);
                            JSONObject Obj = new JSONObject(api_response);
                            String statusCode = Obj.getString("status_code");
                            String strMessage = Obj.getString("message");
                            if (Obj != null && statusCode.equalsIgnoreCase("0")) {
                                btnSave.setText("EDIT");
                                disableEditText(edtFname);
                                disableEditText(edtLname);
                                disableEditText(edtEmail);
                                disableEditText(edtMbNo);
                                disableEditText(edtEmpPin);
                                dialogAddUpdateEmployeeActivity(strMessage);
                            } else {
                                dialogAddUpdateEmployeeActivity(strMessage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //    CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    dialogAddUpdateEmployeeActivity("Request Timeout.Please Try Again");
                }
            }
        }.execute();

    }

    private void EdtPinOnTouch() {
        edtEmpPin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edtEmpPin.getRight() - edtEmpPin.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        if (pass_view == 0) {
                            pass_view = 1;

                            edtEmpPin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_close, 0);

                            //    edtEmpPin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            edtEmpPin.setTransformationMethod(new PasswordTransformationMethod());       // to make password filed *
                            //  edtEmpPin.setTransformationMethod(new PasswordTransformationMethod());


                        } else if (pass_view == 1) {
                            pass_view = 0;
                            edtEmpPin.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_open, 0);

                            // edtEmpPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            edtEmpPin.setTransformationMethod(null);


                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        if (mtaskAddEmployee != null && mtaskAddEmployee.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskAddEmployee.cancel(true);

        }
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        super.onDestroy();
    }


}
