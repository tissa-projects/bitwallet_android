package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;

public class EnableDisableActivity extends AppCompatActivity implements View.OnClickListener {

    private SwitchCompat switchEmp, switchTip;
    private TextView txtEmp, txtTip;
    private Context context;
    private boolean isEmpSwtchOn, isTipSwitchOn;
    private String screenType;
    private LinearLayout ll_drawer;
    private  boolean minimizeBtnPressed = false;
    private static final String TAG = EnableDisableActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_enable_disable_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_enable_disable);
        }
        context = EnableDisableActivity.this;
        initialize();
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        }else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        switchEmp = findViewById(R.id.switch_employee);
        switchTip = findViewById(R.id.switch_tip);
        txtEmp = findViewById(R.id.txt_Employee);
        txtTip = findViewById(R.id.txt_tip);
        ll_drawer = findViewById(R.id.title_bar_left_menu);
        isEmpSwtchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, false);
        isTipSwitchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_TIP_SWITCH_ON, false);

        if (isEmpSwtchOn) {
            switchEmp.setChecked(true);
        } else {
            switchEmp.setChecked(false);
        }
        if (isTipSwitchOn) {
            switchTip.setChecked(true);
        } else {
            switchTip.setChecked(false);
        }

        ll_drawer.setOnClickListener(this);
        txtEmp.setOnClickListener(this);
        switchEmp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ACU.MySP.setSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, isChecked);
                Log.e(TAG, "onCheckedChanged: switchEmp : " + isChecked);
            }
        });

        switchTip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ACU.MySP.setSPBoolean(context, ACU.MySP.IS_TIP_SWITCH_ON, isChecked);
                Log.e(TAG, "onCheckedChanged: switchTip : " + isChecked);
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_Employee:
                isEmpSwtchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, false);
                Log.e(TAG, "onClick: " + isEmpSwtchOn);
                if (isEmpSwtchOn) {
                    startActivity(new Intent(context, EmployeeListActivity.class));
                } else {
                    Toast.makeText(context, "Please On Employee", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.title_bar_left_menu :
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(context, SettingActivity.class));
        finish();
    }
}
