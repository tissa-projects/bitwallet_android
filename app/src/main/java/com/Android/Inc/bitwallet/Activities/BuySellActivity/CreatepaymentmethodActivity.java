package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

public class CreatepaymentmethodActivity extends AppCompatActivity implements LifecycleObserver {

    private Context mContext;
    String publicToken, strWyreAccid, screenType;
    ProgressBar progressBar;
    private boolean minimizeBtnPressed = false;
    private static final String TAG = CreatepaymentmethodActivity.class.getSimpleName();
    public static String createPaymentMtdUrl = "wyre/sell/debit/createpayment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createpaymentmethod);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_createpaymentmethod_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_createpaymentmethod);
        }
        progressBar = findViewById(R.id.progressBar);
        mContext = CreatepaymentmethodActivity.this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        getIntentData();

        if (VU.isConnectingToInternet(mContext,screenType)){
            createPaymentMethod();
        }
    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(mContext, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(mContext, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(mContext, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(mContext, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(mContext, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(mContext, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(mContext, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(mContext, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(mContext, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(mContext, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void getIntentData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            strWyreAccid = bundle.getString("wyreAccId");
            publicToken = bundle.getString("publicToken");
        }
    }

    private void createPaymentMethod() {

        progressBar.setVisibility(View.VISIBLE);
        String urlParameters = "publicToken=" + publicToken + "&paymentMethodType=" + "LOCAL_TRANSFER" +
                "&country=" + "US" + "&wyre_acc_id=" + strWyreAccid;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(createPaymentMtdUrl);
        helper.setUrlParameter(urlParameters);
        RestAsyncTask restAsyncTask = new RestAsyncTask(mContext, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        dialogGoToActivity(mContext, screenType, mContext.getResources().getString(R.string.request_timeout),"sell_Activity");
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        Log.e(TAG, "onPostExecute: " + status_code);
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(mContext, screenType);
                        } else if (status_code.equals("200") && status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            if (dataObj != null) {
                                dialogGoToActivity(mContext, screenType, mContext.getResources().getString(R.string.go_to_upload_document),"upload_Doc_activity");
                            }
                        } else {
                            dialogGoToActivity(mContext, screenType, msg,"sell_Activity");
                        }
                    }
                } catch (Exception e) {
                    dialogGoToActivity(mContext, screenType, mContext.getResources().getString(R.string.request_timeout),"sell_Activity");
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();

    }

    public void dialogGoToActivity(final Context context, final String screenType, String text, final String activityType) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activityType.equalsIgnoreCase("upload_Doc_activity")) {
                    Intent intent = new Intent(mContext, SelectDocumentForUpload.class);
                    intent.putExtra("wyreAccId", strWyreAccid);
                    intent.putExtra("documentStatusType", "0");  //0 :- because 1st time it will upload government proof;
                    startActivity(intent);
                    finish();
                }else if (activityType.equalsIgnoreCase("sell_Activity")) {
                    Intent intent = new Intent(mContext, SellActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
  // dnt have to allow go back
    }
}
