package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.RetrofitClient.RetrofitClient;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.FilterNEmployeesAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterNEmployeesActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver, SwipeRefreshLayout.OnRefreshListener {

    private ImageView imgEmployee, imgFilter, imgSendBtc;
    private Context context;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private LinearLayout ll_drawer;
    private TextView txtTotalBtc, txtTotalTip, txtBtcAmt, txtDollarAmt;
    private LinearLayout ll1;
    private String screenType;
    private String status_code = "", strBalanceBtcAmt, strBalanceDollerAmt;
    private ProgressBar progressBar;
    private boolean minimizeBtnPressed = false;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerTransaction;
    private FilterNEmployeesAdapter filterNEmployeesAdapter = null;

    private API exampleApi;
    private static final String TAG = FilterNEmployeesActivity.class.getSimpleName();
    public static String transactionListUrl = "business/v2/get_txns_history";
    // public static String WithDrawUrl = ACU.MySP.MAIN_URL + "business/withdraw-btc";
    public static String walletDetailsUrl = "business/v2/get_walletList";
    private AsyncTask mtaskWalletTransaction = null, mtaskWithDraw = null;
    private AsyncTask mtaskWalletList = null;
    public static String transactionListUrlcache = "business/v2/get_cache_txn";
    String strbal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_filter_n_employees_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_filter_n_employees);
        }
        // getIntentData();
        context = FilterNEmployeesActivity.this;
        initialize();
        if (VU.isConnectingToInternet(context, screenType)) {
            WalletListData();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    private void getIntentData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                strbal = extras.getString("stramt");
                Log.e("FLAG", " " + strbal);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGrou3nd");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {

        imgEmployee = findViewById(R.id.img_employee);
        imgFilter = findViewById(R.id.img_filter);
        imgSendBtc = findViewById(R.id.img_send_btc);
        txtTotalBtc = findViewById(R.id.txt_total_btc);
        txtTotalTip = findViewById(R.id.txt_total_tip);
        txtBtcAmt = findViewById(R.id.txt_btc_amt);
        txtDollarAmt = findViewById(R.id.txt_dollar_amt);
        progressBar = findViewById(R.id.transaction_progressBar);
        swipeRefreshLayout = findViewById(R.id.transaction_refresh);
        recyclerTransaction = findViewById(R.id.recycler_transactions);
        ll1 = findViewById(R.id.ll1);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        ll_drawer = findViewById(R.id.title_bar_left_menu);



        if ( ACU.MySP.getFromTrantion(context, ACU.MySP.TRASLANG, "") != null) {
            strbal= ACU.MySP.getFromTrantion(context, ACU.MySP.TRASLANG, "");
            strbal = Miscellaneous.CustomsMethods.convertToBTCFormat(strbal);
            txtBtcAmt.setText(strbal.equals("0") ? "0.00000000" : strbal);
            txtDollarAmt.setText(Miscellaneous.CustomsMethods.convertBtcToDoller(strbal, context));
        }


        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerTransaction.setLayoutManager(linearLayoutManager);
        recyclerTransaction.setHasFixedSize(true);


        swipeRefreshLayout.setOnRefreshListener(this);


        imgEmployee.setOnClickListener(this);
        txtTotalBtc.setOnClickListener(this);
        txtTotalTip.setOnClickListener(this);
        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_drawer.setOnClickListener(this);
        imgFilter.setOnClickListener(this);
        imgSendBtc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_employee:
               /* Log.e(TAG, "onClick: emp click");
                startActivity(new Intent(context, SearchEmployeeActivity.class));
                finish();*/
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.This_Feature_Is_Not_Yet_Available));
                break;
            case R.id.img_filter:
                showMenu();
                break;
            case R.id.img_send_btc:
                if (Validate()) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        dialogPaymentReceivedWithDraw(getResources().getString(R.string.Are_You_Sure_Want_To_Withdraw_BTC));

                    }
                }
                break;
            case R.id.txt_total_btc:
                txtTotalBtc.setTextColor(getResources().getColor(R.color.white));
                txtTotalBtc.setBackground(ContextCompat.getDrawable(context, R.drawable.left_curved_background));
                txtTotalTip.setTextColor(getResources().getColor(R.color.btn_color));
                txtTotalTip.setBackground(ContextCompat.getDrawable(context, R.drawable.right_white_curved_background));
                txtBtcAmt.setText(strBalanceBtcAmt);
                txtDollarAmt.setText(strBalanceDollerAmt);
                break;
            case R.id.txt_total_tip:
                /*txtTotalTip.setTextColor(getResources().getColor(R.color.white));
                txtTotalTip.setBackground(ContextCompat.getDrawable(context, R.drawable.right_curved_background));
                txtTotalBtc.setTextColor(getResources().getColor(R.color.btn_color));
                txtTotalBtc.setBackground(ContextCompat.getDrawable(context, R.drawable.left_white_curved_background));
                txtBtcAmt.setText("0.000065");
                txtDollarAmt.setText("0.60");*/
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.This_Feature_Is_Not_Yet_Available));
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
               // socialMedia("linkedIn", "https://www.linkedin.com/company/bitwallet%E2%84%A2");
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                startActivity(new Intent(context, SidePanalActivity.class));
                finish();
                break;
        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    public boolean Validate() {
        if ((Double.valueOf(txtDollarAmt.getText().toString()) < (99))) {
            dialogPaymentReceivedSingleLine(getResources().getString(R.string.minimum_withdraw_amt));
            return false;
        }
        return true;
    }

    public void dialogPaymentReceivedSingleLine(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogPaymentReceivedWithDraw(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_delete_wallet_tab);
        } else {
            dialog.setContentView(R.layout.dialog_delete_wallet);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt);
        Button btnYes = dialog.findViewById(R.id.btn_ok);
        btnYes.setText("YES");
        msgTxt.setText(text);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ACU.MySP.CHECK_FOR_ACTIVITY = "FilterNEMployeesActivity";
                Intent intent = new Intent(context, PinforInnerActivity.class);
                startActivity(intent);
                finish();
                //WithDrawData();

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SidePanalActivity.class));
        finish();
    }

    private void showMenu() {
        PopupMenu popup = new PopupMenu(this, imgFilter);
        popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.menuDaily:
                        Toast.makeText(FilterNEmployeesActivity.this, "You clicked Daily", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.menuAllTime:
                        Toast.makeText(FilterNEmployeesActivity.this, "You clicked All Time", Toast.LENGTH_SHORT).show();
                        break;

                }
                return true;
            }
        });// to implement on click event on items of menu
        /*MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());*/
        popup.show();

    }

    private void TransactionListData(final String walletId, final String walletUniqueId) {
        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + walletId + "&walletType=PAYMENT" + "&unique_id=" + walletUniqueId;

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CacheTransactionListData(walletId);
//                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status_code.equalsIgnoreCase("405") && status == false) {
                            CacheTransactionListData(walletId);

                        }
                        if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strCurrency = dataObj.getString("currency");
                            String strBtcAMt = dataObj.getString("wallet_bal");

                            Log.d("strBtcAMt", strBtcAMt);
                            strBtcAMt = Miscellaneous.CustomsMethods.convertToBTCFormat(strBtcAMt);
                            txtBtcAmt.setText(strBtcAMt.equals("0") ? "0.00000000" : strBtcAMt);
                            txtDollarAmt.setText(Miscellaneous.CustomsMethods.convertBtcToDoller(strBtcAMt, context));
                            JSONArray dataListArray = dataObj.getJSONArray("txns");
                            FilterNEmployeesAdapter filterNEmployeesAdapter = new FilterNEmployeesAdapter(context, dataListArray, screenType);
                            recyclerTransaction.setAdapter(filterNEmployeesAdapter);
                        }
//                        else {
//                            CustomDialogs.dialogShowMsg(context, screenType, msg);
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onPostExecute: " + e.getMessage());
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                }
            }
        }.execute();
    }

    private void CacheTransactionListData(final String walletId) {
        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + walletId;

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrlcache);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strCurrency = dataObj.getString("currency");
                            String strBtcAMt = dataObj.getString("wallet_bal");
                            strBtcAMt = Miscellaneous.CustomsMethods.convertToBTCFormat(strBtcAMt);
                            txtBtcAmt.setText(strBtcAMt.equals("0") ? "0.00000000" : strBtcAMt);
                            txtDollarAmt.setText(Miscellaneous.CustomsMethods.convertBtcToDoller(strBtcAMt, context));
                            JSONArray dataListArray = dataObj.getJSONArray("txns");
                            FilterNEmployeesAdapter filterNEmployeesAdapter = new FilterNEmployeesAdapter(context, dataListArray, screenType);
                            recyclerTransaction.setAdapter(filterNEmployeesAdapter);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onPostExecute: " + e.getMessage());
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                }
            }
        }.execute();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevTransactionListData() {
        progressBar.setVisibility(View.VISIBLE);

        final String str_bUser_email = "Abhilesh.kathote@tissatech.com";
        final String str_encode_bUser_email = VU.encode(str_bUser_email.getBytes());
        final String str_encode_address = VU.encode(str_bUser_email.getBytes());//=======================================
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzE2NDM5NDMsInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczMTE1MTcyfQ._xfXGW3Q-vYV-mmMHHDzmfHWtXqYF6d-hTxJWSNvGTGHVd_kpzlrTLf26hoCOZdfD4m5Q6h_h34slIT0S4kLPA";

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().TransactionListData(auth_token, str_encode_bUser_email, str_encode_address);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    if (Obj != null && statusCode.equalsIgnoreCase("0")) {

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void WalletListData() {
        mtaskWalletList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) + "&walletType=PAYMENT"+"&balance_flag=" + false;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(walletDetailsUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        if (dataObj != null) {
                            JSONArray walletListArray = dataObj.getJSONArray("walletList");
                            Log.e(TAG, "onPostExecute: walletListArray: " + walletListArray);
                            String strWalletId = walletListArray.getJSONObject(0).getString("wallet_id");
                            String strWalletUniqueId = walletListArray.getJSONObject(0).getString("unique_id");
                            TransactionListData(strWalletId, strWalletUniqueId);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }


    //Swipe referesh
    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
            Log.e("inRefresh", "refresh");
            WalletListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }


}
