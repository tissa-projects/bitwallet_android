package com.Android.Inc.bitwallet.utils;

import android.content.Context;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class Miscellaneous {
    private static final String TAG = Miscellaneous.class.getSimpleName();

    public static class CustomsMethods {


        private static int quality = 60;
        private static int density = 80;

        public static String convertBtcToDoller(String btcAmt, Context context) {
            String strWalletDollerAmt = "";
            try {
                double doubleBtcAmt = Double.parseDouble((String.valueOf(btcAmt)));
                Log.e(TAG, "CustomsMethods: convertBtcToDoller: btcAmt : " + doubleBtcAmt);
                Log.e("usd", ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                double walletDollerAmt = (double) ((doubleBtcAmt * Double.parseDouble(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                Log.e(TAG, "CustomsMethods: convertBtcToDoller: walletDollerAmt : " + walletDollerAmt);
                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                strWalletDollerAmt = numberFormat.format(walletDollerAmt);

            } catch (Exception e) {

            }

            return strWalletDollerAmt;
        }

        public static String convertDollerToBtc(String btcAmt, Context context) {
            double doubleBtcAmt = Double.valueOf((String.valueOf(btcAmt)));
            Log.e(TAG, "CustomsMethods: convertBtcToDoller: btcAmt : " + doubleBtcAmt);
            Log.e("usd", ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
            double walletDollerAmt = (double) ((doubleBtcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
            Log.e(TAG, "CustomsMethods: convertBtcToDoller: walletDollerAmt : " + walletDollerAmt);
            DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
            String strWalletDollerAmt = numberFormat.format(walletDollerAmt);

            return strWalletDollerAmt;
        }


        public static String convertDoubleToString(Double value) {
            Log.e(TAG, "convertDoubleToString: double value " + value);
            String fValue = (BigDecimal.valueOf(value).toPlainString());
            Log.e(TAG, "convertDoubleToString: fValue " + fValue);
            fValue = fValue + "0";
            if (fValue.length() > 4) {
                fValue = fValue.substring(0, fValue.length() - 1);
            }
            Log.e(TAG, "convertDoubleToString: fValue: " + fValue);
            return fValue;
        }


        public static String convertToUSDFormat(String value) {
            String convertedValue = "0.00";
            DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
            if (value != null && value.length() > 0){
                convertedValue = numberFormat.format(Double.valueOf(value));
                return convertedValue;
            }
            return convertedValue;
        }

        public static String convertToBTCFormat(String value) {
            Log.e(TAG, "convertToUSDFormat: value: " + value);
            String fValue = (BigDecimal.valueOf(Double.valueOf(value)).toPlainString());
            DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
            String convertedValue = numberFormat.format(Double.valueOf(fValue));
            return convertedValue;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public static String getBase64FromPath(String path) {
            String encodedString = "";
            try {
                File file = new File(path);
                encodedString = Base64.encodeToString(FileUtils.readFileToByteArray(file), Base64.NO_WRAP);

                //   encodedString = VU.encode(FileUtils.readFileToByteArray(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(TAG, "getBase64FromPath: " + encodedString);
            return encodedString;
        }

        public static int calculateAge(Date birthDate) {
            int years = 0;
            int months = 0;
            int days = 0;

            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());

            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);

            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;

            //if month difference is in negative then reduce years by one
            //and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }

            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            Log.d(TAG, "calculateAge: " + months);
            Log.d(TAG, "calculateAge: " + years);
            return years;
        }

        public static String capitalizeFirstLetter(String str) {
            if (str == null || str.isEmpty()) {
                return str;
            }

            return str.substring(0, 1).toUpperCase() + str.substring(1);
        }
    }

    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename which to be converted to string
     * @return String value of File
     * @throws java.io.IOException if error occurs
     */
    public static String loadFileAsString(String filename) throws java.io.IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) buf.append(String.format("%02X:", aMac));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                // Log.e(TAG, "getIPAddress: "+addrs );
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //   boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }
}
