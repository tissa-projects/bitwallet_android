package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

public class SidePanalActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    private TextView txtPortfolio, txtWallets, txtCharts, txtNews, txtSupport, txtViewTransaction, txtSettings;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private Context context;
    private LinearLayout llBitwalletPay;
    private View layout;
    String screenType;
    private boolean minimizeBtnPressed = false;
    public static String walletDetailsUrl = "business/v2/get_walletList";
    private AsyncTask mtaskWalletTransaction = null, mtaskWithDraw = null;
    private AsyncTask mtaskWalletList = null;
    private static final String TAG = FilterNEmployeesActivity.class.getSimpleName();
    private ProgressBar progressBar;
    private String status_code = "";
    String strbal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_side_panal_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_side_panal);
        }
        context = SidePanalActivity.this;
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        initialize();
        if (ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").equalsIgnoreCase("BUSINESS")) {
            llBitwalletPay.setVisibility(View.VISIBLE);
            txtViewTransaction.setVisibility(View.GONE);

            progressBar = findViewById(R.id.transaction_progressBar);
            WalletListData();
        } else {
            llBitwalletPay.setVisibility(View.GONE);
            txtViewTransaction.setVisibility(View.GONE);
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);


    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyAppSidePanal", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSidePanal", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    private void WalletListData() {
        mtaskWalletList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) + "&walletType="+ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "")+ "&balance_flag=" + true;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(walletDetailsUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
//                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (jsonObject.getJSONObject("data") != null) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            JSONArray walletListArray = dataObj.getJSONArray("walletList");

                            strbal = dataObj.getString("total_bal");
                            ACU.MySP.saveTrantion(context, ACU.MySP.TRASLANG, strbal);

                            Log.e(TAG, "onPostExecute: zm: " + strbal);

//                            String strWalletId = walletListArray.getJSONObject(0).getString("wallet_id");
//                            String strWalletUniqueId = walletListArray.getJSONObject(0).getString("unique_id");

                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSidePanal", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    public void initialize() {
        txtPortfolio = findViewById(R.id.txt_portfolio);
        //  txtBitWalletPay = findViewById(R.id.txt_bitWalletPay);
        txtWallets = findViewById(R.id.txt_wallets);
        txtCharts = findViewById(R.id.txt_chart);
        txtNews = findViewById(R.id.txt_news);
        txtSupport = findViewById(R.id.txt_support);
        txtViewTransaction = findViewById(R.id.txt_view_transaction);
        txtSettings = findViewById(R.id.txt_settings);
        llBitwalletPay = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        txtPortfolio.setOnClickListener(this);
        llBitwalletPay.setOnClickListener(this);
        txtWallets.setOnClickListener(this);
        txtCharts.setOnClickListener(this);
        txtNews.setOnClickListener(this);
        txtSupport.setOnClickListener(this);
        txtViewTransaction.setOnClickListener(this);
        txtSettings.setOnClickListener(this);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_portfolio:
                // ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"portfolio");   // to get which fragment is click ---> DashBoard Activity
                startActivity(new Intent(context, PortfolioActivity.class));
                finish();
                break;
            case R.id.title_bar_left_menu:
                // ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"portfolio");   // to get which fragment is click ---> DashBoard Activity
                startActivity(new Intent(context, ChargeBitwalletActivity.class));
                finish();
                break;
            case R.id.txt_wallets:
                //  ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"walletList");
                startActivity(new Intent(context, WalletListActivity.class));
                finish();
                break;
            case R.id.txt_chart:
                //  ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"chart");
                startActivity(new Intent(context, ChartActivity.class));
                finish();
                break;
            case R.id.txt_news:
                //       ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"news");
                startActivity(new Intent(context, NewsActivity.class));
                finish();
                break;
            case R.id.txt_support:
                //      ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"support");
                startActivity(new Intent(context, SupportActivity.class));
                finish();
                break;

            case R.id.txt_view_transaction:
                //      ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"support");
                Intent in_contact = new Intent(context, FilterNEmployeesActivity.class);
             //   in_contact.putExtra("stramt", strbal);
                startActivity(in_contact);
                finish();
                break;
            case R.id.txt_settings:
                //      ACU.MySP.saveSP(context, ACU.MySP.SELECTED_FRAGMENT,"setting");
                startActivity(new Intent(context, SettingActivity.class));
                finish();
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            minimizeBtnPressed = true;
            finish();
        }
    }
}
