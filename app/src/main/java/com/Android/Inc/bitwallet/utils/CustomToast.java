package com.Android.Inc.bitwallet.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.Android.Inc.bitwallet.R;

public class CustomToast {

    public static void custom_Toast(Context context, String toastText,View layout){


        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(toastText);


        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void custom_Toast_CopyKey(Context context, String toastText,View layout){


        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(toastText);


        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}
