package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.Activities.AddUpdateEmployeeActivity;
import com.Android.Inc.bitwallet.R;

import org.json.JSONArray;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.MyViewHolder> {
    private Context context;
    private JSONArray dataArray;
    private boolean isMinusBtnClick;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;
    private static final String TAG = EmployeeListAdapter.class.getSimpleName();

    public EmployeeListAdapter(Context context, JSONArray dataArray,boolean isMinusBtnClick) {
        this.context = context;
        this.dataArray = dataArray;
        this.isMinusBtnClick = isMinusBtnClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_employee_list_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_employee_list, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) holder;
            try {
                if (isMinusBtnClick){
                    view.imgMinus.setVisibility(View.VISIBLE);
                }else {
                    view.imgMinus.setVisibility(View.GONE);
                }
                final String buseremail = dataArray.getJSONObject(i).getString("buseremail");
                final String empcode = dataArray.getJSONObject(i).getString("empcode");
                final String empId = dataArray.getJSONObject(i).getString("empId");
                final String email = dataArray.getJSONObject(i).getString("email");
                final String contactNo = dataArray.getJSONObject(i).getString("contactNo");
                final String firstName = dataArray.getJSONObject(i).getString("firstName");
                final String lastName = dataArray.getJSONObject(i).getString("lastName");
                final String employeePin = dataArray.getJSONObject(i).getString("employeePin");
                final String countryCode = dataArray.getJSONObject(i).getString("countryCode");

                view.txtEmpName.setText(firstName + " " + lastName);

                view.imgMinus.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View v) {
                        recyclerViewItemClickListener.onItemClick(v,i);
                    }
                });

                view.btnViewDetails.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, AddUpdateEmployeeActivity.class);
                        intent.putExtra("buseremail",buseremail);
                        intent.putExtra("empcode",empcode);
                        intent.putExtra("empId",empId);
                        intent.putExtra("email",email);
                        intent.putExtra("contactNo",contactNo);
                        intent.putExtra("firstName",firstName);
                        intent.putExtra("lastName",lastName);
                        intent.putExtra("employeePin",employeePin);
                        intent.putExtra("countryCode",countryCode);
                        context.startActivity(intent);
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: " + e.getMessage());
            }

        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtEmpName;
        Button btnViewDetails ;
        ImageView imgMinus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtEmpName = itemView.findViewById(R.id.txt_emp_name);
            btnViewDetails = itemView.findViewById(R.id.btn_view_details);
            imgMinus = itemView.findViewById(R.id.minus);
        }
    }

    public interface RecyclerViewItemClickListener{
        void onItemClick(View view, int position);
    }


    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener){
        this.recyclerViewItemClickListener=recyclerViewItemClickListener;
    }


}
