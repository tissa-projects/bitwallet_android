package com.Android.Inc.bitwallet.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Android.Inc.bitwallet.Activities.CustomActivities.InnerStateCityActivity;
import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class InnerSearchListAdapter  extends BaseAdapter {
    private List<ListItemModel> listItemListModel;
    Context context;
    InnerStateCityActivity myActivity;
    ArrayList<ListItemModel> nameList;


    public InnerSearchListAdapter(List<ListItemModel> listItemListModel, Context context) {
        this.listItemListModel = listItemListModel;
        this.context = context;
        myActivity = (InnerStateCityActivity) context;
        this.nameList = new ArrayList<ListItemModel>();
        nameList.addAll(listItemListModel);

    }

    @Override
    public int getCount() {
        return listItemListModel.size();
    }

    @Override
    public Object getItem(int position) {
        return listItemListModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();

            Configuration config = context.getResources().getConfiguration();
            if (config.smallestScreenWidthDp >= 600) {
                view = LayoutInflater.from(context).inflate(R.layout.recycler_search_list_tab, null);
            } else {
                view = LayoutInflater.from(context).inflate(R.layout.recycler_search_list, null);
            }

            holder.txtName = view.findViewById(R.id.txt_name);
            holder.rl = view.findViewById(R.id.rl);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.txtName.setText(listItemListModel.get(position).getName());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
                String name = holder.txtName.getText().toString();
                Tools.hideKeyboard(myActivity);
                Intent resultIntent = new Intent();
                resultIntent.putExtra("name", name);
                myActivity.setResult(Activity.RESULT_OK, resultIntent);
                myActivity.finish();
            }
        });


        return view;
    }

    public class ViewHolder {
        TextView txtName;
        RelativeLayout rl;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listItemListModel.clear();
        Log.e("filter", charText);

        if (charText.length() == 0) {
            listItemListModel.addAll(nameList);
        } else {
            Log.e("char", charText);
            ;
            for (ListItemModel listItemModel : nameList) {
                if (listItemModel.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listItemListModel.add(listItemModel);
                }
            }
        }
        notifyDataSetChanged();
    }
}
