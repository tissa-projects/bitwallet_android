package com.Android.Inc.bitwallet.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.BackgroundServices.UsdRateService;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;
import com.alimuzaffar.lib.pin.PinEntryEditText;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import in.arjsna.passcodeview.PassCodeView;

public class PinActivity extends AppCompatActivity implements View.OnClickListener {
    private PinEntryEditText passCodeView;
    private Context context;
    private TextView txtEnterPin, txtREEnterPin;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn, imgBackArrow;
    private ProgressBar progressBar;
    private String checkPinMsg = "";
    private String bussinessUserType;
    private int count = 0;
    ArrayList<String> pinText = null;
    View layout;
    private boolean isSecurityKey = false;
    private String flag = "";
    private String status_code = "";

    private API exampleApi;
    private static final String TAG = PinActivity.class.getSimpleName();

    public static String verifyPasscodeUrl = "passcode_verify";
    public static String HasPasscodeUrl = "has_passcode";
    private AsyncTask mtaskPasscodeVerify = null, mtaskHasPasscode = null;
    private String screenType;

    private PendingIntent pendingIntent;
    private AlarmManager manager;
    private int interval = 500;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_pin_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_pin);
        }
        context = PinActivity.this;
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        initialize();
        forceUpdate();
        bindEvents();
        bussinessUserType = ACU.MySP.getFromSP(context, ACU.MySP.BUSSINESS_USER_TYPE, "");
        if (bussinessUserType.equalsIgnoreCase("admin") || bussinessUserType.equalsIgnoreCase("employee")) {
            imgBackArrow.setVisibility(View.VISIBLE);
        }
//        if (!ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "").equals("")) {
//            UsdRateServiceCall();
//        }

        // findViewById(R.id.bottom_ll).setVisibility(View.GONE);
    }

    private void UsdRateServiceCall() {
        Intent alarmIntent = new Intent(this, UsdRateService.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        //  new UsdRateService(context);


    }

    public void initialize() {
        count = 0;
        passCodeView = (PinEntryEditText) findViewById(R.id.pass_code_view);
        pinText = new ArrayList<>();

        txtEnterPin = findViewById(R.id.enter_pin_text);
        txtREEnterPin = findViewById(R.id.reenter_pin_text);
        imgBackArrow = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        progressBar = findViewById(R.id.pin_progressBar);
        isSecurityKey = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_SECURITY_CODE, true);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        imgBackArrow.setOnClickListener(this);


        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/ShareTechMono-Regular.ttf");
        passCodeView.setTypeface(typeFace);

//        passCodeView.setKeyTextColor(Color.parseColor("#ffffff"));
//        passCodeView.setEmptyDrawable(R.drawable.empty_dots_pinview);
//        passCodeView.setFilledDrawable(R.drawable.filled_dots_pinview);

        txtEnterPin.setVisibility(View.VISIBLE);
        txtREEnterPin.setVisibility(View.GONE);
        imgBackArrow.setVisibility(View.GONE);

        if (!isSecurityKey) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.create_pin));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void bindEvents() {

        passCodeView.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                String text = str.toString();
                if (text.length() == 6) {
                    if (count != 1) {
                        if (!isSecurityKey) {
                            txtEnterPin.setVisibility(View.GONE);
                            txtREEnterPin.setVisibility(View.VISIBLE);
                            passCodeView.setText(null);
                            imgBackArrow.setVisibility(View.VISIBLE);
                        } else {
                            if (VU.isConnectingToInternet(context, screenType)) {
                                count = 0;
                                CreateAndVerifyPin(text);
                            }

                        }
                    }
                    pinText.add(text);
//                    passCodeView.setText(null);

                    count++;
                    if (count == 2) {
                        pinText.add(1, text);
                        if (pinText.get(0).equalsIgnoreCase(pinText.get(1))) {
                            if (VU.isConnectingToInternet(context, screenType)) {
                                CreateAndVerifyPin(text);
                            }
                        } else {
                            count = 1;
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.PIN_Does_Not_Match));
                        }

                        //    passCodeView.setText(null);
                    }

                }
            }
        });
//        passCodeView.setOnTextChangeListener(new PassCodeView.TextChangeListener() {
//            @Override
//            public void onTextChanged(String text) {
//
//                if (text.length() == 6) {
//                    if (count != 1) {
//                        if (!isSecurityKey) {
//                            txtEnterPin.setVisibility(View.GONE);
//                            txtREEnterPin.setVisibility(View.VISIBLE);
//                            imgBackArrow.setVisibility(View.VISIBLE);
//                        } else {
//                            if (VU.isConnectingToInternet(context, screenType)) {
//                                count = 0;
//                                CreateAndVerifyPin(text);
//                            }
//
//                        }
//                    }
//                    pinText.add(text);
//                    passCodeView.reset();
//                    count++;
//                    if (count == 2) {
//                        pinText.add(1, text);
//                        if (pinText.get(0).equalsIgnoreCase(pinText.get(1))) {
//                            if (VU.isConnectingToInternet(context, screenType)) {
//                                CreateAndVerifyPin(text);
//                            }
//                        } else {
//                            count = 1;
//                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.PIN_Does_Not_Match));
//                        }
//
//                        passCodeView.reset();
//                    }
//
//                }
//            }
//        });
    }

    public void socialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            Log.e(TAG, "socialMedia: Pinpage opening1");
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:

                if (bussinessUserType.equalsIgnoreCase("admin") || bussinessUserType.equalsIgnoreCase("employee")) {
                    onBackPressed();
                } else {
                    count = 0;
                    passCodeView.setText(null);
                    pinText = null;
                    pinText = new ArrayList<>();
                    imgBackArrow.setVisibility(View.GONE);
                    txtEnterPin.setVisibility(View.VISIBLE);
                    txtREEnterPin.setVisibility(View.GONE);
                }

                break;

        }
    }

    public void CreateAndVerifyPin(String pinTxt) {

        Log.e("StringPin", pinTxt);
        final String encode_pintxt = VU.encode(pinTxt.getBytes());

        mtaskPasscodeVerify = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "security_code=" + encode_pintxt;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(verifyPasscodeUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    /*Log.e(TAG, "doInBackground: response: " + s);
                    exampleApi = new API(context, encode_pintxt);
                    s = exampleApi.doGet(verifyPasscodeUrl, "PinActivity");*/
                } catch (Throwable ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    if (s == null) {
                        //CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogSessionExpire(context, screenType);
                    } else {
                        JSONObject pinObject = new JSONObject(s);
                        String msg = pinObject.getString("message");
                        status_code = pinObject.getString("status_code");
                        boolean status = pinObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equalsIgnoreCase("800")) {
                            CustomDialogs.dialogAppMantenance(context, screenType);
                        } else if (status) {
                            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
                            if (!isSecurityKey) {
                                txtEnterPin.setVisibility(View.GONE);
                                txtREEnterPin.setVisibility(View.VISIBLE);
                                passCodeView.setText(null);
                                if (ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").equalsIgnoreCase("BUSINESS")) {
                                    startActivity(new Intent(getApplicationContext(), ChargeBitwalletActivity.class));
                                    finish();
                                } else {
                                    startActivity(new Intent(getApplicationContext(), PortfolioActivity.class));
                                    finish();
                                }

                            } else if (isSecurityKey) {
                                if (ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").equalsIgnoreCase("BUSINESS")) {
                                    startActivity(new Intent(getApplicationContext(), ChargeBitwalletActivity.class));
                                    finish();
                                } else {
                                    startActivity(new Intent(getApplicationContext(), PortfolioActivity.class));
                                    finish();
                                }
                            } else {
                                count = 0;
                                if (msg == null || msg.equalsIgnoreCase("null")) {
                                    msg = "Unable to process, Please try after some time";
                                }
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            count = 0;
                            if (msg == null || msg.equalsIgnoreCase("null")) {
                                msg = "Unable to process, Please try after some time";
                            }
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        }.execute();

    }

    @Override
    public void onBackPressed() {
        if (bussinessUserType.equalsIgnoreCase("admin") || bussinessUserType.equalsIgnoreCase("employee")) {
            startActivity(new Intent(context, SelectBusinessUserActivity.class));
            finish();
        } else if (txtREEnterPin.getVisibility() == View.VISIBLE) {
            count = 0;
            passCodeView.setText(null);
            pinText = null;
            pinText = new ArrayList<>();
            imgBackArrow.setVisibility(View.GONE);
            txtEnterPin.setVisibility(View.VISIBLE);
            txtREEnterPin.setVisibility(View.GONE);
        } else {
            doExitApp();
        }
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }



    public void forceUpdate() {
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String currentVersion = packageInfo.versionName;
        new PinActivity.ForceUpdateAsync(currentVersion, PinActivity.this).execute();
    }

    public class ForceUpdateAsync extends AsyncTask<String, String, String> {

        private String latestVersion;
        private String currentVersion;
        private Context context;

        public ForceUpdateAsync(String currentVersion, Context context) {
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.e("playStoreUrl", " : " + "https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en");
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                        .timeout(10000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                latestVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return latestVersion;
        }

        @Override
        protected void onPostExecute(String jsonObject) {
            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!(context instanceof SplashActivity)) {
                        if (!((Activity) context).isFinishing()) {
                            showForceUpdateDialog();
                        }
                    }
                }
            }
            super.onPostExecute(jsonObject);
        }

        public void showForceUpdateDialog() {

            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

            if (screenType.equalsIgnoreCase("tablet")) {
                dialog.setContentView(R.layout.dialog_app_update_tab);
            } else {
                dialog.setContentView(R.layout.dialog_app_update);
            }
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                        dialog.dismiss();
                    }
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }

    }

}
