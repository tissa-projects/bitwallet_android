package com.Android.Inc.bitwallet.ServiceCall;


import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.Android.Inc.bitwallet.Models.CertificateModel;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.SSl_Classes.AuthenticationParameters;
import com.Android.Inc.bitwallet.SSl_Classes.IOUtil;
import com.Android.Inc.bitwallet.SSl_Classes.SSLContextFactory;
import com.Android.Inc.bitwallet.SSl_Classes.TLSSocketFactory;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.FileUtils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import static com.github.barteksc.pdfviewer.util.FileUtils.copy;

public class RestAPIClient {


    public static class APICLient {

        private static final String TAG = API.class.getSimpleName();
        private static Context context;
        private static SSLContext sslContext;
        private static String caCertificateName = "new_crt_cert.crt";
        private static String clientCertificateName = "client_cert.p12";
        private static String clientCertificatePassword = "kSwlosORJ1lw2N";

        //  private static String clientCertificatePassword = "abc@123";


        public static void Auth_ssl() {
            try {
                try {
                    AuthenticationParameters authParams = new AuthenticationParameters();
                    authParams.setClientCertificate(getClientCertFile());
                    authParams.setClientCertificatePassword(clientCertificatePassword);
                    authParams.setCaCertificate(readCaCert());
                    sslContext = SSLContextFactory.getInstance().makeContext(authParams.getClientCertificate(),
                            authParams.getClientCertificatePassword(),
                            authParams.getCaCertificate());
                    CookieHandler.setDefault(new CookieManager());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                Log.e("AuthException", e.getMessage());
            }
        }


        public static String getRemoteCall(RestAPIClientHelper helper, Context ctx) {
            Log.e(TAG, "getRemoteCall: helper: " + helper.toString());
            context = ctx;
            HttpURLConnection connection = null;
            String auth_code = null;
            byte[] postData = null;
            String url = null;
            int postDataLength = 0, lastResponseCode = 0;
            String result = null;
            if (helper != null) {
                try {
                    //  url = (helper.isLiveAPI() ? live_url + helper.getRequestUrl() : dev_url + helper.getRequestUrl());
                    url = ACU.MySP.MAIN_URL + helper.getRequestUrl();
                    Log.e(TAG, "Endpoint_URL : " + url);
                    connection = (HttpURLConnection) new URL(url).openConnection();
                    auth_code = ACU.MySP.getFromSP(context, ACU.MySP.AUTH_TOKEN, "");
                    Log.e(TAG, "getRemoteCall: " + auth_code);
                    if (helper.isLiveAPI()) {
                        if (connection instanceof HttpsURLConnection) {
                            Log.e(TAG, "getRemoteCall: if condition");
                            if (sslContext == null) {
                                Auth_ssl();
                            }
                            ((HttpsURLConnection) connection).setSSLSocketFactory(new TLSSocketFactory(sslContext.getSocketFactory(), ctx));
                        }
                    } else {
                        Log.e(TAG, "getRemoteCall: else condition");
                        //   auth_code = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MThlMzAyMC0yOTI3LTQzNjQtOTYzZS02MjFlMzcwMWVkNzYiLCJpYXQiOjE1ODIyODY5NjQsImV4cCI6MTU4Mzc1ODE5M30.FlNLDuO2Y7W0MhFmII-csEsiadh4DI3qVppt-v6cY4uvwrs-wNTM8mohh5NHhSQdcG6Jgh2nXRQhXSBcgUs3OA";
                        // auth_code = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJhZjExZGJkNi1kNDNjLTRiOWEtOWM4Yy0wOWViOGU4NDhjNDgiLCJpYXQiOjE1ODI2MDk3NDMsInN1YiI6ImhkZmpkIiwiZXhwIjoxNTg0MDgwOTcyfQ.DF8hJgg6JG44gPvZkS7fjLVmqrMd_tdLRK-wR2j2K_0qcFHQCWMZQhnvZPv3nUpdJOx12jJqb199uaImv_hlnQ";
                    }

                    connection.setRequestMethod(helper.getMethodType());
                    // connection.setRequestProperty("Accept", "application/json");
                    //  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("Content-Type", helper.getContentType());
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("auth_token", auth_code);
                    connection.setInstanceFollowRedirects(false);
                    connection.setUseCaches(true);
                    connection.setDoInput(true);
                    Log.e(TAG, "getRemoteCall: urlParameter: " + helper.getUrlParameter());


                    postData = helper.getUrlParameter().getBytes(StandardCharsets.UTF_8);
                    postDataLength = postData.length;
                    Log.e("postdataLength", postDataLength + "");
                    connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));

                    try {
                        Log.d(TAG, "getRemoteCall: connectionData " + connection.getRequestMethod());
                        Log.d(TAG, "getRemoteCall: connectionData " + connection.getRequestProperties().toString());

                    } catch (Exception e) {
                        Log.e(TAG, "getRemoteCall: connectionData " + e.toString());
                    }

                    try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                        wr.write(postData);
                    } catch (Exception e) {
                        Log.e(TAG, " catch : DataOutputStream: " + e.getMessage());
                        e.printStackTrace();
                    }

                    connection.setConnectTimeout(1000);
                    connection.setReadTimeout(1000);

                    lastResponseCode = connection.getResponseCode();
                    ACU.MySP.RESPONSE_CODE = lastResponseCode;
                    //   ACU.MySP.saveSP(ctx,ACU.MySP.RESPONSE_CODE,String.valueOf(lastResponseCode));
                    Log.e(TAG, "getRemoteCall: lastResponseCode: " + lastResponseCode);
                    Log.e(TAG, "getRemoteCall: lastResponseCode:ACU " + ACU.MySP.RESPONSE_CODE);
                    result = IOUtil.readFully(connection.getInputStream());
                    Log.e(TAG, "getRemoteCall: lastResponseCode:ACU " + ACU.MySP.RESPONSE_CODE);
                    Log.e(TAG, "getRemoteCall: result : " + result);

                } catch (Exception ex) {
                    Log.e(TAG, " catch getRemoteCall: " + ex.getMessage());
                    ex.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

            }
            return result;
        }

        private static File getClientCertFile() throws Exception {
            File certificatefile = FileUtils.fileFromAsset(context, clientCertificateName);
            return certificatefile;
        }

        private static String readCaCert() throws Exception {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(caCertificateName);
            return IOUtil.readFully(inputStream);
        }

        // InputStream -> File
        private static void copyInputStreamToFile(InputStream inputStream, File file)
                throws IOException {
            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                int read;
                byte[] bytes = new byte[1024];
                while ((read = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
