package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.MoonPayActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.LockableNestedScrollView;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener, OnChartValueSelectedListener {
    View view, layout;
    View viewAll, viewYear, viewMonth, viewWeek, viewDay;
    Context context;
    Button btnSend, btnReceive;
    ProgressBar progressBar;
    TextView txtUsdRate, txtDate, txtDollerAMt;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_drawer;
    private LineChart lineChart;
    private int displayWidth;
    private LockableNestedScrollView nestedScrollView;
    private int chartFillColor;
    private int chartBorderColor;
    private IndexAxisValueFormatter XAxisFormatter;
    private ArrayList<String> dates = null;
    int lastMinute = 0;
    int currentMinute = 0;
    private AsyncTask mtask = null;
    private String status_code = "";
    String screenType;
    private boolean minimizeBtnPressed = false;
    String strUsdValue;
    private static final String TAG = ChartActivity.class.getSimpleName();
    public static String ChartDaily = "chart_daily";
    public static String ChartWeekly = "chart_weekly";
    public static String ChartMonthly = "chart_monthly";
    public static String ChartYearly = "chart_yearly";
    public static String ChartAllTime = "chart_alltime";


    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_chart_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_chart);
        }

        context = ChartActivity.this;
        initialize();
        updateUSDRate();
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    private void updateUSDRate(){
        strUsdValue = ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE,"");
        if (strUsdValue != null && strUsdValue.length() > 0){
            txtDollerAMt.setText("$" + Miscellaneous.CustomsMethods.convertToUSDFormat(strUsdValue));
        }

    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @RequiresApi(api = 28)
    public void initialize() {
        txtUsdRate = findViewById(R.id.txt_usdRate);
        txtDollerAMt = findViewById(R.id.txt_dollerAmt);
        txtDate = findViewById(R.id.txt_date);
        btnSend = findViewById(R.id.btn_send);
        btnReceive = findViewById(R.id.btn_receive);
        progressBar = findViewById(R.id.progressBar);
        nestedScrollView = findViewById(R.id.graphFragmentNestedScrollView);
        lineChart = findViewById(R.id.chart);

        ll_drawer = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_drawer.setOnClickListener(this);
        setUpChart();
        WindowManager mWinMgr = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        displayWidth = mWinMgr.getDefaultDisplay().getWidth();


        viewAll = findViewById(R.id.view_All);
        viewYear = findViewById(R.id.view_year);
        viewMonth = findViewById(R.id.view_month);
        viewWeek = findViewById(R.id.view_week);
        viewDay = findViewById(R.id.view_day);


        btnSend.setOnClickListener(this);
        btnReceive.setOnClickListener(this);
        findViewById(R.id.ll_all).setOnClickListener(this);
        findViewById(R.id.ll_year).setOnClickListener(this);
        findViewById(R.id.ll_month).setOnClickListener(this);
        findViewById(R.id.ll_week).setOnClickListener(this);
        findViewById(R.id.ll_day).setOnClickListener(this);


        if (VU.isConnectingToInternet(context, screenType)) {
            ChartData(ChartAllTime);
        }

        viewAll.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                goToWalletActivity("buy");
                break;
                //buy
            case R.id.btn_receive:  //sell
               goToWalletActivity("sell");
                break;

            case R.id.ll_all:
                if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
                    mtask.cancel(true);
                }
                viewAll.setVisibility(View.VISIBLE);
                viewYear.setVisibility(View.GONE);
                viewMonth.setVisibility(View.GONE);
                viewWeek.setVisibility(View.GONE);
                viewDay.setVisibility(View.GONE);

                if (VU.isConnectingToInternet(context, screenType)) {
                    txtUsdRate.setText(" ");
                    txtDate.setText(" ");
                    lineChart.clear();
                    ChartData(ChartAllTime);
                }

                break;

            case R.id.ll_year:
                if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
                    mtask.cancel(true);
                }
                viewAll.setVisibility(View.GONE);
                viewYear.setVisibility(View.VISIBLE);
                viewMonth.setVisibility(View.GONE);
                viewWeek.setVisibility(View.GONE);
                viewDay.setVisibility(View.GONE);
                if (VU.isConnectingToInternet(context, screenType)) {
                    txtUsdRate.setText(" ");
                    txtDate.setText(" ");
                    lineChart.clear();
                    ChartData(ChartYearly);
                }

                break;

            case R.id.ll_month:
                if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
                    mtask.cancel(true);
                }
                viewAll.setVisibility(View.GONE);
                viewYear.setVisibility(View.GONE);
                viewMonth.setVisibility(View.VISIBLE);
                viewWeek.setVisibility(View.GONE);
                viewDay.setVisibility(View.GONE);
                if (VU.isConnectingToInternet(context, screenType)) {
                    txtUsdRate.setText(" ");
                    txtDate.setText(" ");
                    lineChart.clear();
                    ChartData(ChartMonthly);
                }

                break;

            case R.id.ll_week:
                if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
                    mtask.cancel(true);
                }
                viewAll.setVisibility(View.GONE);
                viewYear.setVisibility(View.GONE);
                viewMonth.setVisibility(View.GONE);
                viewWeek.setVisibility(View.VISIBLE);
                viewDay.setVisibility(View.GONE);
                if (VU.isConnectingToInternet(context, screenType)) {
                    txtUsdRate.setText(" ");
                    txtDate.setText(" ");
                    lineChart.clear();
                    ChartData(ChartWeekly);
                }


                break;

            case R.id.ll_day:
                if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
                    mtask.cancel(true);
                }
                viewAll.setVisibility(View.GONE);
                viewYear.setVisibility(View.GONE);
                viewMonth.setVisibility(View.GONE);
                viewWeek.setVisibility(View.GONE);
                viewDay.setVisibility(View.VISIBLE);
                if (VU.isConnectingToInternet(context, screenType)) {
                    txtUsdRate.setText(" ");
                    txtDate.setText(" ");
                    lineChart.clear();
                    ChartData(ChartDaily);
                }

                break;

            case R.id.img_facebook:
                SocialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_twitter:
                SocialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.img_linkedIn:
                SocialMedia("linkedIn", ACU.MySP.INSTA_URL);

                break;
            case R.id.title_bar_left_menu:
                onBackPressed();

        }
    }

    private void goToWalletActivity(String s) {
        if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
            mtask.cancel(true);
        }
        Log.e(TAG, "goToWalletActivity: IS_DEFAULT_WALLET_ID: "+ACU.MySP.getFromSP(context, ACU.MySP.IS_DEFAULT_WALLET_ID, "") );
        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "charts_sendReceive");
      if(s.equals("buy")){
          Intent intent = new Intent(context, MoonPayActivity.class);
          intent.putExtra("txt_type", "BUY");
          ACU.MySP.setSPBoolean(context,"isSell", false); // for changing is sell transaction status
          startActivity(intent);
          finish();
      }else {
//          Intent intent = new Intent(context, MoonPayActivity.class);
//          intent.putExtra("txt_type", "SELL");
//          ACU.MySP.setSPBoolean(context,"isSell", true); // for changing is sell transaction status
//          startActivity(intent);
//          finish();

          Intent intent = new Intent(context, WalletListActivity.class);
          ACU.MySP.setSPBoolean(context,"isSell", true);
          startActivity(intent);
          finish();
      }


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, SidePanalActivity.class));
        finish();
    }

    public void SocialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            Log.e("flag", flag);
            Log.e("url", url);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    public void setUpChart() {

        XAxis xAxis = lineChart.getXAxis();
        YAxis yAxis = lineChart.getAxisLeft();
        xAxis.setValueFormatter(XAxisFormatter);
        yAxis.setTextColor(Color.WHITE);
        yAxis.setTextSize(13);
        yAxis.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/ShareTechMono-Regular.ttf"));
        xAxis.setDrawAxisLine(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        xAxis.setAvoidFirstLastClipping(true);
        lineChart.getAxisLeft().setEnabled(true);
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.getDescription().setEnabled(false);
        lineChart.setContentDescription("");
        lineChart.getXAxis().setDrawLabels(false);
        /*lineChart.getAxisRight().setDrawLabels(true);
        lineChart.getAxisLeft().setDrawLabels(false);*/
        lineChart.getAxisLeft().setDrawTopYLabelEntry(true);
        lineChart.setNoDataText("No Data Available For Chart");
        lineChart.setNoDataTextColor(R.color.darkRed);
        lineChart.setOnChartValueSelectedListener(this);
        lineChart.setOnChartValueSelectedListener(this);
        lineChart.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                YAxis yAxis = lineChart.getAxisLeft();
                // Allow scrolling in the right and left margins
                if (me.getX() > yAxis.getLongestLabel().length() * yAxis.getTextSize() &&
                        me.getX() < displayWidth - lineChart.getViewPortHandler().offsetRight()) {
                    //    viewPager.setPagingEnabled(false);
                    nestedScrollView.setScrollingEnabled(false);
                }
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                //  viewPager.setPagingEnabled(true);
                nestedScrollView.setScrollingEnabled(true);

            }

            @Override
            public void onChartLongPressed(MotionEvent me) {

            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {

            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {

            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {

            }
        });
    }

    public LineDataSet setUpLineDataSet(List<Entry> entries) {

        chartFillColor = ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null);
        chartBorderColor = ResourcesCompat.getColor(getResources(), R.color.white, null);

        LineDataSet dataSet = new LineDataSet(entries, "Price");
        dataSet.setColor(chartBorderColor);
        dataSet.setFillColor(chartFillColor);
        dataSet.setDrawHighlightIndicators(true);
        dataSet.setDrawFilled(true);
        dataSet.setDrawCircles(true);
        dataSet.setColors(chartBorderColor);
        dataSet.setCircleColor(chartBorderColor);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawValues(true);
        dataSet.setCircleRadius(1);
        dataSet.setHighlightLineWidth(1);
        dataSet.setHighlightEnabled(true);
        dataSet.setDrawHighlightIndicators(true);
        dataSet.setHighLightColor(chartBorderColor); // color for highlight indicator

        return dataSet;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        DecimalFormat numberFormat = new DecimalFormat("#,###,###,###.00");
        double double_usd = Double.valueOf(e.getY());
        String str_usd = numberFormat.format(double_usd);

        try {
            txtUsdRate.setText("USD " + str_usd);
            txtDate.setText(dates.get(Integer.valueOf((int) e.getX())));
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected() {

    }


    public void chartResponse(String response) {

        final ArrayList<Double> avgValue = new ArrayList<>();
        dates = new ArrayList<>();
        final ArrayList<Entry> values = new ArrayList<>();
        int count = 0;
        int a = 0;

        try {

            String s1 = response;
            JSONObject chartObject = new JSONObject(s1);
            status_code = chartObject.getString("status_code");
            if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                CustomDialogs.dialogSessionExpire(context, screenType);
            } else {
                JSONArray chartDataArray = chartObject.getJSONArray("data");
            }
            //Since we are not getting proper json format its going to catch block and we handling everything in catch block
        } catch (Exception e) {
            String s = e.toString();
            String[] s1 = s.split("Value");
            String cleanString1111 = s1[1].replaceAll("\r", "").replaceAll("\n", "").replaceAll("'\'", "");
            try {
                JSONArray jsonArray = new JSONArray(cleanString1111);

                /*  for(int i =0;i<jsonArray.length();i++) {*/
                for (int i = jsonArray.length() - 1; i > 0; i--) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Log.e("JsonObj", jsonObject.toString());
                    String str_time = jsonObject.getString("time");
                    double str_average = jsonObject.getDouble("average");
                    a = count++;
                    dates.add(a, str_time);
                    avgValue.add(a, str_average);

                }


                for (int i = 0; i < avgValue.size(); i++) {
                    values.add(new Entry(i, Float.valueOf(String.valueOf(avgValue.get(i)))));
                }

                LineDataSet dataSet = setUpLineDataSet(values);
                LineData lineData = new LineData(dataSet);
                lineChart.setData(lineData);
                lineChart.animateX(800);

            } catch (Exception e1) {
                CustomDialogs.dialogRequestTimeOut(context, screenType);
            } finally {
                count = 0;
                a = 0;
                progressBar.setVisibility(View.GONE);
            }

        }
    }


    public void ChartData(final String chartUrl) {

        mtask = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(chartUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                   /* exampleApi = new API(context);
                    s = exampleApi.doGet(chartUrl, "chartActivity");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        chartResponse(s);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);

                }
            }
        }.execute();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
            mtask.cancel(true);
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
            mtask.cancel(true);
        }

    }

    @Override
    public void onStop() {

        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }

        if (mtask != null && mtask.getStatus() != AsyncTask.Status.FINISHED) {
            mtask.cancel(true);
        }

        super.onStop();

    }


}
