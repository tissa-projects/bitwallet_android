package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.Android.Inc.bitwallet.Activities.CustomActivities.CountryCodeActivity;
import com.Android.Inc.bitwallet.Activities.CustomActivities.StateCityActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientStateCity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SignUpIndividualActivity extends AppCompatActivity implements View.OnClickListener {
    private AppCompatEditText edtFname, edtLname, edtEmail, edtpassword, edtConfirmPassword, edtMbNo, edtCountryCode,
            edtCountryName, edtStateName, edtCityName, edtAddress, edtZipCode;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private TextView txtTermsCondition;
    private LinearLayout titleBarBackBtn;
    private RadioButton radioButton;
    private Button btnSubmit;
    private Context context;
    private ProgressBar progressBar;
    private boolean radioBtnState = false;
    View layout, parent_view;
    private AsyncTask mtaskSignUpIndividual = null;
    String screenType, authorization = "";
    private static final int STATE_CODE = 100;
    private static final int CITY_CODE = 200;
    private static final int COUNTRY_CODE = 300;


    private API exampleApi;
    private static final String TAG = SignUpIndividualActivity.class.getSimpleName();

    public static String signUpUrl = "business/v2/individual/register";
    public static String signUpUrlNew = "business/v2/individual/signup";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.activity_sign_up);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_sign_up_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_sign_up);
        }
        context = SignUpIndividualActivity.this;
        intialize();


        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }

    private void intialize() {
        edtFname = findViewById(R.id.edt_fname);
        edtLname = findViewById(R.id.edt_Lname);
        edtEmail = findViewById(R.id.edt_email);
        edtpassword = findViewById(R.id.edt_password);
        edtConfirmPassword = findViewById(R.id.edt_ConfirmPassword);
        edtCountryName = findViewById(R.id.edt_country);
        edtStateName = findViewById(R.id.edt_state);
        edtCityName = findViewById(R.id.edt_city);
        edtAddress = findViewById(R.id.edt_address);
        edtZipCode = findViewById(R.id.edt_zipcode);
        edtMbNo = findViewById(R.id.edt_mbNo);
        edtCountryCode = findViewById(R.id.edt_code);
        progressBar = findViewById(R.id.register_progressBar);
        parent_view = findViewById(android.R.id.content);
        setClickableFocusable();

        edtpassword.setTransformationMethod(new PasswordTransformationMethod());       // to make password filed *
        edtConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());

        ACU.MySP.saveSP(context, ACU.MySP.COUNTRY_CODE, "");


        txtTermsCondition = findViewById(R.id.txt_termsCondition);
        btnSubmit = findViewById(R.id.btn_submit);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);
        radioButton = findViewById(R.id.radio_termsCondition);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);
        txtTermsCondition.setOnClickListener(this);
        radioButton.setOnClickListener(this);
        edtCountryCode.setOnClickListener(this);
        edtCountryName.setOnClickListener(this);
        edtStateName.setOnClickListener(this);
        edtCityName.setOnClickListener(this);

    }

    private void setClickableFocusable() {
        edtCountryCode.setFocusable(false);
        edtCountryCode.setClickable(true);
        edtCountryName.setFocusable(false);
        edtCountryName.setClickable(true);
        edtStateName.setFocusable(false);
        edtStateName.setClickable(true);
        edtCityName.setFocusable(false);
        edtCityName.setClickable(true);
    }


    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;

            case R.id.btn_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        if (edtpassword.getText().toString().trim().equalsIgnoreCase
                                (edtConfirmPassword.getText().toString().trim())) {
                            ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS_BOOLEAN, "");
                            Registration();
                        } else {
                            dialogPasswordNotMatched();
                        }

                    }
                }
                break;

            case R.id.txt_termsCondition:
                ACU.MySP.saveSP(context, ACU.MySP.ARROW_VISIBILITY, "SignUp");
                startActivity(new Intent(context, PdfActivity.class));
                //   finish();
                break;

            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

            case R.id.radio_termsCondition:
                boolean checked = (radioButton).isChecked();

                if (checked) {
                    radioBtnState = true;
                } else
                    radioBtnState = false;
                break;

            case R.id.edt_code:
                startActivityForResult(new Intent(context, CountryCodeActivity.class), COUNTRY_CODE);
                break;

            case R.id.edt_country:
                Log.e(TAG, "onClick: " + edtCountryCode.getText().toString());
                if (edtCountryCode.getText().toString().equalsIgnoreCase("")) {
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Code));
                }
                break;
            case R.id.edt_state:
                Log.e(TAG, "onClick: " + edtCountryName.getText().toString());
                if (!edtCountryName.getText().toString().equalsIgnoreCase("")) {
                    if (VU.isConnectingToInternet(context, screenType))
                        getStateList();
                } else {
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Name));
                }
                break;

            case R.id.edt_city:
                Log.e(TAG, "onClick: " + edtStateName.getText().toString());
                if (!edtStateName.getText().toString().equalsIgnoreCase("")) {
                    if (VU.isConnectingToInternet(context, screenType))
                        getCityList();
                } else {
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_State_Name));
                }
                break;

        }
    }

    @Override
    public void onBackPressed() {
          /*  startActivity(new Intent(context, MainActivity.class));
            finish();*/

        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("login")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, LoginActivity.class));
            finish();
        } else {
            startActivity(new Intent(context, CheckIndividualBussinsActivity.class));
            finish();
        }
        super.onBackPressed();
    }


    public void dialogPasswordNotMatched() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_password_not_matched_tab);
        } else {
            dialog.setContentView(R.layout.dialog_password_not_matched);
        }
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public boolean Validate() {
//        if (VU.isEmpty(edtFname)) {
//            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_First_Name));
//            return false;
//        } else if (VU.isEmpty(edtLname)) {
//            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Last_Name));
//            return false;
//        } else if (edtMbNo.getText().length() > 12 || edtMbNo.getText().length() < 10) {
//            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Valid_Mobile_No));
//            return false;
//        } else if (VU.isEmpty(edtCountryCode)) {
//            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Code));
//            return false;
//        }
        /*else if (VU.isEmpty(edtCountryName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Name));
            return false;
        } else if (VU.isEmpty(edtStateName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_State_Name));
            return false;
        } else if (VU.isEmpty(edtCityName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_City_Name));
            return false;
        } else if (VU.isEmpty(edtAddress)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Address));
            return false;
        } else if (VU.isEmpty(edtZipCode)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Zip_Code));
            return false;
        }*/  if (VU.isEmailId(edtEmail)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Valid_Email_Address));
            return false;
        } else if (VU.isEmpty(edtpassword)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Password));
            return false;
        } else if (edtpassword.getText().length() < 8) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Password_Should_Be_Greater));
            return false;
        } else if (VU.isEmpty(edtConfirmPassword)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Confirm_Password));
            return false;
        } else if (radioBtnState == false) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Terms_Conditions));
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void Registration() {
        progressBar.setVisibility(View.VISIBLE);
        final String str_fName = VU.encode(edtFname.getText().toString().getBytes());
        final String str_LName = VU.encode(edtLname.getText().toString().getBytes());
        final String str_encode_userName = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_email = VU.encode(edtEmail.getText().toString().getBytes());
        final String str_encode_password = VU.encode(edtpassword.getText().toString().getBytes());
        final String str_mbNo_with_country_code = edtCountryCode.getText().toString() + edtMbNo.getText().toString();
        final String str_encode_mbNo_with_country_code = VU.encode(str_mbNo_with_country_code.getBytes());
        final String str_encode_state = VU.encode(edtStateName.getText().toString().getBytes());
        final String str_encode_city = VU.encode(edtCityName.getText().toString().getBytes());
        final String str_encode_address = VU.encode(edtAddress.getText().toString().getBytes());
        final String str_encode_zipcode = VU.encode(edtZipCode.getText().toString().getBytes());
        final String str_encode_country = VU.encode(edtCountryName.getText().toString().getBytes());

        final String str_encode_type = VU.encode("INDIVIDUAL".getBytes());
        mtaskSignUpIndividual = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                  /*  urlParameters = "first_name=" + str_fName + "&last_name=" + str_LName + "&user_name=" + str_encode_userName + "&email_id=" + str_encode_email +
                            "&password=" + str_encode_password + "&mobile_no=" + str_encode_mbNo_with_country_code + "&addressLine=" + str_encode_address +
                            "&city=" + str_encode_city + "&state=" + str_encode_state + "&country=" + str_encode_country + "&zipCode=" + str_encode_zipcode;*/


//                    urlParameters = "first_name=" + str_fName + "&last_name=" + str_LName + "&user_name=" + str_encode_userName + "&email_id=" + str_encode_email +
//                            "&password=" + str_encode_password + "&mobile_no=" + str_encode_mbNo_with_country_code ;

                    urlParameters =  "&user_name=" + str_encode_userName + "&email_id=" + str_encode_email +
                            "&password=" + str_encode_password + "&user_type=" + str_encode_type ;

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(signUpUrlNew);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject signUpObject = new JSONObject(s);
                        String msg = signUpObject.getString("message");
                        boolean status = signUpObject.getBoolean("status");
                        Log.e("DATA", signUpObject.toString());
                        /*if (status.equalsIgnoreCase("true")) {
                            ACU.MySP.saveSP(context, ACU.MySP.REGISTRATION_EMAIL, edtEmail.getText().toString());
                            //startActivity(new Intent(context, OTPActivity.class));
                            startActivity(new Intent(context, LoginActivity.class));
                            finish();
                        }*/
                        if (status) {
                            dialogSignUpActivity(msg);
                        } else {
                            edtEmail.setText("");
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();

    }

    private void getStateList() {

        mtaskSignUpIndividual = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;

                try {
                    String stateReqstUrl = "https://www.universal-tutorial.com/api/states/" + edtCountryName.getText().toString();
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setAuthorization("Bearer " + ACU.MySP.getFromSP(context,ACU.MySP.AUTHORIZATION_TOKEN,""));
                    helper.setMethodType("GET");
                    helper.setRequestUrl(stateReqstUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
//                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);

                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        Log.e(TAG, "onPostExecute: RESPONSE_CODE " + ACU.MySP.RESPONSE_CODE);
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                            Log.e(TAG, "onPostExecute: ===========");
                        } else
                            CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {

                        JSONArray jsonArray = new JSONArray(s);
                        Log.e(TAG, "onPostExecute: " + jsonArray.length());
                        if (jsonArray.length() > 0) {
                            ArrayList<String> nameList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                nameList.add(jsonArray.getJSONObject(i).getString("state_name"));
                            }
                            Intent intent = new Intent(context, StateCityActivity.class);
                            intent.putStringArrayListExtra("name_list", nameList);
                            intent.putExtra("search_type", "State");
                            startActivityForResult(intent, STATE_CODE);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, "State List Not Available For " + edtCountryName.getText().toString());
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }.execute();
    }

    private void getCityList() {

        mtaskSignUpIndividual = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;

                try {
                    String stateReqstUrl = "https://www.universal-tutorial.com/api/cities/" + edtStateName.getText().toString();
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setAuthorization("Bearer " + ACU.MySP.getFromSP(context,ACU.MySP.AUTHORIZATION_TOKEN,""));
                    helper.setMethodType("GET");
                    helper.setRequestUrl(stateReqstUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        Log.e(TAG, "onPostExecute: RESPONSE_CODE " + ACU.MySP.RESPONSE_CODE);
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                        } else
                            CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONArray jsonArray = new JSONArray(s);
                        Log.e(TAG, "onPostExecute: " + jsonArray.length());
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                        } else if (jsonArray.length() > 0) {
                            ArrayList<String> nameList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                nameList.add(jsonArray.getJSONObject(i).getString("city_name"));
                            }
                            Intent intent = new Intent(context, StateCityActivity.class);
                            intent.putStringArrayListExtra("name_list", nameList);
                            intent.putExtra("search_type", "City");
                            startActivityForResult(intent, CITY_CODE);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, "City List Not Available For " + edtStateName.getText().toString());
                        }
                    }
                } catch (Exception e) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();
    }

    private void getAuthorizationToken() {
        mtaskSignUpIndividual = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                String apiToken = "8iEgAhgQtedaE_Y8rTwVusf_uiG6dNyPHM6qMN9C49af2b5ffAJjwh1kiLCfvRE0tGM";

                try {
                    String accessTokenUrl = "https://www.universal-tutorial.com/api/getaccesstoken";
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setApiToken(apiToken);
                    helper.setMethodType("GET");
                    helper.setRequestUrl(accessTokenUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);

                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        // CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e(TAG, "onPostExecute: " + jsonObject.toString());
                        authorization = jsonObject.getString("auth_token");
                        ACU.MySP.saveSP(context,ACU.MySP.AUTHORIZATION_TOKEN,authorization);
                    }
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            String name = data.getStringExtra("name");
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case COUNTRY_CODE:
                        edtCountryName.setText(name);
                        edtCountryCode.setText(data.getStringExtra("code"));
                        edtStateName.setText("");
                        edtCityName.setText("");
                        break;

                    case STATE_CODE:
                        edtStateName.setText(name);
                        edtCityName.setText("");
                        break;

                    case CITY_CODE:
                        edtCityName.setText(name);
                        break;
                }
            }
        }
    }


    public void dialogSignUpActivity(final String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ACU.MySP.saveSP(context, ACU.MySP.REGISTRATION_EMAIL, edtEmail.getText().toString());
                startActivity(new Intent(context, LoginActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
