package com.Android.Inc.bitwallet.SSl_Classes;


import android.content.Context;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.Android.Inc.bitwallet.utils.ACU;
import com.google.android.gms.security.ProviderInstaller;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


/**
 * client-side interface to the back-end application.
 */
public class API {

    private SSLContext sslContext;
    private int lastResponseCode;
    public String s = null;
    public String auth_code = null;
    private boolean isreceiptsend;
    private Context context;
    private String S1 = null, s2 = null, s3 = null, s7 = null, s8 = null, s9 = null, s10 = null, s12 = null, s11 = null,
            s4 = null, s5 = null, s6 = null, walletId = null;
    private String txHash, senderAddr, receiverAddr, timestamp, receipentEmail, billAmt, tipAmt, businessName, timeZone, empcode, bussinessEmail,tipPercent;
    private String buseremail, empId, empEmail, contactNo, firstName, lastName, countryCode;
    private  int amount = 0;
    String caCertificateName = "new_crt_cert.crt";
    String clientCertificateName = "client_cert.p12";
    //String clientCertificatePassword = "G3edKQab93713!#LH"; Old Password
    String clientCertificatePassword = "kSwlosORJ1lw2N";
    ///

    private static final String TAG = API.class.getSimpleName();
    public int getLastResponseCode() {
        return lastResponseCode;
    }

    //profile,walletList,chart,sesssion,logout Activity
    public API(Context context) throws Exception {
        this.context = context;
        Auth_ssl();
    }

    //pin , support,transaction, RenameCreateWallete(deleteAPI), forgetPassword, OTP(ResendOTP)  Activity
    public API(Context context, String S1) throws Exception {
        this.context = context;
        this.S1 = S1;
        Auth_ssl();
    }

    //Login, password, session, Send, SendingCurrency,Business Account, RenameCreateWallet(createWallet,RenameWallet API) Activity
    public API(Context context, String s2, String s3) throws Exception {
        this.context = context;
        this.s2 = s2;
        this.s3 = s3;
        Auth_ssl();
    }

    // requestCurrency Activity
    public API(Context context, String walletId, int amount) throws Exception {
        this.context = context;
        this.walletId = walletId;
        this.amount = amount;
       Auth_ssl();
    }

    //SignUp Activity
    public API(Context context, String s7, String s8, String s9, String s10, String s11, String s12) throws Exception {
        this.context = context;
        this.s7 = s7;
        this.s8 = s8;
        this.s9 = s9;
        this.s10 = s10;
        this.s11 = s11;
        this.s12 = s12;
        Auth_ssl();
    }

    //send , Account ,OTP(verifyOTp)Activity
    public API(Context context, String s4, String s5, String s6) throws Exception {
        this.context = context;
        this.s4 = s4;
        this.s5 = s5;
        this.s6 = s6;
       Auth_ssl();
    }

    // receipt Activity
    public API(Context context, String businessName, String billAmt, String tipAmt, String receipentEmail, String timeZone) throws Exception {
        this.context = context;
        this.businessName = businessName;
        this.billAmt = billAmt;
        this.tipAmt = tipAmt;
        this.receipentEmail = receipentEmail;
        this.timeZone = timeZone;
        Auth_ssl();
    }

    public API(Context context, String txHash, String senderAddr, String receiverAddr,
               String timestamp, String receipentEmail, String billAmt, String tipAmt, String businessName,
               String timeZone, String empEmail, String empcode, String bussinessEmail,String tipPercent,boolean isreceiptsend) {

        this.context = context;
        this.txHash = txHash;
        this.senderAddr = senderAddr;
        this.receiverAddr = receiverAddr;
        this.timestamp = timestamp;
        this.receipentEmail = receipentEmail;
        this.billAmt = billAmt;
        this.tipAmt = tipAmt;
        this.businessName = businessName;
        this.timeZone = timeZone;
        this.empEmail = empEmail;
        this.empcode = empcode;
        this.bussinessEmail = bussinessEmail;
        this.tipPercent = tipPercent;
        this.isreceiptsend = isreceiptsend;
        Auth_ssl();
    }

    // update Employee
    public API(Context context, String firstName, String lastName, String contactNo, String countryCode, String buseremail,
               String empEmail, String empId) {

        this.context = context;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNo = contactNo;
        this.countryCode = countryCode;
        this.buseremail = buseremail;
        this.empEmail = empEmail;
        this.empId = empId;
        this.businessName = businessName;
        this.timeZone = timeZone;
        Auth_ssl();
    }




    public void Auth_ssl() {
        try {
            AuthenticationParameters authParams = new AuthenticationParameters();
            authParams.setClientCertificate(getClientCertFile());
            authParams.setClientCertificatePassword(clientCertificatePassword);
            authParams.setCaCertificate(readCaCert());
            File clientCertFile = authParams.getClientCertificate();
            sslContext = SSLContextFactory.getInstance().makeContext(clientCertFile,
                    authParams.getClientCertificatePassword(), authParams.getCaCertificate());
            CookieHandler.setDefault(new CookieManager());
        } catch (Exception e) {
            Log.e("AuthException", e.getMessage());
        }
    }

    public String doGet(String url, String APIname) throws Exception {

        Log.e("url", url + "   " + APIname);
        String result = null;
        String urlParameters = "";
        byte[] postData = null;
        int postDataLength = 0;
        HttpURLConnection urlConnection = null;
        try {
            URL requestedUrl = new URL(url);
            urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            java.lang.System.setProperty("https.protocols", "TLSv1.2");
            if (!APIname.equalsIgnoreCase("paymentMethodList")) {
                if (urlConnection instanceof HttpsURLConnection) {
                     ((HttpsURLConnection) urlConnection).setSSLSocketFactory(new TLSSocketFactory(sslContext.getSocketFactory(),context));
                   // ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
                }
            }

            auth_code = ACU.MySP.getFromSP(context, ACU.MySP.AUTH_TOKEN, "");
            urlConnection.setDoOutput(true);
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setRequestProperty("auth_token", auth_code);

            Log.e("Activityname", APIname);
            if (APIname.equalsIgnoreCase("login")) {
                urlParameters = "username=" + s2 + "&password=" + s3;
            } else if (APIname.equalsIgnoreCase("SignUpIndividual")) {
                //user_name = email_id
                urlParameters = "first_name=" + s7 + "&last_name=" + s8 + "&user_name=" + s9 + "&email_id=" + s10 +
                        "&password=" + s11 + "&mobile_no=" + s12;
            } else if (APIname.equalsIgnoreCase("SignUpBusiness")) {
                //user_name = email_id
                urlParameters = "businessName=" + s7 + "&mobileNo=" + s8 + "&email=" + s9 +
                        "&address=" + s10 + "&password=" + s11 + "&userName=" + s12;
            } else if (APIname.equalsIgnoreCase("PinActivity")) {
                urlParameters = "security_code=" + S1;
            } else if (APIname.equalsIgnoreCase("supportActivity")) {
                urlParameters = "message=" + S1;
            } else if (APIname.equalsIgnoreCase("profileSetAccountActivity")) {
                urlParameters = "first_name=" + s4 + "&last_name=" + s5 + "&Phone_no=" + s6;
            } else if (APIname.equalsIgnoreCase("PasswordActivity")) {
                urlParameters = "old_pass=" + s2 + "&new_pass=" + s3;
            } else if (APIname.equalsIgnoreCase("SessionActivity")) {
                urlParameters = "page_index=" + s2 + "&page_size=" + s3;
            } else if (APIname.equalsIgnoreCase("transactionActivity")) {
                urlParameters = "wallet_id=" + S1;
            } else if (APIname.equalsIgnoreCase("UseMax_SendActivity")) {
                Log.e("USEMax", "walletId :" + s2 + " " + "amount :" + s3);
                urlParameters = "wallet_id=" + s2 + "&amount=" + s3;
            } else if (APIname.equalsIgnoreCase("getFees_SendActivity")) {
                urlParameters = "wallet_id=" + s4 + "&amount=" + s5 + "&send_to=" + s6;
            } else if (APIname.equalsIgnoreCase("SendingCurrencyActivity")) {
                urlParameters = "raw_hex=" + s2 + "&wallet_txt_id=" + s3;
            } else if (APIname.equalsIgnoreCase("createWallet")) {
                urlParameters = "wallet_type=" + s2 + "&label=" + s3;
            } else if (APIname.equalsIgnoreCase("renameWallet")) {
                urlParameters = "wallet_id=" + s2 + "&label=" + s3;
            } else if (APIname.equalsIgnoreCase("deleteWallet")) {
                urlParameters = "wallet_id=" + S1;
            } else if (APIname.equalsIgnoreCase("RequestCurrencyActivity")) {
                urlParameters = "wallet_id=" + walletId + "&amount=" + amount;
            } else if (APIname.equalsIgnoreCase("ForgetPasswordActivity")) {
                urlParameters = "username=" + S1;
            } else if (APIname.equalsIgnoreCase("ResendOTP")) {
                urlParameters = "user_id=" + S1;
            } else if (APIname.equalsIgnoreCase("verifyOTP")) {
                urlParameters = "otp=" + s4 + "&username=" + s5 + "&type=" + s6;
            } else if (APIname.equalsIgnoreCase("profileSetBusinessAccountActivity")) {
                urlParameters = "businessName=" + s2 + "&address=" + s3;
            } else if (APIname.equalsIgnoreCase("PaymentReceivedActivity")) {
                urlParameters = "username=" + s2 + "&userType=" + s3;
            } else if (APIname.equalsIgnoreCase("WithDrawActivity")) {
                urlParameters = "username=" + s2 + "&userType=" + s3;
            } else if (APIname.equalsIgnoreCase("ReceiptMerchandActivity")) {
                urlParameters = "businessName=" + businessName + "&billAmt=" + billAmt + "&tipAmt=" + tipAmt +
                        "&receipentEmail=" + receipentEmail + "&timeZone=" + timeZone + "&txHash=" + txHash + "&senderAddr=" + senderAddr +
                        "&receiverAddr=" + receiverAddr + "&timestamp=" + timestamp +
                        "&empcode=" + empcode+ "&empemail=" + empEmail+
                        "&merchantemail=" + bussinessEmail+ "&tip_pct=" + tipPercent+ "&isreceiptsend=" + isreceiptsend;
            } else if (APIname.equalsIgnoreCase("resendEmail")) {
                urlParameters = "username=" + S1;
            } else if (APIname.equalsIgnoreCase("AddEmployee")) {  //=====================
                urlParameters = "firstName=" + s7 + "&lastName=" + s8 + "&email=" + s9 +
                        "&contactNo=" + s10 + "&employeePin=" + s11 + "&buseremail =" + s12;
            } else if (APIname.equalsIgnoreCase("UpdateEmployee")) {
                urlParameters ="firstName="+firstName +"&lastName="+lastName +"&email="+empEmail +
                        "&contactNo="+contactNo +"&buseremail ="+buseremail+"&countryCode ="+countryCode;
            }else if (APIname.equalsIgnoreCase("deleteEmployee")) {
                urlParameters ="empId="+empId +"&email="+empEmail +"&buseremail ="+buseremail;
            }

            postData = urlParameters.getBytes(StandardCharsets.UTF_8);
            postDataLength = postData.length;
            Log.e("postdataLength", postDataLength + "");
            urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            urlConnection.setUseCaches(false);
             try (DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream())) {
                wr.write(postData);
            }catch (Exception e){

                 Log.e(TAG, " catch : doGet: "+e.getMessage() );
                 e.printStackTrace();
             }
            urlConnection.setConnectTimeout(60000);
            urlConnection.setReadTimeout(60000);

            lastResponseCode = urlConnection.getResponseCode();
         //   Log.e(TAG, "doGet: clipper suit: " +((HttpsURLConnection) urlConnection).getCipherSuite());
            result = IOUtil.readFully(urlConnection.getInputStream());

        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, " catch 2 doGet: "+ex.getMessage() );
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        Debug.stopMethodTracing();

        return result;
    }

    private File getClientCertFile() throws Exception {

        File file = new File(Environment.getExternalStorageDirectory() + "/" + clientCertificateName);
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(clientCertificateName);
        copyInputStreamToFile(inputStream, file);


        //  File externalStorageDir = new File("android.resource://com.example.sslpining/raw/");
        File externalStorageDir = Environment.getExternalStorageDirectory();
        Log.e("externalStorageDir", externalStorageDir.toString());
        return new File(externalStorageDir, clientCertificateName);
    }


    private String readCaCert() throws Exception {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(caCertificateName);
        return IOUtil.readFully(inputStream);
    }

    // InputStream -> File
    private static void copyInputStreamToFile(InputStream inputStream, File file)
            throws IOException {

        try (FileOutputStream outputStream = new FileOutputStream(file)) {

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }
    }

}
