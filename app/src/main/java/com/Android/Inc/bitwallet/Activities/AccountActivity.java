package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.MoonPayActivity;
import com.Android.Inc.bitwallet.Activities.BuySellActivity.SellActivity;
import com.Android.Inc.bitwallet.Activities.CustomActivities.InnerCountryActivity;
import com.Android.Inc.bitwallet.Activities.CustomActivities.InnerStateCityActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientStateCity;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    private static final String TAG = AccountActivity.class.getSimpleName();

    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    private View layout;
    private ProgressBar progressBar;
    private Context context;
    private AppCompatEditText edtFname, edtLname, edtEmail, edtMbNo, edtBusinessName,
            edtCountryName, edtStateName, edtCityName, edtAddress, edtAddress2, edtZipCode, edtDateOfBirth, edtSsnNo;
    private TextInputLayout businessName, fName, lName;
    private Button btnSave;
    private boolean minimizeBtnPressed = false;
    private String status_code = "", user_type = "", countryShortName = "", countryWithShortName = "", countrycode = "";
    private Date dateBirth = null;
    private AsyncTask mtaskProfileSet = null, mtaskProfileGet = null;
    private RestAsyncTask restAsyncTask = null;
    private String screenType, authorization = "";
    private static final int STATE_CODE = 100;
    private static final int CITY_CODE = 200;
    private static final int COUNTRY_CODE = 300;

    //public static String ProfileSetIndividualUrl = "https://gateway.bitwallet.org/rest/profile_set";
    //public static String profileGetIndividualUrl = "https://gateway.bitwallet.org/rest/profile_get";
    public static String ProfileSetIndividualUrl = "business/v2/individual/profile_set";
    public static String profileGetIndividualUrl = "business/v2/individual/profile_get";
    public static String ProfileSetBusinessUrl = "business/v2/update-profile";
    public static String profileGetBusinessUrl = "business/v2/get-profile";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disableAutofill();
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_account_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_account);
        }

        context = AccountActivity.this;
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        user_type = ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "");

        Log.e(TAG, "onCreate: USER_TYPE:" + user_type);
        if (user_type.equalsIgnoreCase("BUSINESS")) {
            fName.setVisibility(View.GONE);
            lName.setVisibility(View.GONE);
            businessName.setVisibility(View.VISIBLE);
        } else {

            fName.setVisibility(View.VISIBLE);
            lName.setVisibility(View.VISIBLE);
            businessName.setVisibility(View.GONE);
        }

        if (VU.isConnectingToInternet(context, screenType)) {
            profileGet();
        }

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    public void initialize() {
        fName = findViewById(R.id.fname);
        lName = findViewById(R.id.lName);
        businessName = findViewById(R.id.businessName);
        edtEmail = findViewById(R.id.edt_email);
        edtFname = findViewById(R.id.edt_fName);
        edtLname = findViewById(R.id.edt_lName);
        edtMbNo = findViewById(R.id.edt_mobileNo);
        btnSave = findViewById(R.id.btn_save);
        edtAddress = findViewById(R.id.edt_address);
        edtAddress2 = findViewById(R.id.edt_address2);
        edtCountryName = findViewById(R.id.edt_country);
        edtStateName = findViewById(R.id.edt_state);
        edtCityName = findViewById(R.id.edt_city);
        edtZipCode = findViewById(R.id.edt_zipcode);
        edtBusinessName = findViewById(R.id.edt_businessName);
        progressBar = findViewById(R.id.account_progressBar);
        edtDateOfBirth = findViewById(R.id.edt_date_of_birth);
        edtSsnNo = findViewById(R.id.edt_ssn_no);

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);


        edtZipCode.setFilters(new InputFilter[]{
                new InputFilter() {
                    public CharSequence filter(CharSequence src, int start,
                                               int end, Spanned dst, int dstart, int dend) {
                        if (src.equals("")) { // for backspace
                            return src;
                        }
                        if (src.toString().matches("[a-zA-Z 0-9]+")) {
                            return src;
                        }
                        return "";
                    }
                }
        });

//        edtZipCode.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
        edtStateName.setOnClickListener(this);
        edtCityName.setOnClickListener(this);
        edtCountryName.setOnClickListener(this);
        edtDateOfBirth.setOnClickListener(this);
        ssnNOFormat();

        setClickableFocusable();

        btnSave.setOnClickListener(v -> {

//                Log.d(TAG, "onClick: "+edtZipCode.getText().toString().toUpperCase());
            if (VU.isConnectingToInternet(context, screenType)) {
                if (Validate()) {
                    profileSet();
                }
            }
        });
    }

    private void setClickableFocusable() {
        edtStateName.setFocusable(false);
        edtStateName.setClickable(true);
        edtCityName.setFocusable(false);
        edtCityName.setClickable(true);
        edtCountryName.setFocusable(false);
        edtCountryName.setClickable(true);
        edtDateOfBirth.setFocusable(false);
        edtDateOfBirth.setClickable(true);
    }

    public boolean Validate() {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateBirth = sdf.parse(edtDateOfBirth.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user_type.equalsIgnoreCase("BUSINESS")) {
            if (VU.isEmpty(edtBusinessName)) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Business_Name));
                return false;
            }
        } else {
            if (VU.isEmpty(edtFname)) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_First_Name));
                return false;
            } else if (VU.isEmpty(edtLname)) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Last_Name));
                return false;
            }
        }
        if (VU.isEmpty(edtMbNo)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Valid_Mobile_No));
            return false;
        } else if (VU.isEmpty(edtDateOfBirth)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.select_date_birt));
            return false;
        } else if (Miscellaneous.CustomsMethods.calculateAge(dateBirth) < 18) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.age_restriction_msg));
            return false;
        } else if (VU.isEmpty(edtCountryName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Name));
            return false;
        } else if (VU.isEmpty(edtStateName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_State_Name));
            return false;
        } else if (VU.isEmpty(edtCityName)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_City_Name));
            return false;
        } else if (VU.isEmpty(edtAddress)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Address1));
            return false;
        }/*else if (VU.isEmpty(edtAddress2)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Address2));
            return false;
        }*/ else if (VU.isEmpty(edtZipCode)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Zip_Code));
            return false;
        } else if (edtCountryName.getText().toString().equalsIgnoreCase("United States")) {
            if (VU.isEmpty(edtSsnNo)) {
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.enter_ssn_no));
                return false;
            } else if (edtSsnNo.getText().toString().length() > 12) {
                Log.d(TAG, "Validate: aa");
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.valid_ssn_no));
                return false;
//                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.enter_ssn_no));
//                return false;
            } else if (edtSsnNo.getText().toString().length() < 11) {
                Log.d(TAG, "Validate: bb");
                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.valid_ssn_no));
                return false;

//                CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.enter_ssn_no));
//                return false;
            }
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

            case R.id.edt_country:
                Log.e(TAG, "onClick: country click");
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                startActivityForResult(new Intent(context, InnerCountryActivity.class), COUNTRY_CODE);
                break;

            case R.id.edt_state:
                Log.e(TAG, "onClick: " + edtCountryName.getText().toString());
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                if (!edtCountryName.getText().toString().equalsIgnoreCase("")) {
                    if (VU.isConnectingToInternet(context, screenType))
                        getStateList();
                } else {
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_Country_Name));
                }
                break;

            case R.id.edt_city:
                Log.e(TAG, "onClick: " + edtStateName.getText().toString());
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                if (!edtStateName.getText().toString().equalsIgnoreCase("")) {
                    if (VU.isConnectingToInternet(context, screenType))
                        getCityList();
                } else {
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_State_Name));
                }
                break;


            case R.id.edt_date_of_birth:
                selectDate();
                break;


        }

    }

    private void selectDate() {
        try {
            int mYear, mMonth, mDay;
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            edtDateOfBirth.setText(year + "-" + (String.valueOf((monthOfYear + 1)).length() == 1 ? "0" + (monthOfYear + 1) : (monthOfYear + 1))
                                    + "-" + (String.valueOf(dayOfMonth).length() == 1 ? "0" + dayOfMonth : dayOfMonth));
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ssnNOFormat() {
        edtSsnNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // if user is typing string one character at a time
                if (count == 1) {
                    // auto insert dashes while user typing their ssn
                    if (start == 2 || start == 5) {
                        edtSsnNo.setText(edtSsnNo.getText() + "-");
                        edtSsnNo.setSelection(edtSsnNo.getText().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    private void profileGet() {
        mtaskProfileGet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = "";
                try {

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    Log.e(TAG, "profileGet: USER_TYPE:" + user_type);
                    //exampleApi = new API(context);
                    if (user_type.equalsIgnoreCase("BUSINESS")) {
                        // s = exampleApi.doGet(profileGetBusinessUrl, "profileGetAccountActivity");
                        helper.setRequestUrl(profileGetBusinessUrl);
                    } else {
                        // s = exampleApi.doGet(profileGetIndividualUrl, "profileGetAccountActivity");
                        helper.setRequestUrl(profileGetIndividualUrl);
                    }
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else {
                            if (status) {
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                Log.e(TAG, "onPostExecute: dataObj " + dataObj);
                                // user_type = dataObj.getString("userType");
                                String strBusinessName = dataObj.getString("businessName");
                                if (!strBusinessName.equalsIgnoreCase("null")) {
                                    edtBusinessName.setText(strBusinessName);
                                } else {
                                    edtBusinessName.setText("");
                                }
                                String strFName = dataObj.getString("first_name");
                                if (!strFName.equalsIgnoreCase("null")) {
                                    edtFname.setText(strFName);
                                } else {
                                    edtFname.setText("");
                                }

                                String strLName = dataObj.getString("last_name");
                                if (!strLName.equalsIgnoreCase("null")) {

                                    edtLname.setText(strLName);
                                } else {
                                    edtLname.setText("");
                                }


                                if (dataObj.getString("country").equalsIgnoreCase("null") || dataObj.getString("country").equalsIgnoreCase("")) {
                                    edtCountryName.setText("");
                                    edtCountryName.setEnabled(true);
                                }
                                if (user_type.equalsIgnoreCase("BUSINESS")) {
                                    edtAddress.setText(dataObj.getString("businessAddr").equalsIgnoreCase("null") ? "" : dataObj.getString("businessAddr"));
                                } else {
                                    edtAddress.setText(dataObj.getString("addressLine").equalsIgnoreCase("null") ? "" : dataObj.getString("addressLine"));
                                }
                                edtEmail.setText(dataObj.getString("email_id"));
                                if (!dataObj.getString("mobile_no").equalsIgnoreCase("null")) {
                                    edtMbNo.setText(dataObj.getString("mobile_no"));
                                } else {
                                    edtMbNo.setText("");
                                }

                                countryWithShortName = dataObj.getString("country");
                                String strCountry = (countryWithShortName.equalsIgnoreCase("null") || (countryWithShortName.equalsIgnoreCase(""))) ? "" : dataObj.getString("country").split("-")[1];
                                edtCountryName.setText(strCountry);

                                String strDecodedSsnNo = dataObj.getString("ssn").equalsIgnoreCase("null") ? "" : VU.decode(dataObj.getString("ssn"));

                             //   String signature = generator.sign(keyFile, Base64.decode(data, Base64.NO_WRAP));

                                Log.e(TAG, "onPostExecute: " + strDecodedSsnNo);
                                edtSsnNo.setText(strDecodedSsnNo);
                                edtAddress2.setText(dataObj.getString("addressLine2").equalsIgnoreCase("null") ? "" : dataObj.getString("addressLine2"));
                                edtDateOfBirth.setText(dataObj.getString("dob").equalsIgnoreCase("null") ? "" : dataObj.getString("dob"));
                                edtCityName.setText(dataObj.getString("city").equalsIgnoreCase("null") ? "" : dataObj.getString("city"));
                                edtStateName.setText(dataObj.getString("state").equalsIgnoreCase("null") ? "" : dataObj.getString("state"));
                                edtZipCode.setText(dataObj.getString("zipCode").equalsIgnoreCase("null") ? "" : dataObj.getString("zipCode"));
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    private void profileSet() {
        mtaskProfileSet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    final String str_encode_state = edtStateName.getText().toString();
                    final String str_encode_city = edtCityName.getText().toString();
                    final String str_encode_address = edtAddress.getText().toString();
                    final String str_encode_address2 = edtAddress2.getText().toString();
                    final String str_encode_zipcode = edtZipCode.getText().toString().toUpperCase();
                    String str_encode_MbNo = "";
                    if (edtMbNo.getText().toString().contains("+")) {
                        str_encode_MbNo = VU.encode(edtMbNo.getText().toString().getBytes());
                        Log.d(TAG, "doInBackground: w: " + edtMbNo.getText().toString());
                    } else {
                        String w = countrycode + edtMbNo.getText().toString();
                        Log.d(TAG, "doInBackground: w: " + w);
                        str_encode_MbNo = VU.encode(w.getBytes());
                    }


                    Log.e(TAG, "doInBackground: countryWithShortName: " + countryWithShortName);
                    Log.e(TAG, "doInBackground: countryShortName : " + countryShortName + "-" + edtCountryName.getText().toString());
                    String str_encode_country = ((countryWithShortName.equals("null") || countryWithShortName.equals("")) ? countryShortName + "-" + edtCountryName.getText().toString() : countryWithShortName);

                    Log.e(TAG, "doInBackground: " + edtSsnNo.getText().toString());
                    final String str_encode_ssn_no = VU.encode(edtSsnNo.getText().toString().getBytes());
                    final String str_2nd_level_encode_ssn_no = VU.encode(str_encode_ssn_no.getBytes());

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    if (user_type.equalsIgnoreCase("BUSINESS")) {
                        urlParameters = "businessName=" + edtBusinessName.getText().toString() + "&Phone_no=" + str_encode_MbNo
                                + "&businessAddr=" + str_encode_address + "&addressLine2=" + str_encode_address2 + "&city=" + str_encode_city + "&state=" + str_encode_state +
                                "&country=" + str_encode_country + "&zipCode=" + str_encode_zipcode + "&dob=" + edtDateOfBirth.getText().toString() +
                                "&ssn=" + str_2nd_level_encode_ssn_no;
                        helper.setRequestUrl(ProfileSetBusinessUrl);
                    } else {
                        urlParameters = "first_name=" + edtFname.getText().toString() + "&last_name=" + edtLname.getText().toString() + "&Phone_no=" + str_encode_MbNo
                                + "&addressLine=" + str_encode_address + "&addressLine2=" + str_encode_address2 + "&city=" + str_encode_city + "&state=" + str_encode_state +
                                "&country=" + str_encode_country + "&zipCode=" + str_encode_zipcode + "&dob=" + edtDateOfBirth.getText().toString() +
                                "&ssn=" + str_2nd_level_encode_ssn_no;
                        helper.setRequestUrl(ProfileSetIndividualUrl);
                    }
                    Log.e(TAG, "doInBackground: urlParameters " + urlParameters);
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        //  CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equalsIgnoreCase("200")) {
                            dialogShowMsg(msg);  //  go back to previous activity
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    @Override
    public void onDestroy() {
        if (mtaskProfileSet != null && mtaskProfileSet.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskProfileSet.cancel(true);

        } else if (mtaskProfileGet != null && mtaskProfileGet.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskProfileGet.cancel(true);
        }
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("SellActivity")) {
            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
            startActivity(new Intent(context, SellActivity.class));
            finish();
        } else {
            startActivity(new Intent(context, SettingActivity.class));
            finish();
        }
    }


    private void getStateList() {
        mtaskProfileGet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;

                try {
                    String stateReqstUrl = "https://www.universal-tutorial.com/api/states/" + edtCountryName.getText().toString();
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setAuthorization("Bearer " + ACU.MySP.getFromSP(context, ACU.MySP.AUTHORIZATION_TOKEN, ""));
                    helper.setMethodType("GET");
                    helper.setRequestUrl(stateReqstUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
//                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);

                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        Log.e(TAG, "onPostExecute: RESPONSE_CODE " + ACU.MySP.RESPONSE_CODE);
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                            Log.e(TAG, "onPostExecute: ===========");
                        } else
                            CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {

                        JSONArray jsonArray = new JSONArray(s);
                        Log.e(TAG, "onPostExecute: " + jsonArray.length());
                        if (jsonArray.length() > 0) {
                            ArrayList<String> nameList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                nameList.add(jsonArray.getJSONObject(i).getString("state_name"));
                            }
                            Intent intent = new Intent(context, InnerStateCityActivity.class);
                            intent.putStringArrayListExtra("name_list", nameList);
                            intent.putExtra("search_type", "State");
                            startActivityForResult(intent, STATE_CODE);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, "State List Not Available For " + edtCountryName.getText().toString());
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }.execute();
    }

    private void getCityList() {

        mtaskProfileGet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;

                try {
                    String stateReqstUrl = "https://www.universal-tutorial.com/api/cities/" + edtStateName.getText().toString();
                    urlParameters = "";
                    Log.e(TAG, "doInBackground: " + ACU.MySP.getFromSP(context, ACU.MySP.AUTHORIZATION_TOKEN, ""));
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setAuthorization("Bearer " + ACU.MySP.getFromSP(context, ACU.MySP.AUTHORIZATION_TOKEN, ""));
                    helper.setMethodType("GET");
                    helper.setRequestUrl(stateReqstUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        Log.e(TAG, "onPostExecute: RESPONSE_CODE " + ACU.MySP.RESPONSE_CODE);
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                        } else
                            CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONArray jsonArray = new JSONArray(s);
                        Log.e(TAG, "onPostExecute: " + jsonArray.length());
                        if ((ACU.MySP.RESPONSE_CODE) == (500)) {
                            getAuthorizationToken();
                        } else if (jsonArray.length() > 0) {
                            ArrayList<String> nameList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                nameList.add(jsonArray.getJSONObject(i).getString("city_name"));
                            }
                            if (edtStateName.getText().toString().trim().equals("Washington")){
                                nameList.add("Gold Bar");
                            }
                            Intent intent = new Intent(context, InnerStateCityActivity.class);
                            intent.putStringArrayListExtra("name_list", nameList);
                            intent.putExtra("search_type", "City");
                            startActivityForResult(intent, CITY_CODE);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, "City List Not Available For " + edtStateName.getText().toString());
                        }
                    }
                } catch (Exception e) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();
    }

    private void getAuthorizationToken() {
        mtaskProfileGet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                String apiToken = "8iEgAhgQtedaE_Y8rTwVusf_uiG6dNyPHM6qMN9C49af2b5ffAJjwh1kiLCfvRE0tGM";

                try {
                    String accessTokenUrl = "https://www.universal-tutorial.com/api/getaccesstoken";
                    urlParameters = "";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setAccept("application/json");
                    helper.setApiToken(apiToken);
                    helper.setMethodType("GET");
                    helper.setRequestUrl(accessTokenUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClientStateCity.APIClient.getRemoteCall(helper, context);
                    Log.e("s", s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);

                try {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        // CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        Log.e(TAG, "onPostExecute: " + jsonObject.toString());
                        authorization = jsonObject.getString("auth_token");
                        ACU.MySP.saveSP(context, ACU.MySP.AUTHORIZATION_TOKEN, authorization);
                    }
                } catch (Exception e) {

                    e.printStackTrace();

                }
            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            String name = data.getStringExtra("name");
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {

                    case COUNTRY_CODE:
                        countryShortName = data.getStringExtra("countryShortName");
                        countrycode = data.getStringExtra("code");


//                        String code = edtMbNo.getText().toString().trim().substring(0, 1); // checking plus sign --> if plus  sign means country code is there
//
//
//                        if (!(countrycode.trim().substring(0, 1).equalsIgnoreCase(code))) {
//                            edtMbNo.setText(countrycode + edtMbNo.getText().toString());
//                        }

                        edtCountryName.setText(name);
                        edtStateName.setText("");
                        edtCityName.setText("");
                        break;

                    case STATE_CODE:
                        edtStateName.setText(name);
                        edtCityName.setText("");
                        break;

                    case CITY_CODE:
                        edtCityName.setText(name);
                        break;
                }
            }
        }
    }


    public void dialogShowMsg(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("Portfolio_sendReceive") || ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("TransactionActivity") || ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("charts_sendReceive")) {
                    ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
                    if (getIntent().getStringExtra("txt_type")!=null){
                        String txttype = getIntent().getStringExtra("txt_type");
                        Intent intent = new Intent(context, MoonPayActivity.class);
                        intent.putExtra("txt_type", txttype);
                        startActivity(intent);
                        finish();
                    }

//                    startActivity(new Intent(context, MoonPayActivity.class));
//                    finish();
                } else {
                    startActivity(new Intent(context, SettingActivity.class));
                    finish();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    protected void onPause() {
        super.onPause();
        if (restAsyncTask != null && restAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            restAsyncTask.cancel(true);

        }
    }

}
