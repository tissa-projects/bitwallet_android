package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.MoonPayActivity;
import com.Android.Inc.bitwallet.Activities.RenameCreateWalletActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.Activities.WalletListActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class WalletListAdapter extends RecyclerView.Adapter<WalletListAdapter.MyViewHolder> {

    Context context;
    JSONArray jsonArray;
    WalletListActivity myActivity;
    View layout;


    public WalletListAdapter(Context context, JSONArray jsonArray, View layout) {
        this.context = context;
        this.jsonArray = jsonArray;
        this.layout = layout;
        myActivity = (WalletListActivity) context;


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) holder;
            try {
                final JSONObject txnListObj = jsonArray.getJSONObject(position);
                final String strCurrencyType = txnListObj.getString("currency");
                final String strWalletName = txnListObj.getString("name");
                final String strWalletId = txnListObj.getString("wallet_id");
                final String strWalletUniqueId = txnListObj.getString("unique_id");
                final String strWallettype = txnListObj.getString("walletType");
                final int isDefaultWallet = txnListObj.getInt("is_default");

                if (isDefaultWallet == 1) {
                    ACU.MySP.saveSP(context, ACU.MySP.IS_DEFAULT_WALLET_ID, strWalletId);
                }
                 final double WalletAmt = txnListObj.getDouble("wallet_bal");


                final float walletDollerAmt = (float) (WalletAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);

                DecimalFormat walletAmtFormat = new DecimalFormat("#0.00000000");

                //Added by Niraj
                String tempAmount = walletAmtFormat.format(WalletAmt);
                try {
                    tempAmount = tempAmount.replaceAll(",", ".");

                } catch (Exception ignore) {
                }
                //---------------------------------
                final String strWalletAmt = tempAmount;


                holder.txtBitWalletName.setText(strWalletName);
                holder.txtDollarAmt.setText("$ " + strWalletDollerAmt);
                holder.txtBitAmt.setText((strWalletAmt) + " " + strCurrencyType);

                view.rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_LIST_FRAGMENT, "transactionFragment");
                        ACU.MySP.saveSP(context, ACU.MySP.CURRENCY_TYPE, strCurrencyType);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_ID, strWalletId);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_UNIQUE_ID, strWalletUniqueId);
                        Log.e("WalletId : ", strWalletId + " strWalletUniqueId: " + strWalletUniqueId);
                        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMT, strWalletDollerAmt);
                        ACU.MySP.saveSP(context, ACU.MySP.BTC_AMT, strWalletAmt);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_Type, strWallettype);


                        boolean isSell = ACU.MySP.getSPBoolean(context, "isSell", false);

                        Log.e("IsSell Status", "onClick: " + isSell);
                        if (isSell) {

                            if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

                                Intent intent = new Intent(context, MoonPayActivity.class);
                                intent.putExtra("txt_type", "SELL");
                                context.startActivity(intent);

                            } else {

                                String screenType = "";
                                Configuration config = context.getResources().getConfiguration();
                                if (config.smallestScreenWidthDp >= 600) {
                                    screenType = "tablet";
                                } else {
                                    screenType = "mobile";
                                }
                                CustomDialogs.dialogShowMsg(context, screenType, "Insufficient Balance, Please Add BTC To Your Wallet.");
                            }
                        } else {
                            context.startActivity(new Intent(context, TransactionActivity.class));
                            // myActivity.finish();
                        }
                    }
                });

                view.txtEditWallet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ACU.MySP.saveSP(context, ACU.MySP.BTN_CLICK, "walletListAdapter");
                        ACU.MySP.saveSP(context, ACU.MySP.CURRENCY_TYPE, strCurrencyType);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_ID, strWalletId);
                        ACU.MySP.saveSP(context, ACU.MySP.WALLET_NAME, strWalletName);
                        Log.e("DollerAmt", strWalletDollerAmt);
                        ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMT, strWalletDollerAmt);
                        Intent intent = new Intent(context, RenameCreateWalletActivity.class);
                        intent.putExtra("isDefaultWallet", isDefaultWallet);
                        context.startActivity(intent);
                        myActivity.finish();
                        //   loadFragment(new RenameCreateWalletFragment());
                    }
                });
            } catch (Exception e) {

            }
        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

   /* private boolean  loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            myActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("fragment")
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }*/


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtBitWalletName, txtDollarAmt, txtBitAmt, txtEditWallet;
        RelativeLayout rl;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtBitWalletName = itemView.findViewById(R.id.txt_bitWalletName);
            this.txtDollarAmt = itemView.findViewById(R.id.txt_dollarAmt);
            this.txtBitAmt = itemView.findViewById(R.id.txt_bitAmount);
            this.txtEditWallet = itemView.findViewById(R.id.txt_editWallet);
            this.rl = itemView.findViewById(R.id.rl);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_wallets_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_wallets, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


}
