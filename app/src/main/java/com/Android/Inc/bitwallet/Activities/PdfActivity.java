package com.Android.Inc.bitwallet.Activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.github.barteksc.pdfviewer.PDFView;

public class PdfActivity extends AppCompatActivity {

    private ImageView imgArrow;
    PDFView termsConditionPdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_pdf_tab);
        } else {
            setContentView(R.layout.activity_pdf);
        }

        imgArrow = findViewById(R.id.title_bar_left_menu);
        termsConditionPdf = findViewById(R.id.pdfView);
        Log.e("value", ACU.MySP.getFromSP(PdfActivity.this, ACU.MySP.ARROW_VISIBILITY, ""));

        if ((ACU.MySP.getFromSP(PdfActivity.this, ACU.MySP.ARROW_VISIBILITY, "").equalsIgnoreCase("SignUp"))) {
            imgArrow.setVisibility(View.VISIBLE);
        }/*else {
            imgArrow.setVisibility(View.GONE);

        }*/

        imgArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  startActivity(new Intent(PdfActivity.this,SignUpIndividualActivity.class));
                finish();
            }
        });

        termsConditionPdf.fromAsset("BitWallet_Terms_conditions.pdf").load();
    }
}
