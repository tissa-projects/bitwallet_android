package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.Activities.EmployeeListActivity;
import com.Android.Inc.bitwallet.Activities.SearchEmployeeActivity;
import com.Android.Inc.bitwallet.Activities.SelectEmployeeFromListActivity;
import com.Android.Inc.bitwallet.Models.EmployeeItemModel;
import com.Android.Inc.bitwallet.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SelectEmployeeFromListAdapter extends RecyclerView.Adapter<SelectEmployeeFromListAdapter.MyViewHolder> {

    private Context context;
    private JSONArray dataArray;
    private SelectEmployeeFromListActivity myActivity;
    private List<EmployeeItemModel> employeeItemModelList;
    private ArrayList<EmployeeItemModel> employeeItemModels;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;

    private static final String TAG = SelectEmployeeFromListAdapter.class.getSimpleName();


    public SelectEmployeeFromListAdapter(Context context, List<EmployeeItemModel> employeeItemModelList) {
        this.context = context;
        this.employeeItemModelList = employeeItemModelList;

        myActivity = (SelectEmployeeFromListActivity) context;
        this.employeeItemModels = new ArrayList<EmployeeItemModel>();
        employeeItemModels.addAll(employeeItemModelList);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_search_employee_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_search_employee, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) holder;
            try {

              /*  final String buseremail = dataArray.getJSONObject(i).getString("buseremail");
                final String empcode = dataArray.getJSONObject(i).getString("empcode");
                final String firstName = dataArray.getJSONObject(i).getString("firstName");
                final String lastName = dataArray.getJSONObject(i).getString("lastName");
                final String empId = dataArray.getJSONObject(i).getString("empId");
                final String email = dataArray.getJSONObject(i).getString("email");
                final String contactNo = dataArray.getJSONObject(i).getString("contactNo");
                final String employeePin = dataArray.getJSONObject(i).getString("employeePin");
                final String countryCode = dataArray.getJSONObject(i).getString("countryCode");*/

                view.txtEmpName.setText(employeeItemModelList.get(i).getEmpName());
                view.txtEmpId.setText(employeeItemModelList.get(i).getEmpId());

                view.ll.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onClick(View v) {
                        recyclerViewItemClickListener.onItemClick(v, i,employeeItemModelList.get(i).getEmpId(),employeeItemModelList.get(i).getEmpEmail());
                    }
                });

            }catch (Exception e){
                Log.e(TAG, "onBindViewHolder: "+e.getMessage() );
            }
        }
    }

    @Override
    public int getItemCount() {
        return employeeItemModelList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtEmpName,txtEmpId;
        LinearLayout ll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtEmpName = itemView.findViewById(R.id.txt_emp_name);
            txtEmpId = itemView.findViewById(R.id.txt_emp_id);
            ll = itemView.findViewById(R.id.ll);
        }
    }

    public interface RecyclerViewItemClickListener{
        void onItemClick(View view, int position,String empId,String empEmail);
    }


    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener){
        this.recyclerViewItemClickListener=recyclerViewItemClickListener;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        employeeItemModelList.clear();
        Log.e("filter", charText);

        if (charText.length() == 0) {
            employeeItemModelList.addAll(employeeItemModels);
        } else {
            Log.e("char", charText);
            ;
            for (EmployeeItemModel employeeItemModel : employeeItemModels) {
                Log.e("itemList", "Emp ID : "+employeeItemModel.getEmpId() + "  Emp Name : " + employeeItemModel.getEmpName());
                if (employeeItemModel.getEmpName().toLowerCase(Locale.getDefault()).contains(charText) ||
                        String.valueOf(employeeItemModel.getEmpId()).toLowerCase(Locale.getDefault()).contains(charText)) {
                    employeeItemModelList.add(employeeItemModel);
                    Log.e("Emp Name List", employeeItemModel.getEmpName());
                }
            }
        }

        notifyDataSetChanged();
    }

}
