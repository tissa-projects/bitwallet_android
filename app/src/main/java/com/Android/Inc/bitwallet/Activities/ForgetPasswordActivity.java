package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;


public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatEditText edtEmail;
    private ImageView imgFacebook, imgTwitter, imgLinkedIn;
    LinearLayout titleBarBackBtn;
    private Context context;
    private TextView txtSubmit;
    private ProgressBar progressBar;
    private View layout;
    private String status_code = "";
    private API exampleApi;
    private static final String TAG = ForgetPasswordActivity.class.getSimpleName();
    public static String forgetpasswordUrl = "forget";
    private RestAsyncTask restAsyncTask = null;
    String screenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_forget_password_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_forget_password);
        }
        context = ForgetPasswordActivity.this;
        edtEmail = findViewById(R.id.edt_email);

        txtSubmit = findViewById(R.id.txt_submit);
        imgFacebook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);
        progressBar = findViewById(R.id.forgetPassword_progressBar);


        imgFacebook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        txtSubmit.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.txt_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        forgetPasswordApi();
                    }
                }
                break;
        }
    }

    public boolean Validate() {
        if (VU.isEmailId(edtEmail)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Valid_Email_Address));
            return false;
        }
        return true;
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public void forgetPasswordApi() {
        final String encode_emailId = VU.encode(edtEmail.getText().toString().getBytes());
        String urlParameters = "username=" + encode_emailId;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(forgetpasswordUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject loginObject = new JSONObject(response);
                        String msg = loginObject.getString("message");
                        Log.e("Msg", msg);

                        status_code = loginObject.getString("status_code");
                        boolean status = loginObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status){
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Password_Reset_Link_Msg));
                            edtEmail.setText("");
                        }else
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }

    protected void onPause() {
        super.onPause();
        if (restAsyncTask != null && restAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            restAsyncTask.cancel(true);

        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, LoginActivity.class));
        finish();
    }
}
