package com.Android.Inc.bitwallet.Models;

public class EmployeeItemModel {

    private String EmpName;
    private String EmpId;
    private String EmpEmail;


    public EmployeeItemModel() {
    }

    public EmployeeItemModel(String empName, String empId) {
        this.EmpName = empName;
        this.EmpId = empId;
    }

    public EmployeeItemModel(String empName, String empId,String EmpEmail) {
        this.EmpName = empName;
        this.EmpId = empId;
        this.EmpEmail = EmpEmail;
    }


    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public String getEmpId() {
        return EmpId;
    }

    public void setEmpId(String empId) {
        EmpId = empId;
    }

    public String getEmpEmail() {
        return EmpEmail;
    }

    public void setEmpEmail(String empEmail) {
        EmpEmail = empEmail;
    }


}
