package com.Android.Inc.bitwallet.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

public class SocialMediaActivity extends AppCompatActivity implements LifecycleObserver {

    Context context;
    String strFlag;
    Toolbar toolbar;

    ProgressBar progressBar;
    private WebView webView;
    private String url;
    private LinearLayout imgleftArrow;
    String screenType;
    private boolean minimizeBtnPressed =false;
    private static final String TAG = SocialMediaActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_social_media_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_social_media);
        }
        imgleftArrow = findViewById(R.id.title_bar_left_menu);
        progressBar = findViewById(R.id.progressBar);
        imgleftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                try {
                    ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");

                    finish();
                }
                catch (Exception e)
                {
                    Log.e("MyAppSocial exception ", e.getMessage());

                }
            }
        });

        context = SocialMediaActivity.this;
        //ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
        if(VU.isConnectingToInternet(context,screenType)){
            getIntentData();
            showPrograssBar();
            intitialize();
        }

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    private void intitialize() {

        Log.e("url",url);
        WebView myWebView = (WebView) findViewById(R.id.Webview);
        WebSettings webSettings = myWebView.getSettings();


        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(url);
        ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "");

    }


    private void showPrograssBar() {
        progressBar.setVisibility(View.VISIBLE);
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                progressBar.setVisibility(View.GONE);
            }
        }.start();
    }

    private void getIntentData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                strFlag = extras.getString("flag");
                url = (extras.getString("url"));
                Log.e("FLAG", " " + strFlag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }
    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
      /*  if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        }else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }*/
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        try {

            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
            // ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            // finish();

        /*if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }*/

            if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
                ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
                startActivity(new Intent(context, SplashActivity.class));
                finish();
            }

        }
        catch (Exception e)
        {
            Log.e("MyAppSocial exception ", e.getMessage());

        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        try {
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }

        }
        catch (Exception e)
        {
            Log.e("MyAppSocial exception ", e.getMessage());

        }

    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: ");

        try {
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        //super.onBackPressed();
            finish();

    }
        catch (Exception e)
    {
        Log.e("MyAppSocial exception ", e.getMessage());

    }
    }
}
