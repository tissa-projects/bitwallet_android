package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.Android.Inc.bitwallet.Activities.CountryCodeForAddUpdateActivity;
import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListCountryAddUpdateAdapter extends BaseAdapter {

    private List<ListItemModel> listItemListModel;
    //  ValueFilter valueFilter;
    Context context;
    CountryCodeForAddUpdateActivity myActivity;
    TextView txtCountryName, txtCountryCode;
    ArrayList<ListItemModel> countryList;
    LayoutInflater inflater;

    public ListCountryAddUpdateAdapter(List<ListItemModel> listItemListModel, Context context) {
        this.listItemListModel = listItemListModel;
        this.context = context;
        myActivity = (CountryCodeForAddUpdateActivity) context;
        this.countryList = new ArrayList<ListItemModel>();
        countryList.addAll(listItemListModel);

    }

    @Override
    public int getCount() {
        return listItemListModel.size();
    }

    @Override
    public Object getItem(int position) {
        return listItemListModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();

            Configuration config = context.getResources().getConfiguration();
            if (config.smallestScreenWidthDp >= 600) {
                view = LayoutInflater.from(context).inflate(R.layout.layout_recycler_country_code_tab, null);
            } else {
                view = LayoutInflater.from(context).inflate(R.layout.layout_recycler_country_code, null);
            }

            holder.txtCountryName = view.findViewById(R.id.txt_country_name);
            holder.txtCountryCode = view.findViewById(R.id.txt_country_code);
            holder.rl = view.findViewById(R.id.rl);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.txtCountryName.setText(listItemListModel.get(position).getName());
        holder.txtCountryCode.setText(listItemListModel.get(position).getCode());


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = holder.txtCountryCode.getText().toString();
                String country = holder.txtCountryName.getText().toString();
                Log.e("countryCode", country + " " + code);

                ACU.MySP.saveSP(context, ACU.MySP.COUNTRY_CODE, code);
                Tools.hideKeyboard(myActivity);
                myActivity.finish();
            }
        });


        return view;
    }

    public class ViewHolder {
        TextView txtCountryName, txtCountryCode;
        RelativeLayout rl;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listItemListModel.clear();
        Log.e("filter", charText);

        if (charText.length() == 0) {
            listItemListModel.addAll(countryList);
        } else {
            Log.e("char", charText);
            ;
            for (ListItemModel listItemModel : countryList) {
                Log.e("itemList", listItemModel.getCode() + "  " + listItemModel.getName());
                if (listItemModel.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listItemListModel.add(listItemModel);
                    Log.e("list", listItemModel.getName());
                }
            }
        }

        notifyDataSetChanged();
    }

}
