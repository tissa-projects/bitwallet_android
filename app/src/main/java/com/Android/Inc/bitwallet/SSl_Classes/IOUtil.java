package com.Android.Inc.bitwallet.SSl_Classes;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class IOUtil {
    private static String caCertificateName = "new_crt_cert.crt";
    private static String clientCertificateName = "client_cert.p12";
    private static String clientCertificatePassword = "G3edKQab93713!#LH";

    public static String readFully(InputStream inputStream) throws IOException {

        if(inputStream == null) {
            return "";
        }

        BufferedInputStream bufferedInputStream = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            byteArrayOutputStream = new ByteArrayOutputStream();

            final byte[] buffer = new byte[1024];
            int available = 0;

            while ((available = bufferedInputStream.read(buffer)) >= 0) {
                byteArrayOutputStream.write(buffer, 0, available);
            }

            return byteArrayOutputStream.toString();

        } finally {
            if(bufferedInputStream != null) {
                bufferedInputStream.close();
            }
        }
    }

    public static File getClientCertFile(Context context) throws Exception {

        File file = new File(Environment.getExternalStorageDirectory() + "/" + clientCertificateName);
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(clientCertificateName);
        copyInputStreamToFile(inputStream, file);


        //  File externalStorageDir = new File("android.resource://com.example.sslpining/raw/");
        File externalStorageDir = Environment.getExternalStorageDirectory();
        return new File(externalStorageDir, clientCertificateName);
    }


    public static String readCaCert(Context context) throws Exception {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(caCertificateName);
        return IOUtil.readFully(inputStream);
    }

    // InputStream -> File
    public static void copyInputStreamToFile(InputStream inputStream, File file)
            throws IOException {

        try (FileOutputStream outputStream = new FileOutputStream(file)) {

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }
    }
}
