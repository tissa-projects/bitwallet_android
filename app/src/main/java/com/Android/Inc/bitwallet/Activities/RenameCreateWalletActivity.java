package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.VU;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RenameCreateWalletActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    private static final String TAG = PinActivity.class.getSimpleName();
    public static String createWalletUrl = "business/v2/create_wallet";
    public static String renameWalletUrl = "wallet_rename";
    public static String deleteWalletUrl = "wallet_delete";
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    private Button btnSubmit, btnRename, btnDelete;
    private AppCompatEditText edtWalletType, edtWalletName;
    private Context context;
    private String btnName;
    private List<String> list;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet, view, layout;
    private ProgressBar progressBar;
    private String status_code = "";
    String screenType;
    private boolean minimizeBtnPressed = false;
    private int isDefaultWallet;
    private AsyncTask mtaskCreateWallet = null, mtaskRenameWallet = null, mtaskDeleteWallet = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_rename_create_wallet_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_rename_create_wallet);
        }

        context = RenameCreateWalletActivity.this;
        btnName = ACU.MySP.getFromSP(context, ACU.MySP.BTN_CLICK, "");
        getIntentData();
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            isDefaultWallet = bundle.getInt("isDefaultWallet");
        }
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        btnDelete = findViewById(R.id.btn_delete);
        btnSubmit = findViewById(R.id.btn_submit);
        btnRename = findViewById(R.id.btn_rename);
        edtWalletType = findViewById(R.id.edt_walletType);
        edtWalletName = findViewById(R.id.edt_walletName);
        progressBar = findViewById(R.id.renameCreate_progressBar);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);


        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        if (btnName.equalsIgnoreCase("addWallets")) {
            btnDelete.setVisibility(View.GONE);
            btnRename.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.VISIBLE);

           // edtWalletType.setFocusable(false);
          //  edtWalletType.setClickable(true);
            edtWalletType.setEnabled(true);

            edtWalletType.setText("");
            edtWalletName.setText("");

        } else if (btnName.equalsIgnoreCase("walletListAdapter")) {
           // edtWalletType.setEnabled(false);
            btnDelete.setVisibility(View.VISIBLE);
            btnRename.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.GONE);
          //  edtWalletType.setClickable(false);
            edtWalletType.setFocusable(false);
         //   edtWalletType.setActivated(false);


            edtWalletName.setText(ACU.MySP.getFromSP(context, ACU.MySP.WALLET_NAME, ""));
            edtWalletType.setText(ACU.MySP.getFromSP(context, ACU.MySP.CURRENCY_TYPE, ""));

        }

        btnDelete.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnRename.setOnClickListener(this);
        edtWalletType.setOnClickListener(this);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        CreateWalletAPI();
                    }
                }
                break;

            case R.id.btn_rename:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        RenameWalletAPI();
                    }
                }
                break;

            case R.id.btn_delete:
                Log.e("WalletAmt", "Float Value :" + Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMT, "")));
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMT, "")) == 0) {
                    if (isDefaultWallet == 0) {
                        deleteWalletConfirmDialog();
                    }else {
                        CustomDialogs.dialogShowMsg(context,screenType,getResources().getString(R.string.wallet_not_delete));
                    }
                } else if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMT, "")) > 0) {
                        AlertAmtAvailableDialog();
                }
                break;

            case R.id.edt_walletType:
                showBottomSheetDialog();
                break;

            case R.id.title_bar_left_menu:
                /*startActivity(new Intent(context, SidePanalActivity.class));
                finish();*/
                onBackPressed();
                break;

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public boolean Validate() {
        if (VU.isEmpty(edtWalletType)) {
            CustomDialogs.dialogShowMsg(context, screenType,getResources().getString(R.string.Select_Wallet_Type));
            return false;
        } else if (VU.isEmpty(edtWalletName)) {
            CustomDialogs.dialogShowMsg(context, screenType,getResources().getString(R.string.Enter_Wallet_Name));
            return false;

        }
        return true;
    }


    private void showBottomSheetDialog() {
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.wallet_bottom_list, null);
        final ListView currencyTypeList = view.findViewById(R.id.list_bottom);

        list = new ArrayList<>();
        list = ACU.MySP.getArrayList(context, ACU.MySP.CURRENCY_TYPE_ARRAYLIST);
        Log.e("List", list.toString());

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.adapter_wallet_bottom_list, R.id.textView, list);
        currencyTypeList.setAdapter(arrayAdapter);

        currencyTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                edtWalletType.setText(list.get(position));
                mBottomSheetDialog.cancel();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void CreateWalletAPI() {

        final String str_walletType = edtWalletType.getText().toString();
        final String str_encode_walletName = VU.encode(edtWalletName.getText().toString().getBytes());

        mtaskCreateWallet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null,urlParameters = null;
                try {
                    urlParameters = "wallet_type=" + str_walletType + "&label=" + str_encode_walletName
                            + "&username=" + VU.encode(ACU.MySP.getFromSP(context,ACU.MySP.LOGIN_Email,"").getBytes());
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(createWalletUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: "+s );

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        //    CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");// response : New Wallet Has Been Created Successfully.
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status) {
                            dialogWalletActivity(msg, 1);  // for New Wallet Has Been Created Successfully.  code 1
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }

                    }
                } catch (Exception e) {
                    //  CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);

                }
            }
        }.execute();
    }

    private void RenameWalletAPI() {

        final String str_encode_walletName = VU.encode(edtWalletName.getText().toString().getBytes());

        mtaskRenameWallet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null,urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&label=" + str_encode_walletName;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(renameWalletUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: "+s );
                   /* exampleApi = new API(context, ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, ""), str_encode_walletName);
                    s = exampleApi.doGet(renameWalletUrl, "renameWallet");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");// response : Wallet Renamed Successfully.
                        //   CustomToast.custom_Toast(contextDashBoard, msg, layout);
                        status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");
                        Log.e("statuscode", status_code);
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status){
                            dialogWalletActivity(msg,2);  // for rename wallet code 2
                        }else {
                            CustomDialogs.dialogShowMsg(context, screenType,msg);
                        }
                    }
                } catch (Exception e) {
                    // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();

    }

    private void DeleteWalletAPI() {

        mtaskDeleteWallet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null,urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "");
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(deleteWalletUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: "+s );
                  /*  exampleApi = new API(context, ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, ""));
                    s = exampleApi.doGet(deleteWalletUrl, "deleteWallet");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");
                        Log.e("Delete", s);
                        status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status){
                            dialogWalletActivity(msg,3);  // for delete wallet code 3
                        }else {
                            CustomDialogs.dialogShowMsg(context, screenType,msg);
                        }
                    }
                } catch (Exception e) {
                    //CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();

    }


    public void deleteWalletConfirmDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_delete_wallet_tab);
        } else {
            dialog.setContentView(R.layout.dialog_delete_wallet);
        }
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (VU.isConnectingToInternet(context, screenType)) {
                    dialog.dismiss();
                    DeleteWalletAPI();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    public void AlertAmtAvailableDialog() {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_amount_available_warning_tab);
        } else {
            dialog.setContentView(R.layout.dialog_amount_available_warning);
        }
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        dialog.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (VU.isConnectingToInternet(context, screenType)) {
                    dialog.dismiss();

                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogWalletActivity(final String text, final int code) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (code == 3) {  // for delete wallet code 3
                    dialog.dismiss();
                    startActivity(new Intent(context, SidePanalActivity.class));
                    finish();
                } else if (code == 2) {  // for rename  wallet code 2
                    dialog.dismiss();
                    startActivity(new Intent(context, SidePanalActivity.class));
                    finish();
                } else if (code == 1) {  // for new wallet created code 1
                    dialog.dismiss();
                    startActivity(new Intent(context, SidePanalActivity.class));
                    finish();
                } else {
                    dialog.dismiss();
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onDestroy() {
        if (mtaskCreateWallet != null && mtaskCreateWallet.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskCreateWallet.cancel(true);
        } else if (mtaskDeleteWallet != null && mtaskDeleteWallet.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskDeleteWallet.cancel(true);
        } else if (mtaskRenameWallet != null && mtaskRenameWallet.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskRenameWallet.cancel(true);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, WalletListActivity.class));
        finish();
        super.onBackPressed();
    }
}
