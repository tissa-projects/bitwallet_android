package com.Android.Inc.bitwallet.Activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    View layout;
    private ImageView imgFacebook, imgTwitter, imgLinkedIn;
    private LinearLayout titleBarBackBtn;
    private AppCompatEditText edtEmailId, edtpassword;
    private Context context;
    private TextView txt_forgotpassword, txtLogin, txtCancel;
    private ProgressBar progressBar;
    private CheckBox checkBox;
    private LinearLayout llCreatewallet;
    private String status_code = "";
    private String status = "";
    private String msg = "";
    boolean isSecurityKey = false;
    String screenType;
    private API exampleApi;
    private AsyncTask mtaskLogin = null;
    private RestAsyncTask restAsyncTask = null;
    private static final String TAG = LoginActivity.class.getSimpleName();
    // String exampleUrl = "https://gateway.bitwallet.org/rest/login";
    // public static String exampleUrl = "login";
    public static String exampleUrl = "business/v2/login";
    String resendEmailUrl = "business/v2/resendemail";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_login);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_login_tablet);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_login);
        }
        context = LoginActivity.this;
        ACU.MySP.saveSP(context, ACU.MySP.COUNTRY_CODE, "");
        initialize();
        edtEmailId.setText(ACU.MySP.getFromSP(context, ACU.MySP.EMAIL_REMEMBER_ME, ""));
        ACU.MySP.setSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, false);
        ACU.MySP.setSPBoolean(context, ACU.MySP.IS_TIP_SWITCH_ON, true);

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

    }


    private void initialize() {
        txtLogin = findViewById(R.id.txt_login);
        txtCancel = findViewById(R.id.txt_cancel);
        txt_forgotpassword = findViewById(R.id.txt_forgetPassword);
        llCreatewallet = findViewById(R.id.ll_createwallet);
        imgFacebook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);
        progressBar = findViewById(R.id.login_progressBar);
        checkBox = findViewById(R.id.checkbox_rememberMe);

        txtLogin.setOnClickListener(this);
        txtCancel.setOnClickListener(this);
        txt_forgotpassword.setOnClickListener(this);
        llCreatewallet.setOnClickListener(this);
        imgFacebook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);
        checkBox.setOnClickListener(this);

        edtEmailId = findViewById(R.id.edt_email);
        edtpassword = findViewById(R.id.edt_password);

        edtpassword.setTransformationMethod(new PasswordTransformationMethod());       // to make password filed *
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_login:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        CheckForLogin();
                    }
                }
                //startActivity(new Intent(context, ChargeBitwalletActivity.class));
                break;
            case R.id.txt_cancel:
                onBackPressed();
                break;
            case R.id.txt_forgetPassword:
                startActivity(new Intent(context, ForgetPasswordActivity.class));
                finish();
                break;
            case R.id.ll_createwallet:
                ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "login");
                startActivity(new Intent(context, CheckIndividualBussinsActivity.class));
                finish();
                break;
            case R.id.img_facebook:
                SocialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_twitter:
                SocialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.img_linkedIn:
                SocialMedia("linkedIn", ACU.MySP.INSTA_URL);

                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
            case R.id.checkbox_rememberMe:
                if (Validate()) {
                    if (((CheckBox) v).isChecked()) {
                        ACU.MySP.saveSP(context, ACU.MySP.EMAIL_REMEMBER_ME, edtEmailId.getText().toString());
                    } else {
                        ACU.MySP.saveSP(context, ACU.MySP.EMAIL_REMEMBER_ME, "");
                    }
                }
                break;

        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, MainActivity.class));
        finish();
        super.onBackPressed();
    }

    public void SocialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            Log.e("flag", flag);
            Log.e("url", url);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public boolean Validate() {
        if (VU.isEmailId(edtEmailId)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Valid_Email_Address));
            return false;
        } else if (VU.isEmpty(edtpassword)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Password));
            return false;

        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void CheckForLogin() {

        //   Debug.startMethodTracing("sample");
        final String encode_emailId = VU.encode(edtEmailId.getText().toString().getBytes());
        final String encode_password = VU.encode(edtpassword.getText().toString().getBytes());
        Log.e("original", edtEmailId.getText().toString() + "  " + edtpassword.getText().toString());
        Log.e("ENCODE", encode_emailId + "  " + encode_password);

        mtaskLogin = new AsyncTask<String, String, String>() {
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    String strIpV4 = Miscellaneous.getIPAddress(true);
                    Log.e(TAG, "doInBackground: " + strIpV4);
                    urlParameters = "username=" + encode_emailId + "&password=" + encode_password + "&ip_address=" + strIpV4;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(exampleUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                   /* exampleApi = new API(context, encode_emailId, encode_password);
                    s = exampleApi.doGet(exampleUrl, "login");*/
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    Log.e(TAG, "doInBackground: " + ex.getMessage());
                    ex.printStackTrace();
                    //   getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                String businessName = "", fName = "", address = "", lName = "";
                try {
                    progressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject loginObject = new JSONObject(s);
                        msg = loginObject.getString("message");
                        status = loginObject.getString("status");
                        ACU.MySP.saveSP(context, ACU.MySP.LOGIN_Email, edtEmailId.getText().toString());
                        ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS_BOOLEAN, status);
                        Log.e("Msg", msg);
                        status_code = loginObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equalsIgnoreCase("800")) {
                            CustomDialogs.dialogAppMantenance(context, screenType);
                        }

                        if (status.equalsIgnoreCase("true")) {
                            JSONObject dataObj = loginObject.getJSONObject("data");
                            Log.e("dataObj", dataObj.toString());
                            String auth_token = dataObj.getString("auth_token");
                            Log.e("AUTHCODE", auth_token);
                            String userType = dataObj.getString("userType");
                            Log.e(TAG, "onPostExecute: userType: " + userType);
                            isSecurityKey = dataObj.getBoolean("security_key");
                            if (userType.equalsIgnoreCase("BUSINESS")) {
                                businessName = dataObj.getString("businessName");
                                address = dataObj.getString("address");
                            } else {
                                fName = dataObj.getString("first_name");
                                lName = dataObj.getString("last_name");
                                Log.e(TAG, "onPostExecute: In else Condition");
                            }

                            String email_id = dataObj.getString("email_id");
                            Log.e(TAG, "onPostExecute: emailId: " + email_id);
                            //shared  preferenced
                            ACU.MySP.saveSP(context, ACU.MySP.USER_TYPE, userType);
                            ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS, "true");
                            ACU.MySP.saveSP(context, ACU.MySP.AUTH_TOKEN, auth_token);
                            ACU.MySP.saveSP(context, ACU.MySP.LOGIN_Email, edtEmailId.getText().toString());
                            ACU.MySP.saveSP(context, ACU.MySP.BUSINESS_NAME, businessName);
                            ACU.MySP.setSPBoolean(context, ACU.MySP.IS_SECURITY_CODE, isSecurityKey);

                            if (userType.equalsIgnoreCase("BUSINESS") && isSecurityKey) {
                                //if (userType.equalsIgnoreCase("BUSINESS") ) {
                                startActivity(new Intent(context, ChargeBitwalletActivity.class));
                                //   startActivity(new Intent(context,SelectBusinessUserActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(context, PinActivity.class));
                                finish();
                            }

                        } /*else if (status_code.equalsIgnoreCase("")) { //check for status code
                            dialogVerifyAccount("Your Account Is Not Verified.", "Please Verify Your Account.");

                        }*/ /* else if (status_code.equalsIgnoreCase("401")) {
                            CustomDialogs.dialogShowMsg(context,screenType,getResources().getString(R.string.Verify_Account_Contact_Support_Team));
                        }*/ else if (status_code.equalsIgnoreCase("402")) {
                            dialogLoginVerifyEmail(getResources().getString(R.string.Check_Email_for_verification_link));
                        } else {
                            //unable to process
                            if (msg == null || msg.equalsIgnoreCase("null")) {
                                msg = "Unable to process, Please try after some time";
                            }
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }

                } catch (Exception e) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Log.e(TAG, "onPostExecute: " + e.getMessage());
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        }.execute();

    }

    public void dialogVerifyAccount(String text1, String text2) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_two_line_tab);
        } else {
            dialog.setContentView(R.layout.dialog_two_line);
        }
        TextView firstmsgTxt = dialog.findViewById(R.id.txt_firstline);
        firstmsgTxt.setText(text1);
        TextView secondmsgTxt = dialog.findViewById(R.id.txt_secondline);
        secondmsgTxt.setText(text2);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ACU.MySP.saveSP(context, ACU.MySP.REGISTRATION_EMAIL, edtEmailId.getText().toString());
                startActivity(new Intent(context, OTPActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }   //when otp not verified for time bieng it is commented

    public void dialogLoginVerifyEmail(String text) {   // to verify and resend email
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_verify_email_tab);
        } else {
            dialog.setContentView(R.layout.dialog_verify_email);
        }
        TextView firstmsgTxt = dialog.findViewById(R.id.txt_firstline);
        firstmsgTxt.setText(text);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtpassword.setText("");
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.btn_resend).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                edtpassword.setText("");
                dialog.dismiss();
                ResendEmail();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void ResendEmail() {
        final String encode_emailId = VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes());
        String urlParameters = "username=" + encode_emailId;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(resendEmailUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject loginObject = new JSONObject(response);
                        msg = loginObject.getString("message");
                        status = loginObject.getString("status");
                        Log.e("Msg", msg);
                        status_code = loginObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equalsIgnoreCase("200")) {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();

    }

    protected void onPause() {
        super.onPause();
        if (restAsyncTask != null && restAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            restAsyncTask.cancel(true);

        }
    }
}
