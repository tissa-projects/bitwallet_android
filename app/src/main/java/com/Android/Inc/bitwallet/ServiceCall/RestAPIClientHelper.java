package com.Android.Inc.bitwallet.ServiceCall;

public class RestAPIClientHelper {
    private String contentType = "";
    private String requestUrl = "";
    private String methodType = "";
    private String urlParameter = "";
    private boolean isLiveAPI = true;   // false : when hitting development server & true : when hitting live bitWallet server
    private String accept = "";
    private String authorization = "";
    private String apiToken = "";
    private String authToken = "";

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public boolean isLiveAPI() {
        return isLiveAPI;
    }

    public void setLiveAPI(boolean liveAPI) {
        isLiveAPI = liveAPI;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) { this.requestUrl = requestUrl; }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

    public String getUrlParameter() {
        return urlParameter;
    }

    public void setUrlParameter(String urlParameter) {
        this.urlParameter = urlParameter;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "RestAPIClientHelper{" +
                "contentType='" + contentType + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", methodType='" + methodType + '\'' +
                ", urlParameter='" + urlParameter + '\'' +
                ", isLiveAPI=" + isLiveAPI +
                ", accept='" + accept + '\'' +
                ", authorization='" + authorization + '\'' +
                ", apiToken='" + apiToken + '\'' +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}