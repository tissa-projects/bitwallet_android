package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

public class TransactionOTPverificationActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    private TextView txtResend, txtSubmit;
    private AppCompatEditText edtOTP;
    private ProgressBar progressBar;
    private View layout;
    private Context context;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private LinearLayout titleBarBackBtn;
    private String msg = null;
    private String status_code = "";
    String screenType;
    private boolean minimizeBtnPressed = false;

    private RestAsyncTask restAsyncTask = null;
    private static final String TAG = OTPActivity.class.getSimpleName();
    public static String resendOTPUrl = "crypto-currency/get-otp";
    public static String verifyOTPUrl = "crypto-currency/validate-otp";

    private String  strBackType = "", strWyreAccId;
    private Bundle bundle = null;
    private String strSndAmt, strTxnFee, strAddress, strTxnType, strTotalAmt, strWalletId, strTxnHash, strUniqueId,strSndDollarAmt,
            strCloneWalletId,strCloneWalletStatus,strCloneUniqueId,strWyreFinalRecAmt,strWyreFee,strWyreFeeRate,strIsPaymentWallet;

    private String phone_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_otpverify_for_payment_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_otpverify_for_payment);
        }
        context = TransactionOTPverificationActivity.this;
        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver((LifecycleObserver) this);

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }

    private void getIntentData() {
        bundle = getIntent().getExtras();
        strBackType =  ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "");
        Log.e(TAG, "getIntentData: strBackType: "+strBackType );
        if (bundle != null) {
            strTxnFee = bundle.getString("txnfee");
            strSndAmt = bundle.getString("sendAmt");
            strAddress = bundle.getString("address");
            strTxnType = bundle.getString("txnType");
            strTotalAmt = bundle.getString("totalAmt");
            strWalletId = bundle.getString("walletId");
            strTxnHash = bundle.getString("txnHash");
            strUniqueId = bundle.getString("uniqueId");
            strCloneWalletId = bundle.getString("cloneWalletId");
            strCloneWalletStatus = bundle.getString("cloneWalletStatus");
            strCloneUniqueId = bundle.getString("cloneUniqueId");
            strIsPaymentWallet = bundle.getString("isPaymentWallet");
            strSndDollarAmt  = Miscellaneous.CustomsMethods.convertBtcToDoller(strSndAmt,context);
            Log.e(TAG, "getIntentData: strSndDollarAmt: "+strSndDollarAmt );
            ACU.MySP.saveSP(context, ACU.MySP.BTC_AMOUNT, Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(strSndAmt)));
            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, strAddress);
            Log.e(TAG, "getIntentData: " + strTxnFee);
            ACU.MySP.saveSP(context, ACU.MySP.NETWORK_FEE, Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(strTxnFee)));
            String btc = ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMOUNT, "");
            Log.e(TAG, "getIntentData: " + btc);
            ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, Miscellaneous.CustomsMethods.convertBtcToDoller(btc, context));
            Log.e(TAG, "getIntentData:strTxnFee: " + strTxnFee + " strSndAmt: " + strSndAmt + " strAddress: " + strAddress + " strTxnType: " + strTxnType +
                    " strTotalAmt: " + strSndAmt + " strWalletId: " + strWalletId + " strTxnHash" + strTxnHash + " strUniqueId " + strUniqueId);

            if (strBackType.equalsIgnoreCase("sell_activity")){
                strWyreFinalRecAmt = bundle.getString("wyreFinalRecAmt");
                strWyreFee = bundle.getString("wyreFee");
                strWyreFeeRate = bundle.getString("wyreFeeRate");
            }
        }

        if (VU.isConnectingToInternet(context, screenType)) {
                getOTP();

        }

    }
    private void initialize() {
        txtResend = findViewById(R.id.txt_resend);
        txtSubmit = findViewById(R.id.txt_submit);
        edtOTP = findViewById(R.id.edt_otp);
        progressBar = findViewById(R.id.otp_progressBar);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        txtResend.setOnClickListener(this);
        txtSubmit.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);
        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);

        getIntentData();
    }
    public boolean Validate() {
        if (VU.isEmpty(edtOTP)) {
            /*edtOTP.setError("Please Enter OTP");
            edtOTP.requestFocus();*/
            dialogOTPActivity("Enter OTP", false);
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;

            case R.id.txt_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        submitOTP();
                    }
                }
                break;

            case R.id.txt_resend:
                if (VU.isConnectingToInternet(context, screenType)) {
                    getOTP();
                }
                break;

            case R.id.title_bar_left_menu:
                startActivity(new Intent(context, SendCurrencyActivity.class));
                finish();
                break;
        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }
    public void dialogOTPActivity(String text, final boolean status) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (status) {
                  // Handle here
                } else {
                    edtOTP.setText("");
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
    public void dialogResendOTP(String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtOTP.setText("");
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
    private void gotoSendingCurrencyActivity() {
        Intent intent = new Intent(context, SendingCurrencyActivity.class);
        intent.putExtra("address", strAddress);
        intent.putExtra("sendAmt", strSndAmt);
        intent.putExtra("txnHash", strTxnHash);
        intent.putExtra("uniqueId", strUniqueId);
        intent.putExtra("txnfee", strTxnFee);
        intent.putExtra("txnType", strTxnType);
        intent.putExtra("totalAmt", strTotalAmt);
        intent.putExtra("walletId", strWalletId);
        intent.putExtra("cloneUniqueId", strCloneUniqueId);
        intent.putExtra("cloneWalletId", strCloneWalletId);
        intent.putExtra("cloneWalletStatus", strCloneWalletStatus);
        intent.putExtra("isPaymentWallet", strIsPaymentWallet);
        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "send_currency_activity");
        startActivity(intent);
        finish();
    }
/*
    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    */

    @Override
    public void onBackPressed() {
            startActivity(new Intent(context, SendCurrencyActivity.class));
            finish();

    }

    private void getOTP() {
        String urlParameters = "";
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(resendOTPUrl);
        helper.setUrlParameter(urlParameters);

        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status) {

                            JSONObject dataArray = jsonObject.getJSONObject("data");
                            phone_no = dataArray.getString("phone_no");
                            Log.e(TAG, "processResponse: Mobile: - " + phone_no );
                            dialogResendOTP(msg);
                        } else {

                            if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                                CustomDialogs.dialogSessionExpire(context, screenType);
                            } else
                            {
                                dialogResendOTP(msg);
                            }

                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();
    }

    private void submitOTP() {
        String otp_string = "otp_code="+edtOTP.getText().toString().trim();
        String phone = "&phone_no="+ VU.encode(phone_no.getBytes());
        String urlParameters = otp_string+""+phone;

        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(verifyOTPUrl);
        helper.setUrlParameter(urlParameters);

        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");

                        if (status) {
                            gotoSendingCurrencyActivity();
                            //dialogOTPActivity(msg, status);
                        } else {

                            if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                                CustomDialogs.dialogSessionExpire(context, screenType);
                            } else {
                                dialogOTPActivity("Invalid OTP", status);  // if  otp  is invalid
                            }
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();
    }
}