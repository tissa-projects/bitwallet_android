package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.Models.EmployeeItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.RetrofitClient.RetrofitClient;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.SearchEmployeeAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchEmployeeActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private SearchView searchView;
    private Context context;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerEmployees;
    private SearchEmployeeAdapter employeesAdapter;
    private JSONArray dataArray;
    private LinearLayout titleBarBackBtn;
    private ProgressBar progressBar;
    String screenType;
    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    private boolean minimizeBtnPressed = false;
    private List<EmployeeItemModel> employeeItemModelList;
    private static final String TAG = SearchEmployeeActivity.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_search_employees_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_search_employees);
        }
        context = SearchEmployeeActivity.this;
        employeeItemModelList = new ArrayList<>();


        initialize();
        initRecycler();
        DevEmployeeListData();
        searchForEmployee();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGrou3nd");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        searchView = findViewById(R.id.search_employee);
        progressBar = findViewById(R.id.search_emp_pogressbar);
        swipeRefreshLayout = findViewById(R.id.employee_refresh);
        recyclerEmployees = findViewById(R.id.recycler_employee);
        titleBarBackBtn = findViewById(R.id.title_bar_left_menu);

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
        titleBarBackBtn.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);

    }

    private void initRecycler() {
        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerEmployees.setLayoutManager(linearLayoutManager);
        recyclerEmployees.setHasFixedSize(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
            employeeItemModelList.clear();
            DevEmployeeListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;


        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, FilterNEmployeesActivity.class));
        finish();
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    private void DevEmployeeListData() {
        progressBar.setVisibility(View.VISIBLE);
        String auth_token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI5OTNmY2RjZS1kNmNjLTQzOGYtOGU5Mi00MTFhOWEyMWU0MzciLCJpYXQiOjE1NzIwOTAzMzksInN1YiI6ImFiaGlsZXNoIiwiZXhwIjoxNTczNTYxNTY4fQ.l4p8RqyPCBmv6CDz6erXeCWQU0TEZzAR4Pe02k9rqWwOSFh3aNP1xlEL8iTu6FOFba19i9XFugYCW718iSEBSA";
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().EmployeeList(auth_token, "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBar.setVisibility(View.INVISIBLE);
                try {
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");
                    String strMessage = Obj.getString("message");
                    if (Obj != null && statusCode.equalsIgnoreCase("200")) {
                        dataArray = Obj.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            EmployeeItemModel employeeItemModel = new EmployeeItemModel();
                            final String empcode = dataArray.getJSONObject(i).getString("empcode");
                            final String firstName = dataArray.getJSONObject(i).getString("firstName");
                            final String lastName = dataArray.getJSONObject(i).getString("lastName");
                            final String email = dataArray.getJSONObject(i).getString("email");

                            employeeItemModel.setEmpName(firstName+" "+lastName);
                            employeeItemModel.setEmpId(empcode);
                            employeeItemModel.setEmpEmail(email);

                            employeeItemModelList.add(employeeItemModel);
                            Log.e(TAG, "onResponse: "+employeeItemModelList );

                        }
                        adapterCall();
                    } else {
                        dialogEmployeeListActivity(strMessage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void adapterCall() {

        employeesAdapter = new SearchEmployeeAdapter(context, employeeItemModelList);
        recyclerEmployees.setAdapter(employeesAdapter);
        employeesAdapter.setOnItemClickListener(new SearchEmployeeAdapter.RecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position,String empCode,String empEmail) {
                Log.d(TAG, "onItemClick: "+empCode +" "+empEmail);

                Intent intent = new Intent(context,EmployeeActivity.class);
                intent.putExtra("EmpEmail",empEmail);
                intent.putExtra("EmpCode",empCode);
                startActivity(intent);
                finish();
            }
        });
    }


    private void searchForEmployee() {

        searchView.setActivated(true);
        searchView.setQueryHint("Search for Employee");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    employeesAdapter.filter("");

                } else {
                    employeesAdapter.filter(newText);
                }

                return true;
            }
        });

    }

    public void dialogEmployeeListActivity(final String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


}
