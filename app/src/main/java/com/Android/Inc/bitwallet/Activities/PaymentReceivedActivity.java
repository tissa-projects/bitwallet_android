package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.PaymentReceivedAdapter;
import com.Android.Inc.bitwallet.adapters.WalletListAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class PaymentReceivedActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private TextView txtBtcAmt, txtDollarAmt;
    private LinearLayout backArrow;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerTransaction;
    private Context context;
    private ImageView imgWallet;
    private PaymentReceivedAdapter paymentReceivedAdapter = null;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    private LinearLayout ll_backArrow;
    private String status_code = "";
    private ProgressBar progressBar;
    String screenType;
    private boolean minimizeBtnPressed = false;
    private static final String TAG = TransactionActivity.class.getSimpleName();
    public static String transactionListUrl = "business/get_txns_history";
    public static String walletUrl = "business/get_walletList";
    private AsyncTask mtaskWalletTransaction = null, mtaskWithDraw = null;
    private AsyncTask mtaskWalletList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_payment_received_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_payment_received);
        }
        context = PaymentReceivedActivity.this;

        initialize();
        if (VU.isConnectingToInternet(context, screenType)) {
            WalletListData();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            Log.e(TAG, "onResume: in if");
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    private void initialize() {
        txtBtcAmt = findViewById(R.id.txt_btc_amt);
        txtDollarAmt = findViewById(R.id.txt_doller_amt);
        backArrow = findViewById(R.id.title_bar_left_menu);
        imgWallet = findViewById(R.id.wallet);
        swipeRefreshLayout = findViewById(R.id.transaction_refresh);


        progressBar = findViewById(R.id.transaction_progressBar);
        recyclerTransaction = findViewById(R.id.recycler_transactions);


        @SuppressLint("WrongConstant")
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerTransaction.setLayoutManager(linearLayoutManager);
        recyclerTransaction.setHasFixedSize(true);

        imgWallet.setOnClickListener(this);
        backArrow.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);


        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.wallet:
                if (Validate()) {
                    if (VU.isConnectingToInternet(context, screenType)) {
                        dialogPaymentReceivedWithDraw();
                    }
                }
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }

    }

    public boolean Validate() {
        if ((Double.valueOf(txtBtcAmt.getText().toString()) < (100))) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Minimum_100_dollar_required));
            return false;
        }
        return true;
    }

    public void socialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    //Swipe referesh
    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
            Log.e("inRefresh", "refresh");
            WalletListData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    private void TransactionListData(final String walletId, final String walletUniqueId) {
        mtaskWalletTransaction = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "wallet_id=" + walletId + "&walletType=PAYMENT" + "&unique_id=" + walletUniqueId;

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strCurrency = dataObj.getString("currency");
                            String strBtcAMt = dataObj.getString("wallet_bal");
                            txtBtcAmt.setText(strBtcAMt.equals("0") ? "0.00000000" + " " + strCurrency : strBtcAMt + " " + strCurrency);
                            txtDollarAmt.setText("$" + " " + Miscellaneous.CustomsMethods.convertBtcToDoller(strBtcAMt, context));
                            JSONArray dataListArray = dataObj.getJSONArray("txns");
                            paymentReceivedAdapter = new PaymentReceivedAdapter(context, dataListArray, screenType);
                            recyclerTransaction.setAdapter(paymentReceivedAdapter);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("log", "catch");
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                }
            }
        }.execute();
    }

    private void WithDrawData() {
        mtaskWithDraw = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) +
                            "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes());
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(transactionListUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        String status = jsonObject.getString("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status.equalsIgnoreCase("true")) {
                            JSONObject data_obj = jsonObject.getJSONObject("data");
                            String total_bal = data_obj.getString("total_bal");
                            String txn_fee = data_obj.getString("tx_fee");
                            Intent intent = new Intent(context, WithDrawActivity.class);
                            intent.putExtra("total_bal", total_bal);
                            intent.putExtra("txn_fee", txn_fee);
                            startActivity(intent);
                            finish();
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    private void WalletListData() {
        mtaskWalletList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) + "&walletType=PAYMENT"+ "&balance_flag=" + false;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(walletUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        if (dataObj != null) {
                            JSONArray walletListArray = dataObj.getJSONArray("walletList");
                            Log.e(TAG, "onPostExecute: walletListArray: " + walletListArray);
                            String strWalletId = walletListArray.getJSONObject(0).getString("wallet_id");
                            String strWalletUniqueId = walletListArray.getJSONObject(0).getString("unique_id");
                            TransactionListData(strWalletId, strWalletUniqueId);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, ChargeBitwalletActivity.class));
        finish();
    }


    public void dialogPaymentReceivedWithDraw() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_delete_wallet_tab);
        } else {
            dialog.setContentView(R.layout.dialog_delete_wallet);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt);
        Button btnYes = dialog.findViewById(R.id.btn_ok);
        btnYes.setText("YES");
        msgTxt.setText(getResources().getString(R.string.withdraw_btc));
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ACU.MySP.CHECK_FOR_ACTIVITY = "PaymentReceivedActivity";
                startActivity(new Intent(context, PinforInnerActivity.class));
                finish();
                //    WithDrawData();

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


}
