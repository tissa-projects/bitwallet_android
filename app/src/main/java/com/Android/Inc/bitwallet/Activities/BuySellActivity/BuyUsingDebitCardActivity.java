package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;

public class BuyUsingDebitCardActivity extends AppCompatActivity implements LifecycleObserver {

    private WebView webView;
    private Context context;
    // private ProgressBar progressBar;
    private boolean minimizeBtnPressed = false;
    private LinearLayout backArrow;
    String devUrl = "http://3.132.74.72:8099/rest/";
    private String redirectString  = "http://reset.bitwallet.org/rest/buy/debit/redirect.php";
    private String failureString  = "http://reset.bitwallet.org/rest/buy/debit/failure.php";
    private String screenType, strAddress, strReferenceId, strAccountId,strBuyWidgetUrl;
    private static final String TAG = BuyUsingDebitCardActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_buy_using_debit_card_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_buy_using_debit_card);
        }

        context = BuyUsingDebitCardActivity.this;
        webView = findViewById(R.id.webView);
        //  progressBar = findViewById(R.id.progressBar);
        backArrow = findViewById(R.id.title_bar_left_menu);
        //  progressBar.setVisibility(View.VISIBLE);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        webView.getSettings().setJavaScriptEnabled(true);
        getIntentData();
        openWebViewForBuy();
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customBackpressed();
            }
        });
    }



    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strAddress = bundle.getString("address");
            strReferenceId = bundle.getString("referenceId");
            strAccountId = bundle.getString("accountId");
            strBuyWidgetUrl = bundle.getString("buyWidgetUrl");
            Log.e(TAG, "getIntentData:  strAddress :" + strAddress + " strRefrlId : " + strReferenceId + " strAccountId: " + strAccountId);
        }
    }

    private void openWebViewForBuy() {
        webView.setVisibility(View.VISIBLE);

        String address = "bitcoin%3A"+strAddress;
       /* String myUrl = "https://pay.sendwyre.com/purchase?type=debitcard-hosted&dest=" +address+ "&destCurrency=BTC" +
                "&paymentMethod=debit-card&referenceId=" + strReferenceId + "&accountId=" + strAccountId +
                "&failureRedirectUrl=http://3.132.74.72:8099/rest/wyre/buy/debit/failureredirecturl"+
                "&redirectUrl=http://3.132.74.72:8099/rest/wyre/buy/debit/redirecturl";*/

     //  String myUrl = "https://pay.sendwyre.com/purchase?dest="+address+"&destCurrency=BTC&paymentMethod=debit-card&referenceId="+strReferenceId+"&accountId="+strAccountId;

      //  Log.e(TAG, "openWebViewForBuy: myUrl: "+strBuyWidgetUrl );
       // webView.loadUrl(myUrl);
      //  webView.loadUrl("file:///android_asset/buy_wyre.html");
       webView.loadUrl(strBuyWidgetUrl);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                // runs when a page starts loading
                Log.e(TAG, "onPageStarted: " + url);
                //   finish();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                // page finishes loading
                Log.e(TAG, "onPageFinished: " + url);
                if (url.equalsIgnoreCase(redirectString) /*||
                        url.equalsIgnoreCase("http://3.132.74.72:8099/rest/wyre/buy/debit/failureredirecturl")*/){
                   // webView.setVisibility(View.GONE);
                  //  dialogTransactionFail();
                  //  CustomDialogs.dialogShowMsg(context,screenType, "onPageFinished: "+url);
                    finish();
                }else {
                    Log.e(TAG, "onPageFinished: esle: "+url );
                    url = url.split("\\?")[0];

                    Log.e(TAG, "onPageFinished: esle: "+url );
                    if (url.equals(redirectString)){
                    //    CustomDialogs.dialogShowMsg(context,screenType, "onPageFinished: "+url);
                        finish();
                    }else if (url.equals(failureString)){
                    //    CustomDialogs.dialogShowMsg(context,screenType, "onPageFinished: "+url);
                        dialogTransactionFail();
                    }
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                // runs when there's a failure in loading page
                Log.e(TAG, "onReceivedError: " + error + " " + request);

                dialogTransactionFail();
                //      finish();

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.e(TAG, "shouldOverrideUrlLoading: " + url);
                if (url.equalsIgnoreCase(redirectString)) {
                  //  webView.setVisibility(View.GONE);
                  //  CustomDialogs.dialogShowMsg(context,screenType, "shouldOverrideUrlLoading: "+url);
                    finish();
                    return true;
                } else {
                    Log.e(TAG, "shouldOverrideUrlLoading: else: " + url);
                    url = url.split("\\?")[0];

                    Log.e(TAG, "shouldOverrideUrlLoading: else: " + url);
                    if (url.equals(redirectString)) {
                     //   CustomDialogs.dialogShowMsg(context,screenType, "shouldOverrideUrlLoading: "+url);
                        finish();
                        return true;
                    }else if (url.equals(failureString)){
                     //   CustomDialogs.dialogShowMsg(context,screenType, "shouldOverrideUrlLoading: "+url);
                        dialogTransactionFail();
                        return true;
                    }
                    return false;
                }
            }

        });

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    customBackpressed();
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    private void customBackpressed() {
        if (!webView.canGoBack()) {
            finish();
        } else {
            webView.goBack();
        }
    }

    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }

    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
        // ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        // finish();

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: ");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onBackPressed();
    }

    public  void dialogTransactionFail() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(context.getResources().getString(R.string.transaction_failed));

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}

