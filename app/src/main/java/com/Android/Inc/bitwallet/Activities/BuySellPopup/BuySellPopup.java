package com.Android.Inc.bitwallet.Activities.BuySellPopup;

import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.Activities.AccountActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

import java.text.DecimalFormat;

public class BuySellPopup extends AppCompatActivity {
    private AsyncTask mtaskProfileSet = null, mtaskProfileGet = null;
    private ProgressBar progressBar;
    private static final String TAG = AccountActivity.class.getSimpleName();
    private Context context;
    private String status_code = "", user_type = "", txtid = "", txt_type = "";
    private String screenType, authorization = "";
    LinearLayout ll_backArrow;
    TextView txt_psell, txt_ptsentvalu,txt_cratename,txt_btcamt,txt_tranname,txt_mainstatus, txt_pratesentvalue,txt_tranfree, txt_pcratefee, txt_ptansid, txt_petranid, txt_pmoonfee, txt_pbitfee, txt_pcreate, txt_pstatus, txt_paddress, txt_pnetfee, txt_ptotal, txt_pconf;
    LinearLayout lly_moonpfee,
            lly_bitwalletfee,
            lly_status,
            lly_confirmation,
            lly_tsentvalue,
            lly_csentvalue,
            lly_cratefee,lly_networkfee,lly_amount,
            lly_etxtid;
    public static String GetHistroy = "business/v2/get-history";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_buy_sell_popup);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_buy_sell_popup);
        }
        init();
    }

    private void init() {
        context = BuySellPopup.this;
        progressBar = findViewById(R.id.account_progressBar);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        txt_psell = findViewById(R.id.txt_psell);
        txt_ptansid = findViewById(R.id.txt_ptansid);
        txt_petranid = findViewById(R.id.txt_petranid);
        txt_pmoonfee = findViewById(R.id.txt_pmoonfee);
        txt_pbitfee = findViewById(R.id.txt_pbitfee);
        txt_pcreate = findViewById(R.id.txt_pcreate);
        txt_pstatus = findViewById(R.id.txt_pstatus);
        txt_paddress = findViewById(R.id.txt_paddress);
        txt_pnetfee = findViewById(R.id.txt_pnetfee);
        txt_ptotal = findViewById(R.id.txt_ptotal);
        txt_pconf = findViewById(R.id.txt_pconf);
        txt_ptsentvalu = findViewById(R.id.txt_ptsentvalu);
        txt_pratesentvalue = findViewById(R.id.txt_pratesentvalue);
        txt_pcratefee = findViewById(R.id.txt_pcratefee);
        txt_tranfree = findViewById(R.id.txt_tranfree);
        txt_mainstatus= findViewById(R.id.txt_mainstatus);
        txt_btcamt= findViewById(R.id.txt_btcamt);
        txt_cratename= findViewById(R.id.txt_cratename);
        txt_tranname= findViewById(R.id.txt_tranname);

        lly_moonpfee = findViewById(R.id.lly_moonpfee);
        lly_bitwalletfee = findViewById(R.id.lly_bitwalletfee);
        lly_status = findViewById(R.id.lly_status);
        lly_confirmation = findViewById(R.id.lly_confirmation);
        lly_tsentvalue = findViewById(R.id.lly_tsentvalue);
        lly_csentvalue = findViewById(R.id.lly_csentvalue);
        lly_cratefee = findViewById(R.id.lly_cratefee);
        lly_etxtid = findViewById(R.id.lly_etxtid);
        lly_networkfee= findViewById(R.id.lly_networkfee);
        lly_amount= findViewById(R.id.lly_amount);
        user_type = ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "");

        if (VU.isConnectingToInternet(context, screenType)) {
            getIntentData();
            GetHistroy();
        }
        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onBackPressed() {
//        if (ACU.MySP.getFromSP(context, ACU.MySP.BTNCLICK, "").equalsIgnoreCase("SellActivity")) {
//            ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "");
//            startActivity(new Intent(context, SellActivity.class));
//            finish();
//        } else {
//            startActivity(new Intent(context, SettingActivity.class));
//            finish();
//        }
        finish();
    }
    private void getIntentData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                txtid = extras.getString("txt_id");
                txt_type = (extras.getString("txt_type"));
                Log.e("FLAG", " " + txt_type);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void GetHistroy() {
        mtaskProfileGet = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = "";
                urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&walletType="+ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "") + "&txn_type=" + txt_type
                        + "&transaction_id=" + txtid;


                try {

                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    //  Log.e(TAG, "profileGet: USER_TYPE:" + user_type);
                    //exampleApi = new API(context);

                    helper.setRequestUrl(GetHistroy);
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        String msg = jsonObject.getString("message");
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else {
                            if (status) {
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                Log.e(TAG, "histroy response " + dataObj);
                                String txt_type = dataObj.getString("txn_type");
                                if (txt_type.equals("SEND")) {


                                    txt_ptansid.setText(dataObj.getString("txn_id"));
                                    txt_paddress.setText(dataObj.getString("to_address"));
                                    txt_pcreate.setText(dataObj.getString("timestamp"));


                                    txt_tranfree.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_fee")));


                                    txt_ptsentvalu.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_amount")));
                                    txt_pratesentvalue.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount")));
                                    txt_pcratefee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("fee")));

                                    txt_pstatus.setText(txt_type);
                                    txt_psell.setText(dataObj.getString("amount")+" BTC");
                                    txt_tranname.setText("Transacted Value");
                                    txt_cratename.setText("Current Rate Value");
                                    Double amt= Double.valueOf(dataObj.getString("amount"));

                                    final double Usdrate = Double.valueOf(ACU.MySP.getFromSP(context,ACU.MySP.USD_RATE,""));
                                    final float walletDollerAmt = (float) (amt * Usdrate);
                                    DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                    final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                    txt_btcamt.setText("$"+strWalletDollerAmt);
                                    if(dataObj.getString("confirmation").equals(0)){
                                        txt_mainstatus.setText("unconfirmed");
                                    }else {
                                        txt_mainstatus.setText("confirmed");
                                    }


                                    lly_moonpfee.setVisibility(View.GONE);
                                    lly_bitwalletfee.setVisibility(View.GONE);
                                    lly_networkfee.setVisibility(View.GONE);
                                    lly_amount.setVisibility(View.GONE);
                                    lly_confirmation.setVisibility(View.GONE);
                                    lly_etxtid.setVisibility(View.GONE);


                                } else if (txt_type.equals("SELL")) {


                                    txt_ptansid.setText(dataObj.getString("txn_id"));
                                    txt_paddress.setText(dataObj.getString("to_address"));
                                    txt_pcreate.setText(dataObj.getString("timestamp"));


                                    txt_ptsentvalu.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_amount")));
                                    txt_pratesentvalue.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount")));
                                    txt_pcratefee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("fee")));

                                    // txt_tranfree.setText(dataObj.getString("old_fee"));
                                    txt_pstatus.setText(txt_type);
                                    Double amt= Double.valueOf(dataObj.getString("amount"));
                                    txt_tranname.setText("Transacted Value");
                                    txt_cratename.setText("Current Rate Value");
                                    final double Usdrate = Double.valueOf(ACU.MySP.getFromSP(context,ACU.MySP.USD_RATE,""));
                                    final float walletDollerAmt = (float) (amt * Usdrate);
                                    DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                    final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                    txt_btcamt.setText("$"+strWalletDollerAmt);
                                    txt_psell.setText(dataObj.getString("amount")+" BTC");
                                    if(dataObj.getString("confirmation").equals(0)){
                                        txt_mainstatus.setText("unconfirmed");
                                    }else {
                                        txt_mainstatus.setText("confirmed");
                                    }
                                    JSONObject dataObj1 = dataObj.getJSONObject("moonpapTxn");

                                    txt_petranid.setText(dataObj1.getString("ext_txn_id"));

                                  //  txt_pnetfee.setText(dataObj1.getString("moon_networkFeeAmount"));




                                    txt_pmoonfee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_feeAmount")));
                                    txt_petranid.setText(dataObj1.getString("ext_txn_id"));
                                    txt_pbitfee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_extraFeeAmount")));
                                    // txt_pnetfee.setText(dataObj1.getString("moon_networkFeeAmount"));

                                    txt_tranfree.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_networkFeeAmount")));
                                    txt_ptotal.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount")));
                                    txt_pconf.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_baseCurrencyAmount")));

                                    lly_moonpfee.setVisibility(View.VISIBLE);
                                    lly_bitwalletfee.setVisibility(View.VISIBLE);
                                    lly_networkfee.setVisibility(View.GONE);
                                    lly_amount.setVisibility(View.GONE);
                                    lly_confirmation.setVisibility(View.VISIBLE);
                                    lly_etxtid.setVisibility(View.VISIBLE);


                                }else if (txt_type.equals("BUY")) {
                                    txt_ptansid.setText(dataObj.getString("txn_id"));
                                    txt_paddress.setText(dataObj.getString("to_address"));
                                    txt_pcreate.setText(dataObj.getString("timestamp"));




                                    txt_ptsentvalu.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_amount")));
                                    txt_pratesentvalue.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount")));
                                    txt_pcratefee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("fee")));


                                    txt_pstatus.setText(txt_type);
                                    txt_tranname.setText("Transacted Value");
                                    txt_cratename.setText("Current Rate Value");
                                    Double amt= Double.valueOf(dataObj.getString("amount"));

                                    final double Usdrate = Double.valueOf(ACU.MySP.getFromSP(context,ACU.MySP.USD_RATE,""));
                                    final float walletDollerAmt = (float) (amt * Usdrate);
                                    DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                    final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                    txt_btcamt.setText("$"+strWalletDollerAmt);
                                    if(dataObj.getString("confirmation").equals(0)){
                                        txt_mainstatus.setText("unconfirmed");
                                    }else {
                                        txt_mainstatus.setText("confirmed");
                                    }
                                    txt_psell.setText(dataObj.getString("amount")+" BTC");
                                    JSONObject dataObj1 = dataObj.getJSONObject("moonpapTxn");

                                    txt_pmoonfee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_feeAmount")));
                                    txt_petranid.setText(dataObj1.getString("ext_txn_id"));
                                    txt_pbitfee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_extraFeeAmount")));
                                   // txt_pnetfee.setText(dataObj1.getString("moon_networkFeeAmount"));

                                    txt_tranfree.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_networkFeeAmount")));
                                    txt_ptotal.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount")));
                                    txt_pconf.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj1.getString("moon_baseCurrencyAmount")));

                                    lly_moonpfee.setVisibility(View.VISIBLE);
                                    lly_bitwalletfee.setVisibility(View.VISIBLE);
                                    lly_networkfee.setVisibility(View.GONE);
                                    lly_amount.setVisibility(View.GONE);
                                    lly_confirmation.setVisibility(View.VISIBLE);
                                    lly_etxtid.setVisibility(View.VISIBLE);
                                }

                                else if (txt_type.equals("REC")) {
                                    txt_ptansid.setText(dataObj.getString("txn_id"));
                                    txt_paddress.setText(dataObj.getString("to_address"));
                                    txt_pcreate.setText(dataObj.getString("timestamp"));
                                    txt_ptsentvalu.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_amount"))+ " BTC");
                                    txt_pratesentvalue.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("amount"))+ " BTC");
                                    txt_pcratefee.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("fee"))+ " BTC");
                                    txt_tranfree.setText(Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_fee"))+ " BTC");

                                    Log.d(TAG, "onPostExecute: "+ " "+ Miscellaneous.CustomsMethods.convertToBTCFormat(dataObj.getString("old_amount")));

                                    txt_pstatus.setText("RECEIVE");
                                    txt_cratename.setText("Current Rate Value");
                                    Double amt= Double.valueOf(dataObj.getString("amount"));
                                    txt_tranname.setText("Transacted Value");
                                    final double Usdrate = Double.valueOf(ACU.MySP.getFromSP(context,ACU.MySP.USD_RATE,""));
                                    final float walletDollerAmt = (float) (amt * Usdrate);
                                    DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                    final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                    txt_btcamt.setText("$"+strWalletDollerAmt);
                                    txt_psell.setText(dataObj.getString("amount")+" BTC");

                                    if(dataObj.getString("confirmation").equals(0)){
                                        txt_mainstatus.setText("unconfirmed");
                                    }else {
                                        txt_mainstatus.setText("confirmed");
                                    }
                                    lly_moonpfee.setVisibility(View.GONE);
                                    lly_bitwalletfee.setVisibility(View.GONE);
                                    lly_networkfee.setVisibility(View.GONE);
                                    lly_amount.setVisibility(View.GONE);
                                    lly_confirmation.setVisibility(View.GONE);
                                    lly_etxtid.setVisibility(View.GONE);
                                }
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();
    }
}