package com.Android.Inc.bitwallet.ServiceCall;

import android.content.Context;
import android.os.Debug;
import android.util.Log;

import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.SSl_Classes.IOUtil;
import com.Android.Inc.bitwallet.SSl_Classes.TLSSocketFactory;
import com.Android.Inc.bitwallet.utils.ACU;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class RestAPIClientStateCity {

    public static class APIClient {

        private static final String TAG = API.class.getSimpleName();
        private static Context context;
        private static String live_url = ACU.MySP.MAIN_URL;


        public static String getRemoteCall(RestAPIClientHelper helper, Context ctx) {
            Log.e(TAG, "getRemoteCall: helper: " + helper.toString());
            context = ctx;
            HttpURLConnection connection = null;
            String auth_code = null;
            byte[] postData = null;
            String url = null;
            int postDataLength = 0, lastResponseCode = 0;
            String result = null;

            if (helper != null) {
                try {

                    url = helper.getRequestUrl();
                    Log.e(TAG, "Endpoint_URL : " + url);
                    connection = (HttpURLConnection) new URL(url).openConnection();


                    connection.setRequestMethod(helper.getMethodType());
                    // connection.setRequestProperty("Accept", "application/json");
                    //  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    //  connection.setRequestProperty("Content-Type", helper.getContentType());
                    connection.setRequestProperty("Accept", helper.getAccept());
                    connection.setRequestProperty("Authorization", helper.getAuthorization());
                    connection.setRequestProperty("api-token", helper.getApiToken());
                    connection.setRequestProperty("user-email", "bitwalletinc@gmail.com");
                    connection.setInstanceFollowRedirects(false);
                    connection.setUseCaches(true);
                    connection.setDoInput(true);

                    lastResponseCode = connection.getResponseCode();
                    ACU.MySP.RESPONSE_CODE = lastResponseCode;
                    Log.e(TAG, "getRemoteCall: lastResponseCode:ACU " + ACU.MySP.RESPONSE_CODE);
                    result = IOUtil.readFully(connection.getInputStream());
                    Log.e(TAG, "getRemoteCall: result : " + result);

                } catch (Exception ex) {
                    Log.e(TAG, " catch getRemoteCall: " + ex.getMessage());
                    ex.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }

            }
            return result;
        }
    }

}
