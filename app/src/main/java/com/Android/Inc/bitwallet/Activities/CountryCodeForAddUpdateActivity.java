package com.Android.Inc.bitwallet.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.adapters.ListCountryAddUpdateAdapter;
import com.Android.Inc.bitwallet.utils.ACU;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CountryCodeForAddUpdateActivity extends AppCompatActivity  implements LifecycleObserver {

    private Context context;
    Activity activity;
    LinearLayout ll_backArrow;
    private static final String TAG = CountryCodeForAddUpdateActivity.class.getSimpleName();

    List<String> countryNameList, countryCodeList;
    ListView listView;
    SearchView searchView;
    ListCountryAddUpdateAdapter adapter;
    private List<ListItemModel> listItemListModel;
    String screenType;
    private boolean minimizeBtnPressed =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_country_code_for_add_update_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_country_code_for_add_update);
        }
        context = CountryCodeForAddUpdateActivity.this;
        activity = CountryCodeForAddUpdateActivity.this;
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        countryNameList = Arrays.asList(getResources().getStringArray(R.array.countryName));
        countryCodeList = Arrays.asList(getResources().getStringArray(R.array.countryCode));
        initialize();
           ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     startActivity(new Intent(context,AddUpdateEmployeeActivity.class));
                finish();
            }
        });

    }

    private void initialize() {
        listView = findViewById(R.id.list_view);
        searchView = findViewById(R.id.search);
        listItemListModel = new ArrayList<>();

        for (int i = 0; i < countryCodeList.size(); i++) {
            ListItemModel listItemModel = new ListItemModel();
            listItemModel.setName(countryNameList.get(i));
            listItemModel.setCode(countryCodeList.get(i));

            listItemListModel.add(listItemModel);

        }

        adapter = new ListCountryAddUpdateAdapter(listItemListModel, context);
        listView.setAdapter(adapter);

        searchView.setActivated(true);
        searchView.setQueryHint("Select Your Country");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    adapter.filter("");
                    listView.clearTextFilter();

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

    }
    //Home button pressed or get a call or sleep mode
    @Override
    protected void onUserLeaveHint() {

        minimizeBtnPressed = true;
        finish();

        super.onUserLeaveHint();
    }
    @Override
    protected void onStop() {
        Log.e("MyAppSocial", "Stop method called");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "out_from_outsideView");
      /*  if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        }else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }*/
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyAppSocial", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");
        // ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        // finish();

        /*if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }*/

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyAppSocial", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, "onBackPressed: ");
        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        super.onBackPressed();
    }

}
