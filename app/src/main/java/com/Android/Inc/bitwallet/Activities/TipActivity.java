package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.GenerateQRCode;
import com.Android.Inc.bitwallet.utils.SocketListener;
import com.Android.Inc.bitwallet.utils.Tools;
import com.Android.Inc.bitwallet.utils.VU;
import com.autofit.et.lib.AutoFitEditText;

//import org.java_websocket.client.WebSocketClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

import tech.gusavila92.websocketclient.WebSocketClient;

public class TipActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    private TextView txtNotip, txttenPtip, txtfifteenPtip, txttwentyPtip, txtCustomPtip,
            txtBtcAmt, txtAdddresskey, txtTotalDollarAmt, txtChargedAmtTablet, txtEmpId;
    private AutoFitEditText txtChargedAmtmobile;
    private RelativeLayout rl_barcode;
    private ImageView  imgBarcode;

    private WebSocketClient webSocketClient;


    // imgFaceboook, imgTwitter, imgLinkedIn,
    private LinearLayout ll_backArrow, ll_paymentWaiting, llSelectTip, llAddTip,llEmpId;
    private Context context;
    private ProgressBar progressBar;
    private WebSocket webSocket;
    private String status_code = "", str_address = "", emp_id = "",strWalletId = "";
    private View layout;
    String screenType;
    private boolean connectionEstablished = false;
    private boolean minimizeBtnPressed = false;

    private API exampleApi;
    private static final String TAG = TipActivity.class.getSimpleName();
    public static String getAddressKeyUrl =  "business/v2/get-address";
    private AsyncTask mtaskReceive = null;
    private boolean isTipSwitchOn,isEmpSwitchOn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();

        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_tip_tab);
            txtChargedAmtTablet = findViewById(R.id.txt_charged_amt);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_tip);
            txtChargedAmtmobile = findViewById(R.id.txt_charged_amt);
        }

        context = TipActivity.this;
        intialize();
        getIntentData();
        isTipSwitchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_TIP_SWITCH_ON, false);
        isEmpSwitchOn = ACU.MySP.getSPBoolean(context, ACU.MySP.IS_EMP_SWITCH_ON, false);
        Log.e(TAG, "onCreate: "+isTipSwitchOn );
        if (isTipSwitchOn) {
            llSelectTip.setVisibility(View.VISIBLE);
            llAddTip.setVisibility(View.VISIBLE);
        } else {
            llSelectTip.setVisibility(View.GONE);
            llAddTip.setVisibility(View.GONE);
        }

        if (isEmpSwitchOn) {
            llEmpId.setVisibility(View.VISIBLE);
        }else{
            llEmpId.setVisibility(View.GONE);
        }


        if (isConnectingToInternet(context, screenType)) {
            getAddressKeyApi();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

    }

//    private void  initiateWebsocket() {
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder().url("wss://ws.blockchain.info/inv").build();
//        SocketListener socketListener = new SocketListener(this, str_address);
//        webSocket = client.newWebSocket(request, socketListener);
//        connectionEstablished = true;
//        Log.e("address", str_address);
//        //  Log.e("address","address");
//        webSocket.send("{op:addr_sub,addr:" + str_address + "}");
//    }


    //New Web socket by sachin
    private void initiateWebSocketClient() {
        URI uri;
        try {
            Log.e(TAG, "Address: "+str_address );
            uri = new URI("wss://www.blockonomics.co/payment/"+str_address);
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        webSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen() {
                System.out.println("onOpen");
                Log.e("TAG", "onOpen: " );
              //11  getWebsocketData("{\"status\": 0, \"timestamp\": 1645094007, \"value\": 2310, \"txid\": \"590112b21e1455fe0db4c4dcce4b3c1c6330af9e380d3d46fbbcb17dd35ced87\"}");
                webSocketClient.send("Hello, World!");
            }


            @Override
            public void onTextReceived(String message) {
                System.out.println("onTextReceived");
                Log.e("TAG", "onTextReceived 5: "+message.toString() );

                getWebsocketData(message.toString());

            }

            @Override
            public void onBinaryReceived(byte[] data) {
                System.out.println("onBinaryReceived");
                Log.e("TAG", "onBinaryReceived 2: "+ data.toString() );

            }

            @Override
            public void onPingReceived(byte[] data) {
                System.out.println("onPingReceived");
                Log.e("TAG", "onPingReceived 1: "+ data.toString() );

            }

            @Override
            public void onPongReceived(byte[] data) {
                System.out.println("onPongReceived");
                Log.e("TAG", "onPongReceived 3: " + data.toString());
            }

            @Override
            public void onException(Exception e) {
                System.out.println(e.getMessage());
                Log.e("TAG", "Exception 4: " + e.getMessage());

            }

            @Override
            public void onCloseReceived() {
                System.out.println("onCloseReceived");
                Log.e("TAG", "onCloseReceived: " );
            }
        };

        webSocketClient.setConnectTimeout(10000);
        webSocketClient.setReadTimeout(60000);
        // webSocketClient.addHeader("Origin", "http://developer.example.com");
        webSocketClient.enableAutomaticReconnection(5000);
        webSocketClient.connect();
    }


    private void  getWebsocketData(String data) {

        Log.e(TAG, "Websocket Data: " );

        try {
            JSONObject dataObject = new JSONObject(data);
            int status = dataObject.getInt("status");
            String timestamp = dataObject.getString("timestamp");
            String txid = dataObject.getString("txid");
            double value = dataObject.getDouble("value");

            Log.e(TAG, "Websocket Data: " );
            Log.e(TAG, "getWebsocketData: status- "+status );
            Log.e(TAG, "getWebsocketData: timestamp- "+timestamp );
            Log.e(TAG, "getWebsocketData: txid- "+txid );
            Log.e(TAG, "getWebsocketData: value- "+value );


            Intent intent = new Intent(TipActivity.this, PaymentReceivedMerchandActivity.class);
            intent.putExtra("senderAddress", txid);
            intent.putExtra("valueBtc", value);
            intent.putExtra("hash", txid);
            intent.putExtra("timeStamp", timestamp);
            intent.putExtra("receiverAddress", str_address);
            startActivity(intent);
            webSocketClient.close();
            finish();

        }catch (JSONException err){
            Log.d("WebsocketData Error", err.toString());
        }

    }




    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            emp_id = extras.getString("EmpId");
            txtEmpId.setText(emp_id);
        }
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            Log.e(TAG, "onResume: in if");
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        if (connectionEstablished) {
            //webSocket.cancel();
            webSocketClient.close();
        }
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }
    private void intialize() {
        txtNotip = findViewById(R.id.txt_no_tip);
        txtEmpId = findViewById(R.id.emp_code);
        txttenPtip = findViewById(R.id.txt_10_p_tip);
        txtfifteenPtip = findViewById(R.id.txt_15_p_tip);
        txttwentyPtip = findViewById(R.id.txt_20_p_tip);
        txtCustomPtip = findViewById(R.id.txt_custom_tip);
        txtAdddresskey = findViewById(R.id.txt_adddresskey);
        txtTotalDollarAmt = findViewById(R.id.txt_total_dollar_amt);
        txtBtcAmt = findViewById(R.id.txt_btc_amt);
        llSelectTip = findViewById(R.id.rl_tip);
        llAddTip = findViewById(R.id.ll);
        progressBar = findViewById(R.id.receive_progressBar);
        imgBarcode = findViewById(R.id.img_barcode);
        rl_barcode = findViewById(R.id.rl_img_barcode);
        llEmpId = findViewById(R.id.ll_emp_id);
        //  btnPaymentComplete = findViewById(R.id.btn_payment_complete);

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        ll_paymentWaiting = findViewById(R.id.ll_payment_waiting);
//        imgFaceboook = findViewById(R.id.img_facebook);
//        imgTwitter = findViewById(R.id.img_twitter);
//        imgLinkedIn = findViewById(R.id.img_linkedIn);

        txtNotip.setOnClickListener(this);
        txttenPtip.setOnClickListener(this);
        txtfifteenPtip.setOnClickListener(this);
        txttwentyPtip.setOnClickListener(this);
        txtCustomPtip.setOnClickListener(this);
        //    txtGetReceipt.setOnClickListener(this);

//        imgFaceboook.setOnClickListener(this);
//        imgTwitter.setOnClickListener(this);
//        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);
        //    btnPaymentComplete.setOnClickListener(this);

        txtNotip.setBackgroundResource(R.color.white);
        txtNotip.setTextColor(Color.BLACK);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.txt_no_tip:
                txtNotip.setBackgroundResource(R.color.white);
                txtNotip.setTextColor(Color.BLACK);
                txttenPtip.setBackgroundResource(R.color.black);
                txttwentyPtip.setBackgroundResource(R.color.black);
                txtfifteenPtip.setBackgroundResource(R.color.black);
                txtCustomPtip.setBackgroundResource(R.color.black);
                txttenPtip.setTextColor(Color.WHITE);
                txttwentyPtip.setTextColor(Color.WHITE);
                txtfifteenPtip.setTextColor(Color.WHITE);
                txtCustomPtip.setTextColor(Color.WHITE);
                claculatePercentTip(0);
                ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, "0");
                break;
            case R.id.txt_10_p_tip:
                txttenPtip.setBackgroundResource(R.color.white);
                txttenPtip.setTextColor(Color.BLACK);
                txtNotip.setBackgroundResource(R.color.black);
                txttwentyPtip.setBackgroundResource(R.color.black);
                txtfifteenPtip.setBackgroundResource(R.color.black);
                txtCustomPtip.setBackgroundResource(R.color.black);
                txtNotip.setTextColor(Color.WHITE);
                txttwentyPtip.setTextColor(Color.WHITE);
                txtfifteenPtip.setTextColor(Color.WHITE);
                txtCustomPtip.setTextColor(Color.WHITE);
                claculatePercentTip(10);
                ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, "10");
                break;
            case R.id.txt_15_p_tip:
                txtfifteenPtip.setBackgroundResource(R.color.white);
                txtfifteenPtip.setTextColor(Color.BLACK);
                txtNotip.setBackgroundResource(R.color.black);
                txttwentyPtip.setBackgroundResource(R.color.black);
                txttenPtip.setBackgroundResource(R.color.black);
                txtCustomPtip.setBackgroundResource(R.color.black);
                txtNotip.setTextColor(Color.WHITE);
                txttwentyPtip.setTextColor(Color.WHITE);
                txttenPtip.setTextColor(Color.WHITE);
                txtCustomPtip.setTextColor(Color.WHITE);
                claculatePercentTip(15);
                ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, "15");
                break;
            case R.id.txt_20_p_tip:
                txttwentyPtip.setBackgroundResource(R.color.white);
                txttwentyPtip.setTextColor(Color.BLACK);
                txtNotip.setBackgroundResource(R.color.black);
                txtfifteenPtip.setBackgroundResource(R.color.black);
                txttenPtip.setBackgroundResource(R.color.black);
                txtCustomPtip.setBackgroundResource(R.color.black);
                txtNotip.setTextColor(Color.WHITE);
                txtfifteenPtip.setTextColor(Color.WHITE);
                txttenPtip.setTextColor(Color.WHITE);
                txtCustomPtip.setTextColor(Color.WHITE);
                claculatePercentTip(20);
                ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, "20");
                break;
            case R.id.txt_custom_tip:
                txtCustomPtip.setBackgroundResource(R.color.white);
                txtCustomPtip.setTextColor(Color.BLACK);
                txtNotip.setBackgroundResource(R.color.black);
                txtfifteenPtip.setBackgroundResource(R.color.black);
                txttenPtip.setBackgroundResource(R.color.black);
                txttwentyPtip.setBackgroundResource(R.color.black);
                txtNotip.setTextColor(Color.WHITE);
                txtfifteenPtip.setTextColor(Color.WHITE);
                txttenPtip.setTextColor(Color.WHITE);
                txttwentyPtip.setTextColor(Color.WHITE);
                dialogCustomTip();
                break;
          /*  case R.id.btn_payment_complete:
                onBackPressed();
                break;*/
           /* case R.id.txt_get_receipt:
                ACU.MySP.getFromSP(context, ACU.MySP.TIP_AMT, "");
                ACU.MySP.saveSP(context, ACU.MySP.CHARGED_AMT, "0");
                startActivity(new Intent(context, ReciptMerchandActivity.class));
                finish();
                break;*/
//            case R.id.img_facebook:
//                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
//                break;
//            case R.id.img_linkedIn:
//                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
//                break;
//            case R.id.img_twitter:
//                socialMedia("twitter", "https://twitter.com/bitwalletinc");
//                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (connectionEstablished) {
            //webSocket.cancel();
            webSocketClient.close();
        }
        ACU.MySP.saveSP(context, ACU.MySP.CHARGED_AMT, "0");
        if (ACU.MySP.getSPBoolean(context,ACU.MySP.IS_EMP_SWITCH_ON,false)) {
            startActivity(new Intent(context, SelectEmployeeFromListActivity.class));
        }else{
            startActivity(new Intent(context, ChargeBitwalletActivity.class));
        }
        finish();
    }

    public void socialMedia(String flag, String url) {
        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    private void claculatePercentTip(double tipAmt) {
        try {
            double percentOnChargedAmt = (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.CHARGED_AMT, ""))) * ((tipAmt / 100));
            double totalDollarAmt = ((Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.CHARGED_AMT, ""))) + (percentOnChargedAmt));
            DecimalFormat dollarNumberFormat = new DecimalFormat("#,###,##0.00");
            String strTotalDollerAmt = dollarNumberFormat.format(totalDollarAmt);
            String strpercentOnChargedAmt = dollarNumberFormat.format(percentOnChargedAmt);
            String strChargedAmt = dollarNumberFormat.format(Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.CHARGED_AMT, "")));

            if (screenType.equalsIgnoreCase("tablet")) {
                txtChargedAmtTablet.setText("$ " + strChargedAmt + " + $ " + strpercentOnChargedAmt + " TIP");
                txtTotalDollarAmt.setText("$" + strTotalDollerAmt);
            } else {
                txtChargedAmtmobile.setText("$ " + strChargedAmt + " + $ " + strpercentOnChargedAmt + " TIP");
                txtTotalDollarAmt.setText(strTotalDollerAmt);
            }
            ACU.MySP.saveSP(context, ACU.MySP.TIP_RECEIPT_DOLLAR_AMT, strChargedAmt);
            ACU.MySP.saveSP(context, ACU.MySP.TIP_AMT, strpercentOnChargedAmt);


            final float btcAmt = (float) (totalDollarAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

            DecimalFormat btcNumberFormat = new DecimalFormat("#0.00000000");
            final String strBtcAmt = btcNumberFormat.format(btcAmt);
            ACU.MySP.saveSP(context, ACU.MySP.TIP_RECEIPT_BTC_AMT, strBtcAmt);
            Log.e(TAG, "claculatePercentTip: " + strBtcAmt);
            txtBtcAmt.setText(strBtcAmt + " BTC");
            Log.e(TAG, "claculatePercentTip: " + txtBtcAmt.getText().toString());

            //   String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + str_address + "?amount=" + strBtcAmt;
            String url = "bitcoin:" + str_address + "?amount=" + strBtcAmt;
            Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
            imgBarcode.setImageBitmap(bitmapQRcode);
        } catch (Exception e) {
            CustomDialogs.dialogShowMsg(context,screenType,getResources().getString(R.string.provide_valid_input));
        }

    }
    private void getAddressKeyApi() {

        mtaskReceive = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @SuppressLint("WrongThread")
            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                strWalletId=ACU.MySP.getFromSP(context,ACU.MySP.WALLET_BUSINESS_ID,"");
                try {
                    urlParameters = "txn_type=REC"+"&empcode="+
                            "&wallet_id="+strWalletId+"&wallettype="+ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "");
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(getAddressKeyUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                   /* exampleApi = new API(context, ACU.MySP.getFromSP(context, ACU.MySP.AUTH_TOKEN, ""));
                    s = exampleApi.doGet(getAddressKeyUrl, "TipActivity");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    progressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                ll_paymentWaiting.setVisibility(View.VISIBLE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                String msg = null;
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context,screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        msg = jsonObject.getString("message");
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        str_address = dataObj.getString("address");
                        String txnType = dataObj.getString("txn_type");
                        status_code = jsonObject.getString("status_code");
                        txtAdddresskey.setText(str_address);
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context,screenType);
                        }else if(status_code.equalsIgnoreCase("800")){
                            CustomDialogs.dialogAppMantenance(context,screenType);
                        }
                        ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, "0");
                        claculatePercentTip(0);
                        rl_barcode.setVisibility(View.VISIBLE);
                        //initiateWebsocket();
                        initiateWebSocketClient();
                       /* String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + str_address + "?amount=0";
                        Tools.displayImageOriginalString(context, imgBarcode, url);*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomDialogs.dialogRequestTimeOut(context,screenType);
                }
            }
        }.execute();
    }

    public void dialogCustomTip() {
        final Dialog dialog = new Dialog(context);
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_custom_tip_tab);
        } else {
            dialog.setContentView(R.layout.dialog_custom_tip);
        }
        dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final EditText edtTip = (EditText) dialog.findViewById(R.id.edt_tip);
        ((Button) dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.hideKeyboard((TipActivity) context);
                dialog.dismiss();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_submit)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                Tools.hideKeyboard((TipActivity) context);
                if ((!edtTip.getText().toString().equalsIgnoreCase("")) && (Long.valueOf(edtTip.getText().toString()) < 101)) {
                    ACU.MySP.saveSP(context, ACU.MySP.TIP_PERCENT, edtTip.getText().toString());
                    claculatePercentTip(Integer.valueOf(edtTip.getText().toString()));
                    dialog.dismiss();
                } else {
                    if ((edtTip.getText().toString().equalsIgnoreCase(""))) {
                        CustomToast.custom_Toast(context, getResources().getString(R.string.Add_TIP_Amount), layout);
                    } else {
                        CustomToast.custom_Toast(context, getResources().getString(R.string.TIP_Should_Be_Less), layout);
                    }
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean isConnectingToInternet(Context appContext, String screenType) {
        // Method to check internet connection
        ConnectivityManager conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {

            return true;
        } else {
            // Toast.makeText(appContext, "No internet connection.", Toast.LENGTH_SHORT).show();
            showCustomDialog(appContext, screenType);
            return false;
        }
    }


    public void showCustomDialog(final Context appContext, String screenType) {
        final Dialog dialog = new Dialog(appContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_warning_tab);
        } else {
            dialog.setContentView(R.layout.dialog_warning);
        }
        dialog.setCancelable(false);

        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                // Method to check internet connection
                ConnectivityManager conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected()) {
                    dialog.cancel();
                    getAddressKeyApi();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


}
