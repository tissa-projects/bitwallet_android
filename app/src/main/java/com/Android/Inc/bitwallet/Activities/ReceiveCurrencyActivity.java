package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.GenerateQRCode;
import com.Android.Inc.bitwallet.utils.Tools;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

public class ReceiveCurrencyActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow, llImgShare, llImgCopyAddress;
    RelativeLayout rlImgBarcode;
    ImageView imgBarcode;
    View layout;
    Context context;
    TextView txtAddresskey;
    ProgressBar progressBar;
    private String status_code = "";
    private boolean isShare = false;
    String screenType;
    private boolean minimizeBtnPressed = false;

    private static final String TAG = ReceiveCurrencyActivity.class.getSimpleName();
    public static String getAddressKeyUrl = "business/v2/get-address";
    private AsyncTask mtaskReceive = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_receive_currency_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_receive_currency);
        }
        context = ReceiveCurrencyActivity.this;
        initialize();

        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        if (VU.isConnectingToInternet(context, screenType)) {
            getAddressKeyApi();
        }

        // for sharing img this line is imp other wise img format not support
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (isShare) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        }
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private void initialize() {
        llImgShare = findViewById(R.id.ll_img_share);
        imgBarcode = findViewById(R.id.img_barcode);
        llImgCopyAddress = findViewById(R.id.ll_img_copyAddress);
        rlImgBarcode = findViewById(R.id.rl_img_barcode);
        txtAddresskey = findViewById(R.id.txt_hashkey);
        progressBar = findViewById(R.id.receive_progressBar);

        llImgShare.setEnabled(false);
        llImgCopyAddress.setEnabled(false);

        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        llImgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShare = true;
                ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                takeScreenshot();
            }
        });

        llImgCopyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textHashkey = txtAddresskey.getText().toString();
                setClipboard(context, textHashkey);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            isShare = false;
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, TransactionActivity.class));
        finish();
    }

    // copy hashkey on button click
    private void setClipboard(Context context, String text) {
        View layout = getLayoutInflater().inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, text);
            CustomToast.custom_Toast_CopyKey(context, "Address copied", layout);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
            CustomToast.custom_Toast_CopyKey(context, "Address copied", layout);
            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ADDRESS, text);


        }
    }

    public void takeScreenshot() {
        Bitmap bitmap = getBitmapFromView(rlImgBarcode);
        //This function added by niraj
        onShareImage(bitmap);

        //Old code commented by Niraj (its not working on android 11 or above)
        /*try {
            File file = new File(getExternalCacheDir(), "barcode.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);

            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.putExtra(Intent.EXTRA_TEXT, txtAddresskey.getText().toString());
            intent.setType("image/png");
            startActivity(Intent.createChooser(intent, "Share via"));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    //Sending image - New Method (Added by Niraj)
    public void onShareImage(Bitmap bitmap) {
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
            Uri imageUri = Uri.parse(path);

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            waIntent.setType("image/*");
            waIntent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);

            waIntent.putExtra(Intent.EXTRA_TEXT, txtAddresskey.getText().toString());

            context.startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception e) {
            Log.e("Error on sharing", e + " ");
            Toast.makeText(context, "App not Installed", Toast.LENGTH_SHORT).show();
        }
    }


    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }


    private void getAddressKeyApi() {

        mtaskReceive = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    //urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&amount=" + 0;
                    urlParameters = "txn_type=REC" + "&empcode=" +
                            "&wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&wallettype=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "");
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(getAddressKeyUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                  /*  exampleApi = new API(context, ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, ""), 0);
                    s = exampleApi.doGet(getAddressKeyUrl, "RequestCurrencyActivity");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                rlImgBarcode.setVisibility(View.VISIBLE);
                String msg = null;
                try {
                    if (s == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {

                        JSONObject jsonObject = new JSONObject(s);
                        msg = jsonObject.getString("message");

                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        if (dataObj != null) {
                            String str_address = dataObj.getString("address");
                            String strtxnType = dataObj.getString("txn_type");
                            txtAddresskey.setText(str_address);
                            /*String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + str_address + "?amount=" + 0;
                            Tools.displayImageOriginalString(context, imgBarcode, url);*/
                            String url = "bitcoin:" + str_address + "?amount=" + 0;
                            Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                            imgBarcode.setImageBitmap(bitmapQRcode);
                            llImgShare.setEnabled(true);
                            llImgCopyAddress.setEnabled(true);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    //  CustomToast.custom_Toast(contextDashBoard,msg,layout);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @Override
    public void onDestroy() {
        if (mtaskReceive != null && mtaskReceive.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskReceive.cancel(true);
        }
        super.onDestroy();
    }


}
