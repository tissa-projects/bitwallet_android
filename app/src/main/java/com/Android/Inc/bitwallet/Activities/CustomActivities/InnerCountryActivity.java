package com.Android.Inc.bitwallet.Activities.CustomActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.Android.Inc.bitwallet.Activities.PinActivity;
import com.Android.Inc.bitwallet.Models.ListItemModel;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.adapters.InnerCountryCodeAdapter;
import com.Android.Inc.bitwallet.adapters.ListCountryAdapter;
import com.Android.Inc.bitwallet.utils.ACU;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InnerCountryActivity extends AppCompatActivity {


    private Context context;
    Activity activity;
    LinearLayout ll_backArrow;
    private static final String TAG = CountryCodeActivity.class.getSimpleName();

    List<String> countryNameList, countryCodeList,countryShortName;
    ListView listView;
    SearchView searchView;
    InnerCountryCodeAdapter adapter;
    private boolean minimizeBtnPressed = false;
    private List<ListItemModel> listItemListModel;
    String screenType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_country_code_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_country_code);
        }

        context = InnerCountryActivity.this;
        activity = InnerCountryActivity.this;
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        countryNameList = Arrays.asList(getResources().getStringArray(R.array.countryName));
        countryCodeList = Arrays.asList(getResources().getStringArray(R.array.countryCode));
        countryShortName = Arrays.asList(getResources().getStringArray(R.array.country_short_name));
        initialize();

        ll_backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initialize() {
        listView = findViewById(R.id.list_view);
        searchView = findViewById(R.id.search);
        listItemListModel = new ArrayList<>();

        for (int i = 0; i < countryCodeList.size(); i++) {
            ListItemModel listItemModel = new ListItemModel();
            listItemModel.setName(countryNameList.get(i));
            listItemModel.setCode(countryCodeList.get(i));
            listItemModel.setShortName(countryShortName.get(i));

            listItemListModel.add(listItemModel);

        }

        adapter = new InnerCountryCodeAdapter(listItemListModel, context);
        listView.setAdapter(adapter);

        searchView.setActivated(true);
        searchView.setQueryHint("Select Your Country");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    adapter.filter("");
                    listView.clearTextFilter();

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

    }
    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, PinActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }
}
