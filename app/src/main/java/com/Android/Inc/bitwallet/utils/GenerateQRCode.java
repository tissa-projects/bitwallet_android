package com.Android.Inc.bitwallet.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import java.util.HashMap;
import java.util.Map;

public class GenerateQRCode {

    public static class myQRcode {

       /* public static Bitmap generateQRCode(Context context,String url) {
            Bitmap bitmap = null;
            QRGEncoder qrgEncoder;
            String TAG = "GenerateQRCode";

            WindowManager manager = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
         //   smallerDimension = smallerDimension * 3 / 4;

            qrgEncoder = new QRGEncoder(
                    url, null,
                    QRGContents.Type.TEXT,
                    smallerDimension);
            try {
                bitmap = qrgEncoder.encodeAsBitmap();
            } catch (WriterException e) {
                Log.v(TAG, e.getMessage());
            }
            
                return bitmap;
        }
*/
        public static Bitmap generateQR(Context context,String url){
            Bitmap returnBitmap= null;
            String TAG = "GenerateQRCode";
            final int QR_SIZE = 670;
            try {
               /* Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
                hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                QRCodeWriter qrCodeWriter = new QRCodeWriter();
                BitMatrix matrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, QR_SIZE, QR_SIZE, hintMap);*/

                Map<EncodeHintType, Object> hintMap = new HashMap<>();
                hintMap.put(EncodeHintType.MARGIN, new Integer(1));
                BitMatrix matrix = new MultiFormatWriter().encode(
                        new String(url.getBytes("utf-8"), "utf-8"),
                        BarcodeFormat.QR_CODE, QR_SIZE, QR_SIZE, hintMap);


                //int width = matrix.getWidth();
                int width = matrix.getHeight();
                int height = matrix.getHeight();
                int[] pixels = new int[width*height];
                for(int y=0; y<height; y++) {
                    for(int x=0; x<width; x++) {
                        int grey = matrix.get(x, y) ? 0x00 : 0xff;
                        pixels[y*width+x] = 0xff000000 | (0x00010101*grey);
                    }
                }
                returnBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                returnBitmap.setPixels(pixels, 0, width, 0, 0, width, height);
                //returnBitmap.set
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            return returnBitmap;
        }
    }

}
