package com.Android.Inc.bitwallet.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.Android.Inc.bitwallet.R;
import com.google.gson.JsonArray;

import org.json.JSONArray;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.MyViewHolder> {

    Context context;
    JSONArray jsonArray;
    private static final String TAG = EmployeeAdapter.class.getSimpleName();

    public EmployeeAdapter(Context context, JSONArray jsonArray){
        this.context =context;
        this.jsonArray =jsonArray;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_transaction_tab, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_transaction, parent, false);
        }

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        if (holder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) holder;
            try {


            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: " + e.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDollerAmt, txtDate, txtCatagory, txtViewOnBlockChain, txtConfirmed;
        LinearLayout ll_viewOnBlockChain;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtDollerAmt = (TextView) itemView.findViewById(R.id.txt_dollerAmt);
            this.txtDate = (TextView) itemView.findViewById(R.id.txt_date);
            this.txtCatagory = (TextView) itemView.findViewById(R.id.txt_category);
            this.txtViewOnBlockChain = (TextView) itemView.findViewById(R.id.txt_viewOnBlockChain);
            this.txtConfirmed = (TextView) itemView.findViewById(R.id.txt_confirmed);
            this.ll_viewOnBlockChain = (LinearLayout) itemView.findViewById(R.id.ll_viewOnBlockChain);
        }
    }
}
