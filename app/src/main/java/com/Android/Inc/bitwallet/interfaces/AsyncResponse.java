package com.Android.Inc.bitwallet.interfaces;

public interface AsyncResponse {
    void processResponse(String response);
}
