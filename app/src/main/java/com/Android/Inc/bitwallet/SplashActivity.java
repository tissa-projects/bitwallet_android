package com.Android.Inc.bitwallet;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.Android.Inc.bitwallet.Activities.MainActivity;
import com.Android.Inc.bitwallet.BackgroundServices.UsdRateService;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;


public class SplashActivity extends AppCompatActivity {
    Context context;
    private static int SPLASH_TIME_OUT = 2000;
    private PendingIntent pendingIntent;
    private AlarmManager manager;
    private int interval = 500;
    long repeatInterval = 1000 * 3;
    long triggerTime = 1000;

    //+ repeatInterval
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_splash);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_splash_tab);
        } else {
            setContentView(R.layout.activity_splash);
        }
        context = SplashActivity.this;
         UsdRateServiceCall();

        appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
      //  checkUpdate();

    }

    private void UsdRateServiceCall() {
        Intent alarmIntent = new Intent(this, UsdRateService.class);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getBroadcast
                    (this, 0, alarmIntent, PendingIntent.FLAG_MUTABLE);
        }else{
            pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        }

        manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT >= 19) {
            manager.setRepeating(AlarmManager.RTC_WAKEUP,
                    triggerTime,
                    interval,
                    pendingIntent);
        } else {
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    triggerTime,
                    interval,
                    pendingIntent);
        }

//        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time, interval , pendingIntent);
        //  new UsdRateService(context);

        splashTime();

    }

    public void splashTime() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    /*In App Update*/
    private AppUpdateManager appUpdateManager;
    private static final int IMMEDIATE_APP_UPDATE_REQ_CODE = 124;

    private void checkUpdate() {

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                startUpdateFlow(appUpdateInfo);
            } else if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                startUpdateFlow(appUpdateInfo);
            } else {
                UsdRateServiceCall();
              // Toast.makeText(getApplicationContext(), "Update Not Available ", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startUpdateFlow(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, this, IMMEDIATE_APP_UPDATE_REQ_CODE);
        } catch (IntentSender.SendIntentException e) {
            Toast.makeText(getApplicationContext(), "Update canceled by user!" , Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMMEDIATE_APP_UPDATE_REQ_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "To use this app, Please update your application", Toast.LENGTH_LONG).show();
                checkUpdate();
            } else if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Update success! Result Code: " , Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "New version is available, Please update application form play store" , Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}
