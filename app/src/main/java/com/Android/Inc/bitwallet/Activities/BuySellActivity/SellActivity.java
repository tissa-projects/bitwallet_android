package com.Android.Inc.bitwallet.Activities.BuySellActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.Activities.AccountActivity;
import com.Android.Inc.bitwallet.Activities.SendingCurrencyActivity;
import com.Android.Inc.bitwallet.Activities.SocialMediaActivity;
import com.Android.Inc.bitwallet.Activities.TransactionActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class SellActivity extends AppCompatActivity implements View.OnClickListener, LifecycleObserver {

    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;
    private Button btnSubmit;
    private Context context;
    private AppCompatEditText edtBtcAmt, edtDollarAmt, edtPaymentMethod;
    private AsyncTask mtaskCreatePyMthd = null, mtasksell = null;
    private String screenType, strPublicToken = "", priority = "LOW";
    private ProgressBar progressBar;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private View bottom_sheet;
    private String strWyreAccId = null, strPaymentId = "", strFromActivity, strAddrProofStatus = "", strPaymentMthdStatus = "", strAddressSatus = "", strDobStatus = "",
            strNameStatus = "", strGovIdStatus = "", strCellPhnStatus = "", strEmailStatus = "", strSsnStatus = "", strAccStatus = "", strUploadStatus = "";
    private List<String> paymentNameList, paymentidList, paymentStatusList;
    TextWatcher generalTextWatcher = null;
    private int previousLength;
    private boolean backSpace, typing;
    private boolean minimizeBtnPressed = false;
    private static final String TAG = SellActivity.class.getSimpleName();

    public static String Get_Txn_Fee = "crypto-currency/get-txnfee/v1";// For Live
    // public static String Get_Txn_Fee = "crypto-currency/nownode/get-txnfee/v1";// For Testing 03-10-2022


    public static String checkPaymentAvailableUrl = "wyre/sell/get-payment-methods";
    public static final String Get_Moon_pay_urlnew = "moonpay/v2/get_widget_url";
    int c = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_sell_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_sell);
        }

        context = SellActivity.this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        initialize();
        conversionText();
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        } else {
            if (VU.isConnectingToInternet(context, screenType)) {
                checkPaymentMthdAvailable();
            }
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    private void initialize() {
        btnSubmit = findViewById(R.id.btn_submit);
        edtBtcAmt = findViewById(R.id.edt_btc_amt);
        edtDollarAmt = findViewById(R.id.edt_dollar_amt);
        edtPaymentMethod = findViewById(R.id.edt_payment_method);
        progressBar = findViewById(R.id.progressBar);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        paymentNameList = new ArrayList<>();
        paymentidList = new ArrayList<>();
        paymentStatusList = new ArrayList<>();


        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);

        bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);


        btnSubmit.setOnClickListener(this);
        edtPaymentMethod.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
//                    String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&send_to=" +
//                            "&txnType=SELL" + "&amount=" + edtBtcAmt.getText().toString() + "&username=" + "&wallettype=MAIN" +
//                            "&paymentId=" + strPaymentId + "&wyreAccId=" + strWyreAccId +
//                            "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes()) +
//                            "&priority=" + priority;
//                    Log.d(TAG, "onClick: " + urlParameters);
//                    Double s = Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, ""));
//                    Double b = Double.valueOf(5);
//
//                    Log.d(TAG, "onClick: " + s + "  " + b);

                    if (Validate()) {
                        if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, "")) > Double.valueOf(5)) {

                            getTxnFee();
                        } else
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.minimum_sell_amt));
                    }
                }
                break;

            case R.id.edt_payment_method:
                Log.e(TAG, "onClick: edt_payment_method");
                if (paymentidList.size() > 0) {
                    showBottomSheetDialog();
                } /*else {
                    if (isConnectingToInternet(context)) {
                        createPaymentMethod();
                    }
                }*/
                break;

            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", "https://www.instagram.com/bitwalletinc/");
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    public boolean Validate() {
        if (VU.isEmpty(edtBtcAmt)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_BTC_Amount));
            return false;
        } else if (VU.isEmpty(edtDollarAmt)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Amount_In_Doller));
            return false;
        } else if (VU.isEmpty(edtPaymentMethod)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Select_payment_Mthd));
            return false;
        } else if (edtDollarAmt.getText().toString().equalsIgnoreCase("") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("0") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("00") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("000") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("0000") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("00000") ||
                edtDollarAmt.getText().toString().equalsIgnoreCase("000000")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Amount_In_Doller));
            return false;
        } else if (edtDollarAmt.getText().toString().equalsIgnoreCase("0.")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtDollarAmt.getText().toString().equalsIgnoreCase(".0")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtBtcAmt.getText().toString().equalsIgnoreCase("") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("0") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("00") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("000") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("0000") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("00000") ||
                edtBtcAmt.getText().toString().equalsIgnoreCase("000000")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_BTC_Amount));
            return false;
        } else if (edtBtcAmt.getText().toString().equalsIgnoreCase("0.")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (edtBtcAmt.getText().toString().equalsIgnoreCase(".0")) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
            return false;
        } else if (Double.valueOf(edtDollarAmt.getText().toString()) > Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.DOLLER_AMOUNT, ""))) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.not_enough_btc));
            return false;
        }
        return true;
    }

    private void showBottomSheetDialog() {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        final View view = getLayoutInflater().inflate(R.layout.wallet_bottom_list, null);
        final ListView currencyTypeList = view.findViewById(R.id.list_bottom);
        Log.e(TAG, "showBottomSheetDialog: paymentNameList: " + paymentNameList);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, R.layout.adapter_wallet_bottom_list, R.id.textView, paymentNameList);
        currencyTypeList.setAdapter(arrayAdapter);

        currencyTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                if (paymentStatusList.get(position).equalsIgnoreCase("Pending")) {
                    mBottomSheetDialog.cancel();
                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.payment_method_status));
                } else {
                    edtPaymentMethod.setText(paymentNameList.get(position));
                    strPaymentId = paymentidList.get(position);
                    mBottomSheetDialog.cancel();
                }
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    public void conversionText() {
        generalTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                previousLength = s.length();
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    backSpace = previousLength > s.length();

                    if (edtBtcAmt.getText().hashCode() == s.hashCode()) {
                        typing = true;
                        try {
                            edtBtcAmt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
                            edtDollarAmt.removeTextChangedListener(generalTextWatcher);

                            if (edtBtcAmt.length() == 0) {
                                edtDollarAmt.setText("");
                            } else {

                                final double btcAmt = Double.valueOf((String.valueOf(s)));
                                Log.e("btcAmt", btcAmt + "");
                                Log.e("usd", ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                                double walletDollerAmt = (double) ((btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""))));
                                Log.e("walletDollerAmt", walletDollerAmt + "");

                                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);
                                Log.e("strWalletDollerAmt", strWalletDollerAmt);
                              /*  if(strWalletDollerAmt.length()>3)
                                String[] str_arr1 = strWalletDollerAmt.split(".");
                                Log.e("str_arr1", str_arr1[0]);
                                if(str_arr1[0].equalsIgnoreCase("0")){
                                    edtAmtDoller.setText("0"+strWalletDollerAmt);
                                }else {
                                    edtAmtDoller.setText(strWalletDollerAmt);
                                }*/
                                edtDollarAmt.setText(strWalletDollerAmt);
                                ACU.MySP.saveSP(context, ACU.MySP.DOLLER_AMOUNT, String.valueOf(walletDollerAmt));
                            }
                            edtDollarAmt.addTextChangedListener(generalTextWatcher);
                        } catch (Exception e) {
                            e.printStackTrace();
                            // CustomToast.custom_Toast(contextDashBoard, "incorrect input", layout);
                            edtDollarAmt.addTextChangedListener(generalTextWatcher);
                        }
                    } else if (edtDollarAmt.getText().hashCode() == s.hashCode()) {
                        edtDollarAmt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        if (typing) {
                            if (backSpace) {
                                edtDollarAmt.setText("");
                                edtBtcAmt.setText("");
                            }
                            typing = false;
                        }
                        edtBtcAmt.removeTextChangedListener(generalTextWatcher);
                        if (edtDollarAmt.length() == 0) {
                            edtBtcAmt.setText("");
                        } else {
                            final double dollerAmt = Double.valueOf((String.valueOf(s)));
                            final float btcAmt = (float) (dollerAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

                            DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
                            final String strBtcAmt = numberFormat.format(btcAmt);
                            edtBtcAmt.setText(strBtcAmt);

                        }

                        edtBtcAmt.addTextChangedListener(generalTextWatcher);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    edtBtcAmt.addTextChangedListener(generalTextWatcher);
                    //dialogSendCurrencyActivity("Please Provide Valid Input");
                }
            }
        };
        edtDollarAmt.addTextChangedListener(generalTextWatcher);
        edtBtcAmt.addTextChangedListener(generalTextWatcher);
//swapnil
        edtDollarAmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

//                    priority = "";


                } else {
                    if (c == 0) {
                        Log.d(TAG, "onTouch: wwwkk");
                        CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));
                        edtDollarAmt.setEnabled(false);
                    }
                    c++;
                }
                return false;
            }
        });

        edtBtcAmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                c = 0;
                if (Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.BTC_AMT, "")) > Double.valueOf(0.00000000)) {

//                    priority = "";


                } else {
                    if (c == 0) {
                        Log.d(TAG, "onTouch: wwwkk");
                        CustomDialogs.dialogShowMsg(context, screenType, getString(R.string.zerobalence));
                        edtBtcAmt.setEnabled(false);
                    }
                    c++;
                }
                return false;
            }
        });
    }

    //wallet_id
    private void getTxnFee() {
        String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "") + "&send_to=" +
                "&txnType=SELL" + "&amount=" + edtBtcAmt.getText().toString() + "&username=" + "&wallettype=MAIN" +
                "&paymentId=" + strPaymentId + "&wyreAccId=" + strWyreAccId +
                "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes()) +
                "&priority=" + priority;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(Get_Txn_Fee);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        boolean status = jsonObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
                                String str_raw_hex = dataObject.getString("singrawhexastr");
                                double txnfee = dataObject.getDouble("txnfee");
                                double sendAmt = dataObject.getDouble("send_amt");
                                double totalAmt = dataObject.getDouble("total_amt");
                                String strReceiverAddress = dataObject.getString("receiverAddr");
                                String txnType = dataObject.getString("txnType");
                                String strUniqueId = dataObject.getString("uniqueId");
                                String strWalletId = dataObject.getString("wallet_id");
                                String strCloneWalletStatus = dataObject.getString("clone_wallet_status");
                                String strCloneWalletId = dataObject.getString("clone_wallet_id");
                                String strCloneUniqueId = dataObject.getString("clone_uniqueId");
                                String strWyreFinalRecAmt = dataObject.getString("final_received_amt");
                                String strWyreFee = dataObject.getString("wyreFee");
                                String strWyreFeeRate = dataObject.getString("wyrefeerate");
                                String strIsPaymentWallet = dataObject.getString("is_payment_wallet");

                                String strTxnFee = Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(txnfee));
                                String strSendAmt = Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(sendAmt));
                                String strTotalAmt = Miscellaneous.CustomsMethods.convertDoubleToString(Double.valueOf(totalAmt));

                                Intent intent = new Intent(context, SendingCurrencyActivity.class);
                                intent.putExtra("address", strReceiverAddress);
                                intent.putExtra("txnfee", strTxnFee);
                                intent.putExtra("sendAmt", strSendAmt);
                                intent.putExtra("txnType", txnType);
                                intent.putExtra("totalAmt", strTotalAmt);
                                intent.putExtra("walletId", strWalletId);
                                intent.putExtra("txnHash", str_raw_hex);
                                intent.putExtra("uniqueId", strUniqueId);
                                intent.putExtra("cloneUniqueId", strCloneUniqueId);
                                intent.putExtra("cloneWalletId", strCloneWalletId);
                                intent.putExtra("cloneWalletStatus", strCloneWalletStatus);
                                intent.putExtra("wyreFinalRecAmt", strWyreFinalRecAmt);
                                intent.putExtra("wyreFee", strWyreFee);
                                intent.putExtra("wyreFeeRate", strWyreFeeRate);
                                intent.putExtra("isPaymentWallet", strIsPaymentWallet);
                                ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "sell_activity");
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, TransactionActivity.class));
        finish();
    }

    private void checkPaymentMthdAvailable() {

        String urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) +
                "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes());
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(checkPaymentAvailableUrl);
        helper.setUrlParameter(urlParameters);

        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {

                        JSONObject jsonObject = new JSONObject(response);
                        String msg = jsonObject.getString("message");
                        String status_code = jsonObject.getString("status_code");
                        String status = jsonObject.getString("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equals("200") && status.equals("true")) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            String strStatus = dataObj.getString("status");
                            strWyreAccId = dataObj.getString("wyreAccId");
                            strAccStatus = dataObj.getString("account_status");
                            JSONArray paymentListArray = dataObj.getJSONArray("paymentList");
                            strUploadStatus = dataObj.getString("upload_status");
                            JSONObject profileSatusObj = dataObj.getJSONObject("profile_status");
                            strAddrProofStatus = profileSatusObj.getString("addr_proof_status");
                            strPaymentMthdStatus = profileSatusObj.getString("payment_method_status");
                            strAddressSatus = profileSatusObj.getString("address");
                            strDobStatus = profileSatusObj.getString("dob");
                            strNameStatus = profileSatusObj.getString("name");
                            strGovIdStatus = profileSatusObj.getString("goverment_id_status");
                            strCellPhnStatus = profileSatusObj.getString("cellphone");
                            strEmailStatus = profileSatusObj.getString("email");
                            strSsnStatus = profileSatusObj.getString("ssn");

                            if (strStatus.equals("false") || strAccStatus.equals("OPEN") || strAccStatus.equals("PENDING")) {
                                if (strUploadStatus.equalsIgnoreCase("3")) {
                                    dialogGoToAccountActivity(msg);   // profile data not updated properly.again have to update profile data
                                } else if (strPaymentMthdStatus.equalsIgnoreCase("OPEN")) {
                                    dialogCreatePaymentMthd(getResources().getString(R.string.payment_method_not_available));
                                } else if (strGovIdStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equalsIgnoreCase("0")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.go_to_upload_document));
                                } else if (strAddrProofStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("1")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.upload_address_proof));
                                } else if (strPaymentMthdStatus.equalsIgnoreCase("PENDING")) {
                                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.payment_method_state));
                                } else if (strAddrProofStatus.equalsIgnoreCase("PENDING") || strGovIdStatus.equalsIgnoreCase("PENDING")) {
                                    dialogShowMsg(context, screenType, getResources().getString(R.string.Document_proof));
                                }
                            } else if (strStatus.equalsIgnoreCase("true") && strAccStatus.equalsIgnoreCase("APPROVED")) {
                                if (strPaymentMthdStatus.equalsIgnoreCase("OPEN")) {
                                    dialogCreatePaymentMthd(getResources().getString(R.string.payment_method_not_available));
                                } else if (strGovIdStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("0")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.go_to_upload_document));
                                } else if (strAddrProofStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("1")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.upload_address_proof));
                                } else if (strPaymentMthdStatus.equalsIgnoreCase("PENDING")) {
                                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.payment_method_state));
                                } else if (strAddrProofStatus.equalsIgnoreCase("PENDING") || strGovIdStatus.equalsIgnoreCase("PENDING")) {
                                    dialogShowMsg(context, screenType, getResources().getString(R.string.Document_proof));
                                }
                            }


                            /*if (strStatus.equals("false") || strAccStatus.equals("OPEN") || strAccStatus.equals("PENDING")) {
                                if (strUploadStatus.equalsIgnoreCase("3")) {
                                    dialogGoToAccountActivity(msg);   // profile data not updated properly.again have to update profile data
                                }else if (strGovIdStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equalsIgnoreCase("0")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.go_to_upload_document));
                                } else if (strAddrProofStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("1")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.upload_address_proof));
                                }  else if (strPaymentMthdStatus.equalsIgnoreCase("OPEN")) {
                                    dialogCreatePaymentMthd(getResources().getString(R.string.payment_method_not_available));
                                } else if (strPaymentMthdStatus.equalsIgnoreCase("PENDING")) {
                                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.payment_method_state));
                                } else if (strAddrProofStatus.equalsIgnoreCase("PENDING") || strGovIdStatus.equalsIgnoreCase("PENDING")) {
                                    dialogShowMsg(context, screenType, getResources().getString(R.string.Document_proof));
                                }
                            } else if (strStatus.equalsIgnoreCase("true") && strAccStatus.equalsIgnoreCase("APPROVED")) {
                                if (strPaymentMthdStatus.equalsIgnoreCase("OPEN")) {
                                    dialogCreatePaymentMthd(getResources().getString(R.string.payment_method_not_available));
                                } else if (strGovIdStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("0")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.go_to_upload_document));
                                } else if (strAddrProofStatus.equalsIgnoreCase("OPEN") && strUploadStatus.equals("1")) {
                                    dialogGoToSelectDocumentUploadActivity(getResources().getString(R.string.upload_address_proof));
                                } else if (strPaymentMthdStatus.equalsIgnoreCase("PENDING")) {
                                    CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.payment_method_state));
                                } else if (strAddrProofStatus.equalsIgnoreCase("PENDING") || strGovIdStatus.equalsIgnoreCase("PENDING")) {
                                    dialogShowMsg(context, screenType, getResources().getString(R.string.Document_proof));
                                }
                            }*/

                            if (paymentListArray.length() > 0) {
                                getPaymentList(paymentListArray);
                            }

                        } else if (status_code.equals("205")) {
                            dialogGoToAccountActivity(msg);  // if account(profile) details are not completely filled then jump to account Activity
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }

                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();

                }
            }
        });
        restAsyncTask.execute();

    }

    private void getPaymentList(JSONArray jsonArray) {
        if (jsonArray.length() != 0) {
            try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    paymentNameList.add(jsonArray.getJSONObject(i).getString("name") + " - " +
                            Miscellaneous.CustomsMethods.capitalizeFirstLetter(jsonArray.getJSONObject(i).getString("status")));
                    paymentidList.add(jsonArray.getJSONObject(i).getString("id"));
                    paymentStatusList.add(jsonArray.getJSONObject(i).getString("status"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void dialogGoToAccountActivity(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn_tab);
        } else {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "SellActivity");
                startActivity(new Intent(context, AccountActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogGoToSelectDocumentUploadActivity(String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn_tab);
        } else {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(context, SelectDocumentForUpload.class);
                intent.putExtra("wyreAccId", strWyreAccId);
                intent.putExtra("documentStatusType", strUploadStatus);
                startActivity(intent);
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogCreatePaymentMthd(String msg) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before

        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn_tab);
        } else {
            dialog.setContentView(R.layout.dialog_with_cancel_ok_btn);
        }
        dialog.setCancelable(false);
        TextView txtMsg = dialog.findViewById(R.id.txt_msg);
        txtMsg.setText(msg);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onBackPressed();
            }
        });

        dialog.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                if (VU.isConnectingToInternet(context, screenType)) {
                    dialog.dismiss();
                    ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                    ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                    Intent intent = new Intent(context, PaymentTokenActivity.class);
                    intent.putExtra("wyreAccId", strWyreAccId);
                    startActivity(intent);
                    finish();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogShowMsg(final Context context, String screenType, String text) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, TransactionActivity.class));
                finish();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
