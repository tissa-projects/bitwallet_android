package com.Android.Inc.bitwallet.Activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.Android.Inc.bitwallet.Activities.BuySellActivity.MoonPayActivity;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.adapters.PortfolioAdapter;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PortfolioActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_drawer;
    private Button btnSend, btnReceive;
    private RecyclerView recyclerPortfolio;
    private PieChart pieChart;
    private Context context;
    private PortfolioAdapter portfolioAdapter;
    private ProgressBar progressBar;
    private String strTotalDollerAmt;
    private ArrayList<Float> wallet_dollerAmt_list = null;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String status_code = "";
    String screenType;
    private boolean minimizeBtnPressed = false;
    View layout;

    private API exampleApi;
    private static final String TAG = PortfolioActivity.class.getSimpleName();
    public static String walletUrl = "business/v2/get_walletList";
    private AsyncTask mtaskWalletList = null;


    ArrayList<Integer> colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_portfolio_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_portfolio);
        }
        context = PortfolioActivity.this;

        //list of cities/countries Buy Sellfunctionality not available
        //  CustomDialogs.dialogShowMsg(context,screenType,getResources().getString(R.string.list));

        initialize();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {

        Log.e(TAG, "onUserLeaveHint: start");
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: out if");
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e(TAG, "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e(TAG, "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e(TAG, "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }

    public void initialize() {

        ACU.MySP.setSPBoolean(context, "isSell", false);


        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));


        btnSend = findViewById(R.id.btn_send);
        btnReceive = findViewById(R.id.btn_receive);
        progressBar = findViewById(R.id.portfolio_progressBar);
        swipeRefreshLayout = findViewById(R.id.portfolio_refresh);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        ll_drawer = findViewById(R.id.title_bar_left_menu);

        btnSend.setOnClickListener(this);
        btnReceive.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_drawer.setOnClickListener(this);

        recyclerPortfolio = findViewById(R.id.recycler_portfolio);
        pieChart = findViewById(R.id.pieChart1);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerPortfolio.setLayoutManager(linearLayoutManager);
        recyclerPortfolio.setHasFixedSize(true);

        if (VU.isConnectingToInternet(context, screenType)) {
            portfolioData();
        }

    }

    public void pieChart() {
        pieChart.setUsePercentValues(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setRotationEnabled(false);
        pieChart.animate();
        pieChart.getAnimation();
        pieChart.getLegend().setEnabled(false);

        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(65f);
        pieChart.setHoleColor(Color.BLACK);
        pieChart.setTransparentCircleRadius(0f);
        pieChart.setCenterText("$ " + strTotalDollerAmt);
        //pieChart.setCenterText("$ " + "50,000.00");
        pieChart.setCenterTextColor(Color.WHITE);
        pieChart.setCenterTextRadiusPercent(100f);
        pieChart.setCenterTextTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/roboto_bold.ttf"));
        pieChart.setCenterTextSize(20f);

        ArrayList<PieEntry> pieArray = new ArrayList<>();
        colors = new ArrayList<Integer>();
        int[] MY_COLORS = {Color.WHITE, Color.rgb(169, 169, 169), Color.rgb(68, 173, 226),
                Color.rgb(255, 0, 0), Color.rgb(27, 215, 66), Color.rgb(65, 105, 225),
                Color.rgb(255, 192, 203), Color.rgb(0, 128, 0), Color.rgb(128, 0, 128), Color.rgb(255, 255, 0),
                Color.rgb(255, 165, 0), Color.rgb(128, 128, 128), Color.rgb(0, 191, 255), Color.rgb(218, 165, 32),
                Color.rgb(0, 206, 209), Color.rgb(255, 99, 71), Color.rgb(255, 105, 180), Color.rgb(127, 255, 212),
                Color.rgb(245, 245, 120), Color.rgb(0, 0, 255), Color.rgb(250, 235, 215), Color.rgb(210, 105, 30),
                Color.rgb(100, 149, 237), Color.rgb(189, 187, 107), Color.rgb(255, 182, 192), Color.rgb(216, 191, 216),
                Color.rgb(138, 43, 226), Color.rgb(165, 42, 42), Color.rgb(222, 184, 135), Color.rgb(95, 158, 160),
                Color.rgb(70, 130, 180), Color.rgb(173, 255, 47)};

        for (int i = 0; i < wallet_dollerAmt_list.size(); i++) {
            int c = MY_COLORS[i];
            colors.add(c);
        }

        if (strTotalDollerAmt.equalsIgnoreCase("0.00")) {
            int c = Color.rgb(68, 173, 226);
            pieArray.add(new PieEntry(20f, ""));
            PieDataSet dataSet = new PieDataSet(pieArray, "walletData");
            dataSet.setColors(c);
            dataSet.setSliceSpace(0f);
            dataSet.setSelectionShift(20f);
            pieChart.animateX(800);
            PieData data = new PieData(dataSet);
            data.setValueTextSize(0f);
            pieChart.setData(data);

        } else if ((wallet_dollerAmt_list.size() > 1) || (wallet_dollerAmt_list.size() == 1)) {
            for (int i = 0; i < wallet_dollerAmt_list.size(); i++) {
                pieArray.add(new PieEntry(wallet_dollerAmt_list.get(i), ""));
            }

            PieDataSet dataSet = new PieDataSet(pieArray, "walletData");
            dataSet.setColors(colors);
            dataSet.setSliceSpace(0f);
            dataSet.setSelectionShift(20f);
            pieChart.animateX(400);
            PieData data = new PieData(dataSet);
            data.setValueTextSize(0f);
            pieChart.setData(data);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:  //buy
                ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "Portfolio_sendReceive");
                Intent intent = new Intent(context, MoonPayActivity.class);
                intent.putExtra("txt_type", "BUY");
                ACU.MySP.setSPBoolean(context, "isSell", false); // for changing is sell transaction status
                startActivity(intent);
                finish();
                break;
            case R.id.btn_receive:  //sell
                goToWalletActivity();
                break;
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                startActivity(new Intent(context, SidePanalActivity.class));
                finish();
                break;


        }

    }

    private void goToWalletActivity() {
        ACU.MySP.saveSP(context, ACU.MySP.BTNCLICK, "Portfolio_sendReceive");
        Intent intent = new Intent(context, WalletListActivity.class);
        ACU.MySP.setSPBoolean(context, "isSell", true);
        startActivity(intent);
        finish();
    }

    private void portfolioData() {
        wallet_dollerAmt_list = new ArrayList<>();

        mtaskWalletList = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {

                    urlParameters = "username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes()) + "&walletType=MAIN" + "&balance_flag=" + true;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(walletUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                } catch (Exception ex) {
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    if (s == null) {
                        // CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(s);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        Log.d(TAG, "onPostExecute: getWalletIdCheck " + jsonObject);
                        Log.d(TAG, "onPostExecute: " + status_code + " Message " + msg);
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        } else if (status_code.equalsIgnoreCase("200")) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            if (dataObj != null) {
                                try {
                                    double TotalAmt = dataObj.getDouble("total_bal");
                                    double Usdrate = Double.parseDouble(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, ""));
                                    double TotalDollerAmt = TotalAmt * Usdrate;
                                    DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                    strTotalDollerAmt = numberFormat.format(TotalDollerAmt);
                                    JSONArray walletListArray = dataObj.getJSONArray("walletList");
                                    for (int i = 0; i < walletListArray.length(); i++) {
                                        JSONObject walletObj = walletListArray.getJSONObject(i);
                                        int isdefault = walletObj.getInt("is_default");
                                        double WalletAmt = walletObj.getDouble("wallet_bal");  //in btc
                                        float walletDollerAmt = (float) (WalletAmt * Usdrate);
                                        wallet_dollerAmt_list.add(walletDollerAmt);
                                        if (isdefault == 1) {
                                            ACU.MySP.saveSP(context, ACU.MySP.WALLET_ID, walletObj.getString("wallet_id"));
                                            ACU.MySP.saveSP(context, ACU.MySP.WALLET_Type, walletObj.getString("walletType"));
                                        }
                                        Log.e("floatValue", walletDollerAmt + "");
                                    }
                                    setPieChart(walletListArray);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, ": exception  getWalletIdCheck" + e.getMessage());
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();

    }

    public void setPieChart(JSONArray txnListArray) {

        pieChart.setVisibility(View.VISIBLE);
        pieChart();
        portfolioAdapter = new PortfolioAdapter(context, txnListArray, colors);
        recyclerPortfolio.setAdapter(portfolioAdapter);
    }

    //Swipe referesh
    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public void onRefresh() {
        if (VU.isConnectingToInternet(context, screenType)) {
            portfolioData();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    public void dialogSessionExpire(String text1, String text2) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_two_line_tab);
        } else {
            dialog.setContentView(R.layout.dialog_two_line);
        }
        TextView firstmsgTxt = dialog.findViewById(R.id.txt_firstline);
        firstmsgTxt.setText(text1);
        TextView secondmsgTxt = dialog.findViewById(R.id.txt_secondline);
        secondmsgTxt.setText(text2);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                    dialog.dismiss();
                    ACU.MySP.saveSP(context, ACU.MySP.LOGOUT_YES_BTN, "logoutClick");
                    ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS, "");
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                    //     getActivity().finish();
                    //   getActivity().onBackPressed();

                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onDestroy() {
        if (mtaskWalletList != null && mtaskWalletList.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskWalletList.cancel(true);
        }
        super.onDestroy();
    }

    @Override
    public void onPause() {
        if (mtaskWalletList != null && mtaskWalletList.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskWalletList.cancel(true);
        }
        super.onPause();
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            minimizeBtnPressed = true;
            finish();
        }
    }
}
