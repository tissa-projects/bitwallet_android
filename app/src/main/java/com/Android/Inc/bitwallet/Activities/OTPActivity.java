package com.Android.Inc.bitwallet.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.CustomToast;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;


public class OTPActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtResend, txtSubmit;
    private AppCompatEditText edtOTP;
    private ProgressBar progressBar;
    private View layout;
    private Context context;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    // private LinearLayout titleBarBackBtn;
    private String msg = null;
    private String status_code = "";
    String screenType;

    private RestAsyncTask restAsyncTask = null;
    private static final String TAG = OTPActivity.class.getSimpleName();
    public static String resendOTPUrl = "resend_otp";
    public static String verifyOTPUrl = "outh";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_otp_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_otp);
        }
        context = OTPActivity.this;
        initialize();


        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
    }

    private void initialize() {
        txtResend = findViewById(R.id.txt_resend);
        txtSubmit = findViewById(R.id.txt_submit);
        edtOTP = findViewById(R.id.edt_otp);
        progressBar = findViewById(R.id.otp_progressBar);
        //  titleBarBackBtn = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        txtResend.setOnClickListener(this);
        txtSubmit.setOnClickListener(this);
        //titleBarBackBtn.setOnClickListener(this);
        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);

        if (ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_STATUS_BOOLEAN, "").equalsIgnoreCase("false")) {
            if (VU.isConnectingToInternet(context, screenType)) {
                resendOTP();
            }
        }

    }

    public boolean Validate() {
        if (VU.isEmpty(edtOTP)) {
           /* edtOTP.setError("Please Enter OTP");
            edtOTP.requestFocus();*/
            dialogOTPActivity("Enter BitWallet Secure Code", false);
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;

            case R.id.txt_submit:
                if (VU.isConnectingToInternet(context, screenType)) {
                    if (Validate()) {
                        submitOTP();
                    }
                }
                break;

            case R.id.txt_resend:
                ACU.MySP.saveSP(context, ACU.MySP.LOGIN_STATUS_BOOLEAN, "");
                if (VU.isConnectingToInternet(context, screenType)) {
                    resendOTP();
                }
                break;

           /* case R.id.title_bar_left_menu:
                startActivity(new Intent(context, SignUpIndividualActivity.class));
                finish();
                break;*/
        }
    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }


    public void resendOTP() {

        String urlParameters = "user_id=" + ACU.MySP.getFromSP(context, ACU.MySP.REGISTRATION_EMAIL, "");
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setAccept("application/json");
        helper.setMethodType("POST");
        helper.setRequestUrl(resendOTPUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);

        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject OTPObject = new JSONObject(response);
                        msg = OTPObject.getString("message");
                        Log.e("Msg", msg);
                        status_code = OTPObject.getString("status_code");
                        boolean status = OTPObject.getBoolean("status");

                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status) {
                            dialogResendOTP(getResources().getString(R.string.Secure_Code_Sent_Successfully));
                        } else {
                            dialogResendOTP(msg);
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();
    }

    public void submitOTP() {
        final String type = "registration";

        String urlParameters = "otp=" + edtOTP.getText().toString() + "&username=" + ACU.MySP.getFromSP(context, ACU.MySP.REGISTRATION_EMAIL, "") + "&type=" + type;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setAccept("application/json");
        helper.setMethodType("POST");
        helper.setRequestUrl(verifyOTPUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);

        restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject OTPObject = new JSONObject(response);
                        msg = OTPObject.getString("message");
                        Log.e("Msg", msg);
                        status_code = OTPObject.getString("status_code");
                        boolean status = OTPObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status) {
                            dialogOTPActivity(msg, status);
                        } else {
                            dialogOTPActivity(getResources().getString(R.string.Secure_Code_Is_Invalid), status);  // if  otp  is invalid
                        }
                    }
                } catch (Exception e) {
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    e.printStackTrace();
                }
            }
        });
        restAsyncTask.execute();


    }

    public void dialogOTPActivity(String text, final boolean status) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (status) {
                    startActivity(new Intent(context, LoginActivity.class));
                    finish();
                } else {
                    edtOTP.setText("");
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void dialogResendOTP(String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        if (screenType.equalsIgnoreCase("tablet")) {
            dialog.setContentView(R.layout.dialog_one_line_text_tab);
        } else {
            dialog.setContentView(R.layout.dialog_one_line_text);
        }
        TextView msgTxt = dialog.findViewById(R.id.txt_msg);
        msgTxt.setText(text);

        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.btn_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtOTP.setText("");
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @Override
    public void onBackPressed() {
        doExitApp();
    }


    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            CustomToast.custom_Toast(context, "Press Back Again To Exit App", layout);
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }


}
