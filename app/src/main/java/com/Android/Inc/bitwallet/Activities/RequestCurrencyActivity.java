package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.Android.Inc.bitwallet.BuildConfig;
import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.GenerateQRCode;
import com.Android.Inc.bitwallet.utils.VU;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class RequestCurrencyActivity extends AppCompatActivity implements LifecycleObserver, View.OnClickListener {

    View layout;
    private ProgressBar progressBar;
    private RelativeLayout rlImgbarcode;
    private AppCompatEditText edtEnterBtcAmt, edtAmtDoller;
    private Button btnRequest;
    private TextView txtAddressKey;
    private ImageView imgBarcode;
    private Context context;
    private double double_btc_amt = 0;
    TextWatcher generalTextWatcher = null;
    private int previousLength;
    private boolean backSpace, typing;
    private String status_code = "", strWalletId = "";
    ;
    String screenType;
    private boolean isShare = false;
    private boolean minimizeBtnPressed = false;

    private API exampleApi;
    private static final String TAG = RequestCurrencyActivity.class.getSimpleName();
    public static String getAddressKeyUrl = "business/v2/get-address";
    private AsyncTask mtaskReceive = null;


    ImageView imgFaceboook, imgTwitter, imgLinkedIn;
    LinearLayout ll_backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_request_currency_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_request_currency);
        }

        context = RequestCurrencyActivity.this;
        //custom toast layout
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        Initialize();
        if (VU.isConnectingToInternet(context, screenType)) {
            getAddressKeyApi();
        }

        // for sharing img this line is imp other wise img format not support
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        btnRequest.setEnabled(false);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        Log.e(TAG, "onUserLeaveHint: ");
        minimizeBtnPressed = true;

        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        if (isShare) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
        }
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onStop() {
        Log.e("MyApp", "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    public void Initialize() {
        progressBar = findViewById(R.id.request_progressBar);
        rlImgbarcode = findViewById(R.id.rl_img_barcode);
        edtEnterBtcAmt = findViewById(R.id.edt_enterBtcAmt);
        edtAmtDoller = findViewById(R.id.edt_amtDoller);
        btnRequest = findViewById(R.id.btn_request);
        txtAddressKey = findViewById(R.id.txt_adddresskey);
        imgBarcode = findViewById(R.id.img_barcode);


        ll_backArrow = findViewById(R.id.title_bar_left_menu);
        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);

        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        ll_backArrow.setOnClickListener(this);


        Log.e("TextDoller", edtAmtDoller.length() + "");
        Log.e("TextBTC", edtEnterBtcAmt.length() + "");

        generalTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                previousLength = s.length();
            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    backSpace = previousLength > s.length();

                    if (edtEnterBtcAmt.getText().hashCode() == s.hashCode()) {
                        typing = true;
                        try {
                            edtAmtDoller.setFilters(new InputFilter[]{new InputFilter.LengthFilter(40)});
                            edtAmtDoller.removeTextChangedListener(generalTextWatcher);

                            if (edtEnterBtcAmt.length() == 0) {
                                edtAmtDoller.setText("");
                                //   String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + 0;
                                String url = "bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + 0;
                                Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                                imgBarcode.setImageBitmap(bitmapQRcode);
                                //      Tools.displayImageOriginalString(context, imgBarcode, url);
                            } else {
                                final double btcAmt = Double.valueOf((String.valueOf(s)));

                                float walletDollerAmt = (float) (btcAmt * Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

                                DecimalFormat numberFormat = new DecimalFormat("#,###,##0.00");
                                final String strWalletDollerAmt = numberFormat.format(walletDollerAmt);

                                DecimalFormat numberFormatBTC = new DecimalFormat("#0.00000000");
                                final String strBtcAmt = numberFormatBTC.format(btcAmt);

                                edtAmtDoller.setText(strWalletDollerAmt);
                               /* String url = "https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + strBtcAmt;
                                Tools.displayImageOriginalString(context, imgBarcode, url);*/
                                String url = "bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + strBtcAmt;
                                Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                                imgBarcode.setImageBitmap(bitmapQRcode);
                            }

                            btnRequest.setEnabled(true);
                            edtAmtDoller.addTextChangedListener(generalTextWatcher);
                        } catch (Exception e) {
                            //  CustomToast.custom_Toast(contextDashBoard, "incorrect input", layout);
                            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.provide_valid_input));
                        }
                    } else if (edtAmtDoller.getText().hashCode() == s.hashCode()) {
                        edtAmtDoller.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                        if (typing) {
                            if (backSpace) {
                                edtAmtDoller.setText("");
                                edtEnterBtcAmt.setText("");
                            }
                            typing = false;
                        }
                        edtEnterBtcAmt.removeTextChangedListener(generalTextWatcher);
                        if (edtAmtDoller.length() == 0) {
                            edtEnterBtcAmt.setText("");
                            String url = "bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + 0;
                            Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                            imgBarcode.setImageBitmap(bitmapQRcode);
                        } else {
                            final double dollerAmt = Double.valueOf((String.valueOf(s)));
                            final float btcAmt = (float) (dollerAmt / Double.valueOf(ACU.MySP.getFromSP(context, ACU.MySP.USD_RATE, "")));

                            DecimalFormat numberFormat = new DecimalFormat("#0.00000000");
                            final String strBtcAmt = numberFormat.format(btcAmt);
                            edtEnterBtcAmt.setText(strBtcAmt);
                            String url = "bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + strBtcAmt;
                            Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                            imgBarcode.setImageBitmap(bitmapQRcode);

                        }

                        btnRequest.setEnabled(true);
                        edtEnterBtcAmt.addTextChangedListener(generalTextWatcher);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //   CustomDialogs.dialogShowMsg(context, screenType,getResources().getString(R.string.provide_valid_input));
                }
            }


        };

        edtAmtDoller.addTextChangedListener(generalTextWatcher);
        edtEnterBtcAmt.addTextChangedListener(generalTextWatcher);


        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validate()) {
                    ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
                    ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
                    isShare = true;
                    takeScreenshot();
                }
            }
        });

    }

    public boolean Validate() {
        if (VU.isEmpty(edtEnterBtcAmt)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_BTC_Amount));
            return false;
        } else if (VU.isEmpty(edtAmtDoller)) {
            CustomDialogs.dialogShowMsg(context, screenType, getResources().getString(R.string.Enter_Amount_In_Doller));
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;
            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;
            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;

        }

    }

    public void socialMedia(String flag, String url) {

        if (VU.isConnectingToInternet(context, screenType)) {
            isShare = false;
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "outsideView");
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            ACU.MySP.saveSP(context, ACU.MySP.WEB_VIEW, "webView");
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, TransactionActivity.class));
        finish();
    }

    private void getAddressKeyApi() {

        mtaskReceive = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                strWalletId = ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, "");
                Log.e(TAG, "doInBackground: walletId : " + strWalletId);
                try {
                    urlParameters = "txn_type=REC" + "&empcode=" +
                            "&wallet_id=" + strWalletId + "&wallettype=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_Type, "");
                    //    urlParameters = "txn_type=BUY" + "&empcode="+"wallettype=MAIN";
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(getAddressKeyUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground response : " + s);

                  /*  exampleApi = new API(context, ACU.MySP.getFromSP(context, ACU.MySP.WALLET_ID, ""), 0);
                    s = exampleApi.doGet(getAddressKeyUrl, "RequestCurrencyActivity");
                    Log.e("s", s);*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                rlImgbarcode.setVisibility(View.VISIBLE);
                String msg = null;
                try {
                    if (s == null) {
                        //   CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {

                        JSONObject jsonObject = new JSONObject(s);
                        msg = jsonObject.getString("message");

                        status_code = jsonObject.getString("status_code");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        if (dataObj != null) {
                            String str_address = dataObj.getString("address");
                            String strtxnType = dataObj.getString("txn_type");
                            txtAddressKey.setText(str_address);
                            String url = "bitcoin:" + txtAddressKey.getText().toString() + "?amount=" + 0;
                            Bitmap bitmapQRcode = GenerateQRCode.myQRcode.generateQR(context, url);
                            imgBarcode.setImageBitmap(bitmapQRcode);
                            edtEnterBtcAmt.setEnabled(true);
                            edtAmtDoller.setEnabled(true);
                            btnRequest.setEnabled(true);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        }.execute();

    }

    public void takeScreenshot() {
        Bitmap bitmap = getBitmapFromView(rlImgbarcode);
        onShareImage(bitmap);
    }

    //Sending image - New Method (Added by Niraj)
    public void onShareImage(Bitmap bitmap) {
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
            Uri imageUri = Uri.parse(path);

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("image/*");
            waIntent.putExtra(android.content.Intent.EXTRA_STREAM, imageUri);

            waIntent.putExtra(Intent.EXTRA_TEXT, "Please send " + edtEnterBtcAmt.getText().toString() + " BTC to the Bitcoin Address. " +
                    txtAddressKey.getText().toString() + " ?amount=" + edtEnterBtcAmt.getText().toString());

            context.startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (Exception e) {
            Log.e("Error on sharing", e + " ");
            Toast.makeText(context, "App not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    //Sending image - Old Method (Removed by niraj because not working with android 11)
    public void takeScreenshot2() {
        Bitmap bitmap = getBitmapFromView(rlImgbarcode);
        try {
            File file = new File(getExternalCacheDir(), "barcode.png");

            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);


            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(getExternalCacheDir() + File.separator + "barcode.png"));

            intent.putExtra(Intent.EXTRA_TEXT, "Please send " + edtEnterBtcAmt.getText().toString() + " BTC to the Bitcoin Address. " +
                    txtAddressKey.getText().toString() + " ?amount=" + edtEnterBtcAmt.getText().toString());
            intent.setType("image/png");


            // Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);


            Log.d(TAG, "takeScreenshot: 1  " + file.getAbsolutePath());
            Log.d(TAG, "takeScreenshot: 2 " + Uri.fromFile(file));
            startActivity(Intent.createChooser(intent, "Share via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }

    @Override
    public void onDestroy() {
        if (mtaskReceive != null && mtaskReceive.getStatus() != AsyncTask.Status.FINISHED) {
            mtaskReceive.cancel(true);
        }
        super.onDestroy();
    }


}
