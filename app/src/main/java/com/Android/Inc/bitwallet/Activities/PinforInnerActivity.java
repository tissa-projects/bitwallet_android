
package com.Android.Inc.bitwallet.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import com.Android.Inc.bitwallet.R;
import com.Android.Inc.bitwallet.SSl_Classes.API;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClient;
import com.Android.Inc.bitwallet.ServiceCall.RestAPIClientHelper;
import com.Android.Inc.bitwallet.ServiceCall.RestAsyncTask;
import com.Android.Inc.bitwallet.SplashActivity;
import com.Android.Inc.bitwallet.interfaces.AsyncResponse;
import com.Android.Inc.bitwallet.utils.ACU;
import com.Android.Inc.bitwallet.utils.CustomDialogs;
import com.Android.Inc.bitwallet.utils.Miscellaneous;
import com.Android.Inc.bitwallet.utils.VU;
import com.alimuzaffar.lib.pin.PinEntryEditText;

import org.json.JSONObject;

import in.arjsna.passcodeview.PassCodeView;

public class PinforInnerActivity extends AppCompatActivity implements View.OnClickListener {

    private PinEntryEditText passCodeView;
    private Context context;
    private TextView txtEnterPin;
    private ImageView imgFaceboook, imgTwitter, imgLinkedIn, imgBackArrow;
    private ProgressBar progressBar;
    View layout;
    private String status_code = "", priority = "LOW";
    private boolean minimizeBtnPressed = false;

    private API exampleApi;
    private static final String TAG = PinforInnerActivity.class.getSimpleName();

    public static String verifyPasscodeUrl = "passcode_verify";
    public static String WithDrawUrl = "crypto-currency/send-txn";
     public static String Get_Txn_Fee = "crypto-currency/get-txnfee/v1";// For Live
 //   public static String Get_Txn_Fee = "crypto-currency/nownode/get-txnfee/v1";// For Testing 03-10-2022
    
    private AsyncTask mtaskPasscodeVerify = null, mtaskWithDraw = null;
    private String screenType;
    private int count = 0;
    private boolean isShare = false;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600) {
            screenType = "tablet";
            setContentView(R.layout.activity_pinfor_inner_tab);
        } else {
            screenType = "mobile";
            setContentView(R.layout.activity_pinfor_inner);
        }
        context = PinforInnerActivity.this;
        initialize();
        bindEvents();
        //  findViewById(R.id.bottom_ll).setVisibility(View.GONE); //for bottom social media buttons
    }


    public void initialize() {
        passCodeView = (PinEntryEditText) findViewById(R.id.pass_code_view);

        txtEnterPin = findViewById(R.id.enter_pin_text);
        imgBackArrow = findViewById(R.id.title_bar_left_menu);

        imgFaceboook = findViewById(R.id.img_facebook);
        imgTwitter = findViewById(R.id.img_twitter);
        imgLinkedIn = findViewById(R.id.img_linkedIn);
        progressBar = findViewById(R.id.pin_progressBar);


        imgFaceboook.setOnClickListener(this);
        imgTwitter.setOnClickListener(this);
        imgLinkedIn.setOnClickListener(this);
        imgBackArrow.setOnClickListener(this);


        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/ShareTechMono-Regular.ttf");
        passCodeView.setTypeface(typeFace);
//        passCodeView.setTypeFace(typeFace);
//        passCodeView.setKeyTextColor(Color.parseColor("#ffffff"));
//        passCodeView.setEmptyDrawable(R.drawable.empty_dots_pinview);
//        passCodeView.setFilledDrawable(R.drawable.filled_dots_pinview);

        txtEnterPin.setVisibility(View.VISIBLE);

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void bindEvents() {

        passCodeView.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                String text = str.toString();
                if (text.length() == 6) {
                    count++;
                    if (VU.isConnectingToInternet(context, screenType)) {
                        if (count == 1) {
                            CreateAndVerifyPin(text);
                        }//count because it calling createVerify Api 2 times

                    }
                }
            }
        });
    }


    //Home button pressed
    @Override
    protected void onUserLeaveHint() {
        minimizeBtnPressed = true;
        if (!ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("outsideView")) {
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            finish();
        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "onResume: ");
        if (ACU.MySP.getFromSP(context, ACU.MySP.OUTSIDE_VIEW, "").equalsIgnoreCase("out_from_outsideView")) {
            Log.e(TAG, "onResume: in if");
            ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }
        super.onResume();
    }


    @Override
    protected void onStop() {
        Log.e(TAG, "Stop method called");
        if (minimizeBtnPressed) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
        } else {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "stop");
        }
        super.onStop();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("MyApp", "App in background");
        ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "BackGround");

        if (ACU.MySP.getFromSP(context, ACU.MySP.CHECK_APP_STOP, "").equalsIgnoreCase("stop")) {
            ACU.MySP.saveSP(context, ACU.MySP.CHECK_APP_STOP, "");
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("MyApp", "App in foreground");
        if (ACU.MySP.getFromSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "").equalsIgnoreCase("BackGround")) {
            ACU.MySP.saveSP(context, ACU.MySP.App_BackGnd_ForeGnd_Status, "ForeGround");
        }
    }


    public void socialMedia(String flag, String url) {

        Log.e(TAG, "socialMedia: Pinpage opening2");

        if (VU.isConnectingToInternet(context, screenType)) {
            Intent in_contact = new Intent(context, SocialMediaActivity.class);
            in_contact.putExtra("flag", flag);
            in_contact.putExtra("url", url);
            startActivity(in_contact);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_facebook:
                socialMedia("facebook", "https://www.facebook.com/BitWalletInc");
                break;

            case R.id.img_linkedIn:
                socialMedia("linkedIn", ACU.MySP.INSTA_URL);
                break;

            case R.id.img_twitter:
                socialMedia("twitter", "https://twitter.com/bitwalletinc");
                break;
            case R.id.title_bar_left_menu:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        passCodeView.setText(null);

        if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("PaymentReceivedActivity"))) {
            startActivity(new Intent(context, PaymentReceivedActivity.class));
            finish();
        } else if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("ChargeBitwalletActivity"))) {
            startActivity(new Intent(context, ChargeBitwalletActivity.class));
            finish();
        } else if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("FilterNEMployeesActivity"))) {
            startActivity(new Intent(context, FilterNEmployeesActivity.class));
            finish();
        }/*else{
            startActivity(new Intent(context, SidePanalActivity.class));
            finish();
        }*/
    }

    public void CreateAndVerifyPin(String pinTxt) {

        Log.e("StringPin", pinTxt);
        final String encode_pintxt = VU.encode(pinTxt.getBytes());

        mtaskPasscodeVerify = new AsyncTask<String, String, String>() {
            @Override
            protected void onPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }


            @Override
            protected String doInBackground(String... objects) {
                String s = null, urlParameters = null;
                try {
                    urlParameters = "security_code=" + encode_pintxt;
                    RestAPIClientHelper helper = new RestAPIClientHelper();
                    helper.setContentType("application/x-www-form-urlencoded");
                    helper.setMethodType("POST");
                    helper.setRequestUrl(verifyPasscodeUrl);
                    helper.setUrlParameter(urlParameters);
                    s = RestAPIClient.APICLient.getRemoteCall(helper, context);
                    Log.e(TAG, "doInBackground: response: " + s);
                  /*  exampleApi = new API(context, encode_pintxt);
                    s = exampleApi.doGet(verifyPasscodeUrl, "PinActivity");*/
                } catch (Throwable ex) {
                    count = 0;
                }

                return s;
            }

            @Override
            protected void onPostExecute(String s) {
                progressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                try {
                    if (s == null) {
                        //CustomToast.custom_Toast(contextDashBoard, "Request Timeout.Please Tyr Again", layout);
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                        passCodeView.setText(null);
                    } else {
                        JSONObject pinObject = new JSONObject(s);
                        String msg = pinObject.getString("message");
                        status_code = pinObject.getString("status_code");
                        boolean status = pinObject.getBoolean("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                           /* ACU.MySP.saveSP(contextDashBoard, ACU.MySP.LOGIN_STATUS, "");
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();*/
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        ACU.MySP.saveSP(context, ACU.MySP.OUTSIDE_VIEW, "");
                        if (status) {
                            //    if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("PaymentReceivedActivity"))) {
                            if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("FilterNEMployeesActivity"))) {
                                if (VU.isConnectingToInternet(context, screenType)) {
                                    Log.e(TAG, "onPostExecute: " + ACU.MySP.CHECK_FOR_ACTIVITY);
                                    getTransactionFee();

                                }
                            } else if (((ACU.MySP.CHECK_FOR_ACTIVITY).equalsIgnoreCase("ChargeBitwalletActivity"))) {
                                startActivity(new Intent(context, SidePanalActivity.class));
                                finish();
                            }
                        } else if (!status) {
                            count = 0;
                            passCodeView.setText(null);
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    count = 0;
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                    passCodeView.setText(null);
                    e.printStackTrace();
                }
            }
        }.execute();

    }


    private void getTransactionFee() {

        String urlParameters = "wallet_id=" + ACU.MySP.getFromSP(context, ACU.MySP.WALLET_BUSINESS_ID, "")
                + "&txnType=WITHDRAW" + "&wallettype=PAYMENT" + "&username=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.LOGIN_Email, "").getBytes())
                + "&paymentId=" + "&wyreAccId=" + "&send_to=" + "&amount="
                + "&userType=" + VU.encode(ACU.MySP.getFromSP(context, ACU.MySP.USER_TYPE, "").getBytes())
                + "&priority=" + priority;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(Get_Txn_Fee);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }

                        if (status && status_code.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            if (dataObject != null) {
                                String str_raw_hex = dataObject.getString("singrawhexastr");
                                String str_fees = dataObject.getString("txnfee");
                                String strReceiverAddress = dataObject.getString("receiverAddr");
                                String strTxnType = dataObject.getString("txnType");
                                String strSendAmt = dataObject.getString("send_amt");
                                String strUniqueId = dataObject.getString("uniqueId");
                                String strWalletId = dataObject.getString("wallet_id");
                                String strTotalAmt = dataObject.getString("total_amt");
                                String strBitpayAddress = dataObject.getString("bitpayaddr");
                                String strBitpayFee = dataObject.getString("bitpayfee");
                                String strCloneWalletStatus = dataObject.getString("clone_wallet_status");
                                String strCloneWalletId = dataObject.getString("clone_wallet_id");
                                String strCloneUniqueId = dataObject.getString("clone_uniqueId");
                                String strIsPaymentWallet = dataObject.getString("is_payment_wallet");
                                Log.e(TAG, "onPostExecute: strTxnType: " + strTxnType);
                                Log.e(TAG, "onPostExecute: str_fees: " + str_fees);
                                Log.e(TAG, "onPostExecute: strReceiverAddress: " + strReceiverAddress);
                                Log.e(TAG, "onPostExecute: strSendAmt: " + strSendAmt);
                                Log.e(TAG, "onPostExecute: strUniqueId: " + strUniqueId);
                                //    String str_is_otp = dataObject.getString("is_opt");
                                ACU.MySP.saveSP(context, ACU.MySP.NETWORK_FEE, str_fees);

                            /*if (str_is_otp.equalsIgnoreCase("true")) {
                                startActivity(new Intent(context, OTPVerifyForPaymentActivity.class));

                            } else {*/
                                ACU.MySP.saveSP(context, ACU.MySP.FEES, str_fees);
                                ACU.MySP.saveSP(context, ACU.MySP.RAW_HEX, str_raw_hex);

                                WithDrawData(str_raw_hex, str_fees, strReceiverAddress, strTxnType, strSendAmt, strUniqueId,
                                        strWalletId, strTotalAmt, strBitpayAddress, strBitpayFee, strCloneWalletStatus, strCloneWalletId, strCloneUniqueId, strIsPaymentWallet);

                            } else {
                                CustomDialogs.dialogShowMsg(context, screenType, msg);
                            }
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        });
        restAsyncTask.execute();
    }

    private void WithDrawData(String str_raw_hex, final String str_fees, String strReceiverAddress, String strTxnType,
                              String strSendAmt, String strUniqueId, String strWalletId, final String strTotalAmt, String strBitpayAddress,
                              String strBitPayFee, String strCloneWalletStatus, String strCloneWalletId, String strCloneUniqueId, String strIsPaymentWallet) {

        String urlParameters = "txnhexastr=" + str_raw_hex + "&wallet_id=" + strWalletId + "&receiverAddr=" + strReceiverAddress
                + "&total_amt=" + strTotalAmt + "&txnfee=" + str_fees + "&txnType=" + strTxnType + "&send_amt="
                + strSendAmt + "&bitpayfee=" + strBitPayFee + "&bitpayaddr=" + strBitpayAddress + "&uniqueId=" + strUniqueId
                + "&clone_wallet_status=" + strCloneWalletStatus + "&clone_wallet_id=" + strCloneWalletId + "&clone_uniqueId=" + strCloneUniqueId
                + "&is_payment_wallet=" + strIsPaymentWallet;
        RestAPIClientHelper helper = new RestAPIClientHelper();
        helper.setContentType("application/x-www-form-urlencoded");
        helper.setMethodType("POST");
        helper.setRequestUrl(WithDrawUrl);
        helper.setUrlParameter(urlParameters);
        progressBar.setVisibility(View.VISIBLE);
        RestAsyncTask restAsyncTask = new RestAsyncTask(context, helper, TAG, new AsyncResponse() {
            @Override
            public void processResponse(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    if (response == null) {
                        CustomDialogs.dialogRequestTimeOut(context, screenType);
                    } else {
                        JSONObject jsonObject = new JSONObject(response);
                        status_code = jsonObject.getString("status_code");
                        String msg = jsonObject.getString("message");
                        String status = jsonObject.getString("status");
                        if (status_code.equalsIgnoreCase("1001") || status_code.equalsIgnoreCase("1002")) {
                            CustomDialogs.dialogSessionExpire(context, screenType);
                        }
                        if (status.equalsIgnoreCase("true")) {
                            Intent intent = new Intent(context, WithDrawActivity.class);
                            intent.putExtra("total_bal", strTotalAmt);
                            intent.putExtra("txn_fee", str_fees);
                            startActivity(intent);
                            finish();
                        } else {
                            CustomDialogs.dialogShowMsg(context, screenType, msg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CustomDialogs.dialogRequestTimeOut(context, screenType);
                }
            }
        });
        restAsyncTask.execute();
    }

}
